<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Kata sandi berhasil diatur ulang.',
    'sent' => 'Tautan untuk reset kata sandi telah dikirim ke email. Silakan cek kotak masuk atau spam.',
    'throttled' => 'Harap tunggu sebentar dan coba lagi.',
    'token' => 'Token tidak valid. Pastikan alamat email sesuai dan klik pada email yang terakhir masuk.',
    'user' => 'Alamat email tidak terdaftar.',

];
