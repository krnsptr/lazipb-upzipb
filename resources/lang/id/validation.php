<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Isian :attribute harus disetujui.',
    'active_url' => 'Isian :attribute harus berupa alamat URL yang aktif.',
    'after' => 'Isian :attribute harus berupa tanggal setelah :date.',
    'after_or_equal' => 'Isian :attribute harus berupa tanggal setelah atau sama dengan :date.',
    'alpha' => 'Isian :attribute hanya boleh mengandung huruf.',
    'alpha_dash' => 'Isian :attribute hanya boleh mengandung huruf, angka, tanda hubung (-), dan underscore (_).',
    'alpha_num' => 'Isian :attribute hanya boleh mengandung huruf dan angka.',
    'array' => 'Isian :attribute harus berupa array.',
    'before' => 'Isian :attribute harus berupa tanggal sebelum :date.',
    'before_or_equal' => 'Isian :attribute harus berupa tanggal sebelum atau sama dengan :date.',
    'between' => [
        'numeric' => 'Isian :attribute harus antara :min dan :max.',
        'file' => 'Isian :attribute harus antara :min dan :max kilobyte.',
        'string' => 'Isian :attribute harus antara :min dan :max karakter.',
        'array' => 'Isian :attribute harus memiliki :min sampai :max item.',
    ],
    'boolean' => 'Isian :attribute harus berupa nilai benar atau salah.',
    'confirmed' => 'Isian :attribute tidak cocok dengan konfirmasi.',
    'date' => 'Isian :attribute harus berupa tanggal yang valid.',
    'date_equals' => 'Isian :attribute harus sama dengan :date.',
    'date_format' => 'Isian :attribute harus berupa tanggal dengan format :format.',
    'different' => 'Isian :attribute dan :other harus berbeda.',
    'digits' => 'Isian :attribute harus :digits digit.',
    'digits_between' => 'Isian :attribute harus antara :min dan :max digit.',
    'dimensions' => 'Dimensi gambar :attribute tidak valid.',
    'distinct' => 'Isian :attribute tidak boleh memiliki nilai yang ganda.',
    'email' => 'Isian :attribute harus berupa alamat email yang valid.',
    'ends_with' => 'Isian :attribute harus berakhiran: :values',
    'exists' => 'Isian :attribute yang dipilih tidak valid.',
    'file' => 'Isian :attribute harus berupa file.',
    'filled' => 'Isian :attribute harus berisi nilai.',
    'gt' => [
        'numeric' => 'Isian :attribute harus lebih dari :value.',
        'file' => 'Isian :attribute harus lebih dari :value kilobyte.',
        'string' => 'Isian :attribute harus lebih dari :value karakter.',
        'array' => 'Isian :attribute harus memiliki lebih dari :value item.',
    ],
    'gte' => [
        'numeric' => 'Isian :attribute harus lebih dari atau sama dengan :value.',
        'file' => 'Isian :attribute harus lebih dari atau sama dengan :value kilobyte.',
        'string' => 'Isian :attribute harus lebih dari atau sama dengan :value karakter.',
        'array' => 'Isian :attribute harus memiliki kurang dari atau sama dengan :value item.',
    ],
    'image' => 'Isian :attribute harus berupa gambar',
    'in' => 'Isian :attribute yang dipilih tidak valid.',
    'in_array' => 'Isian :attribute tidak ada di :other.',
    'integer' => 'Isian :attribute harus berupa bilangan bulat.',
    'ip' => 'Isian :attribute harus berupa alamat IP yang valid.',
    'ipv4' => 'Isian :attribute harus berupa alamat IPv4 yang valid.',
    'ipv6' => 'Isian :attribute harus berupa alamat IPv6 yang valid.',
    'json' => 'Isian :attribute harus berupa string JSON yang valid.',
    'lt' => [
        'numeric' => 'Isian :attribute harus kurang dari :value.',
        'file' => 'Isian :attribute harus kurang dari :value kilobyte.',
        'string' => 'Isian :attribute harus kurang dari :value karakter.',
        'array' => 'Isian :attribute harus memiliki kurang dari :value item.',
    ],
    'lte' => [
        'numeric' => 'Isian :attribute harus kurang dari atau sama dengan :value.',
        'file' => 'Isian :attribute harus kurang dari atau sama dengan :value kilobyte.',
        'string' => 'Isian :attribute harus kurang dari atau sama dengan :value karakter.',
        'array' => 'Isian :attribute harus memiliki kurang dari atau sama dengan :value item.',
    ],
    'max' => [
        'numeric' => 'Isian :attribute tidak boleh lebih dari :max.',
        'file' => 'Isian :attribute tidak boleh lebih dari :max kilobyte.',
        'string' => 'Isian :attribute tidak boleh lebih dari :max karakter.',
        'array' => 'Isian :attribute tidak boleh memiliki lebih dari :max item.',
    ],
    'mimes' => 'Isian :attribute harus berupa file dengan tipe: :values.',
    'mimetypes' => 'Isian :attribute harus berupa file dengan tipe: :values.',
    'min' => [
        'numeric' => 'Isian :attribute tidak boleh kurang dari :min.',
        'file' => 'Isian :attribute tidak boleh kurang dari :min kilobyte.',
        'string' => 'Isian :attribute tidak boleh kurang dari :min karakter.',
        'array' => 'Isian :attribute tidak boleh memiliki kurang dari :min item.',
    ],
    'multiple_of' => 'Isian :attribute harus berupa kelipatan dari :value.',
    'not_in' => 'Isian :attribute tidak valid.',
    'not_regex' => 'Format isian :attribute tidak valid.',
    'numeric' => 'Isian :attribute harus berupa angka.',
    'password' => 'Kata sandi tidak cocok.',
    'present' => 'Isian :attribute harus ada.',
    'regex' => 'Format isian :attribute tidak valid.',
    'required' => 'Isian :attribute harus diisi.',
    'required_if' => 'Isian :attribute harus diisi jika :other berisi :value.',
    'required_unless' => 'Isian :attribute harus diisi kecuali jika :other berisi :values.',
    'required_with' => 'Isian :attribute harus diisi jika :values ada.',
    'required_with_all' => 'Isian :attribute harus diisi jika :values ada.',
    'required_without' => 'Isian :attribute harus diisi jika :values tidak ada.',
    'required_without_all' => 'Isian :attribute harus diisi jika :values tidak ada.',
    'prohibited' => 'Isian :attribute tidak boleh diisi.',
    'prohibited_if' => 'Isian :attribute tidak boleh diisi jika :other berisi :value.',
    'prohibited_unless' => 'Isian :attribute tidak boleh diisi kecuali jika :other berisi :values.',
    'same' => 'Isian :attribute dan :other harus sama.',
    'size' => [
        'numeric' => 'Isian :attribute harus :size.',
        'file' => 'Isian :attribute harus :size kilobyte.',
        'string' => 'Isian :attribute harus :size karakter.',
        'array' => 'Isian :attribute harus memiliki :size item.',
    ],
    'starts_with' => 'Isian :attribute harus berawalan: :values',
    'string' => 'Isian :attribute harus berupa string.',
    'timezone' => 'Isian :attribute harus berupa zona waktu yang valid.',
    'unique' => 'Isian :attribute sudah terdaftar.',
    'uploaded' => 'Isian :attribute gagal diunggah.',
    'url' => 'Isian :attribute harus berupa alamat URL yang valid.',
    'uuid' => 'Isian :attribute harus berupa UUID yang valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
