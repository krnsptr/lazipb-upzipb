<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Alamat email atau kata sandi tidak cocok.',
    'password' => 'Kata sandi tidak cocok.',
    'throttle' => 'Terlalu banyak percobaan masuk. Silakan coba lagi setelah :seconds detik.',

];
