@extends('errors.illustrated-layout')

@section('code', '429')
@section('title', __('Too Many Requests'))

@section('image')
    <div style="background-image: url({{ asset('/errors/403.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    Kami suka semangat kamu, tapi harap tunggu sebentar ya.
    <ul>
        <li>Pastikan alamat email <strong>benar</strong> dan kotak masuk <strong>tidak penuh</strong>.</li>
        <li>Cek semua label <strong>(Sosial, Promosi, dll)</strong> dan <strong>spam</strong>.</li>
        <li>Silakan <strong>coba lagi</strong> dalam beberapa menit.</li>
        <li>Jika masih ada masalah, silakan hubungi kami.</li>
    </ul>
    <br><br>
@endsection
