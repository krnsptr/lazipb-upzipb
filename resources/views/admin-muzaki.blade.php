@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <div class="row">
      <div class="col px-1 px-md-0">
        <div class="card shadow">
          <div class="card-header">
            <h3 class="mb-0">
              Muzaki
              <span class="float-right">
                <a href="{{ route('admin.muzaki_ekspor') }}" class="btn btn-sm btn-secondary">
                  Ekspor
                </a>

                <a href="{{ route('admin.muzaki_impor') }}" class="btn btn-sm btn-dark">
                  Impor
                </a>
              </span>
            </h3>
          </div>
          <div class="px-3">
            @messages
          </div>
          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th style="max-width: 6rem;">ID</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>NIP</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="hapus_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title title">Hapus Muzaki <span class="id_text"></span></h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="hapus_form" method="post" autocomplete="off">
          @csrf

          <h4 class="text-danger">
            Aksi ini bersifat permanen (tidak dapat diurungkan).
          </h4>

          <p>Masukkan kata sandi untuk melanjutkan.</p>

          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
              Password
            </label>
            <div class="col-md-8 col-xl-7">
              <input type="password" class="form-control" id="password" name="password" required>
            </div>
          </div>

          <br>

          <div class="row">
            <div class="col-lg-6 col-md-8 mx-auto my-1">
              <button type="submit" class="btn btn-block btn-danger" id="ekspor_button">
                Hapus
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
<link href="//cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<style>
  .w-4rem {
    width: 4rem !important;
  }
</style>
@endpush

@push ('js')
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
<script src="{{ asset('assets/js/datatables.extend.js') }}"></script>
<script>
  mapper = {};
  render = {};
  filter = {};

  for (f in filter) {
    for (key in mapper[f]) {
      filter[f].push({
        value: key,
        label: mapper[f][key]
      });
    }
  }
</script>
<script>
  aksi = '<button type="button" onclick="lihat(this)" class="btn btn-sm btn-primary">Lihat</button>';
  aksi += '<button type="button" onclick="hapus(this)" class="btn btn-sm btn-danger">Hapus</button>';

  function lihat(button) {
    id = $(button).closest('tr').attr('id');
    window.open(@json(route('admin.muzaki_lihat', '')) + '/' + id);
  }

  function hapus(button) {
    id = $(button).closest('tr').attr('id');
    $('.id_text').text(': #' + id);
    $('#hapus_form').attr('action', @json(route('admin.muzaki_hapus', '')) + '/' + id);
    $('#hapus_modal').modal('show');
  }

  table = $('#table').DataTable({
    columns: [
      {
        data: 'id',
        name: 'id',
        orderable: true,
        searchable: true,
      },
      {
        data: 'nama',
        name: 'nama',
        searchable: true,
      },
      {
        data: 'email',
        name: 'email',
        searchable: true,
      },
      {
        data: 'nip',
        name: 'nip',
        searchable: true,
      },
      {
        data: null,
        defaultContent: aksi,
        searchable: false,
        orderable: false,
      },
    ],
    order: [
      [0, 'asc'],
    ],
  });

  yadcf.init(table, [
    {
      column_number: 0,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm w-4rem mt-1',
      filter_delay: 1000,
      filter_match_mode: 'exact',
    },
    {
      column_number: 1,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 2,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 3,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
  ]);
</script>
@endpush
