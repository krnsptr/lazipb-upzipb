@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Distribusi: #{{ $distribusi->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          @push ('js')
            <script>
              $('.form-control-plaintext.number').addClass('text-monospace');
              $('#nominal').addClass('text-primary').css('font-size', 'larger');
            </script>
          @endpush

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Tanggal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->tanggal }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Dana
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->dana_text }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Bidang
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->bidang_text ?: '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    SDG
                  </label>
                  <div class="col-md-8 col-xl-7">
                    @isset ($distribusi->sdg_enum)
                      {!! $distribusi->sdg_enum->getLink() !!}
                    @else
                      -
                    @endisset
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Mustahik
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->mustahik->nama ?? '-'  }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    NIK
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->mustahik->nik ?? '-'  }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Asnaf
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->asnaf_text ?: '-'  }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Item
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $distribusi->item }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nominal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="nominal" name="nominal"
                      value="{{ format_rupiah($distribusi->nominal) }}" disabled>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.distribusi_edit', $distribusi) }}"
                  class="btn btn-block btn-dark">
                  Edit
                </a>
              </div>

              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
