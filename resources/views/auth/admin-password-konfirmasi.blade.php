@extends ('layouts.argon.dashboard')

@section ('content')

@include ('layouts.argon.inc.header-guest')

<div class="container mt--8 mb-6">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center">Konfirmasi Kata Sandi</h1>
        </div>
        <div class="px-3 pt-3">
          @messages
        </div>
        <div class="card-body px-lg-5 pt-0 pb-0">
          Sebelum melanjutkan proses, konfirmasi kata sandimu, ya.

          <form autocomplete="on" method="post"
            action="{{ route('admin.password.konfirmasi') }}">
            @csrf

            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-key"></i>
                  </span>
                </div>
                <input type="password" name="password" id="password"
                  class="form-control" placeholder="Kata sandi"
                  required autofocus>
              </div>
            </div>

            <button type="submit" class="btn btn-block btn-primary my-4">
              Konfirmasi
            </button>
          </form>
        </div>
        <div class="card-footer bg-transparent text-center">
          <a href="#" onclick="history.go(-1);">
            Kembali
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
