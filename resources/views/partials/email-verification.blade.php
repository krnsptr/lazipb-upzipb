<div class="alert alert-secondary fade show mt-2">
  <p style="font-size: small;">
    Alamat email belum diverifikasi.
    Silakan cek <mark>{{ auth('anggota')->user()->email }}</mark>
    untuk melakukan verifikasi.
  </p>
  <form method="post" action="{{ route('anggota.email.verifikasi.kirim') }}">
    @csrf
    <button type="submit" class="btn btn-sm btn-warning">
      Kirim ulang
    </button>
  </form>
</div>
