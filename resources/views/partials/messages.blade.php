@if ($info = $info ?? session('info') ?? session('status'))
  <div class="alert alert-info alert-dismissible fade show">
    {!! $info !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if ($success = $success ?? session('success'))
  <div class="alert alert-success alert-dismissible fade show">
    {!! $success !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if ($warning = $warning ?? session('warning'))
  <div class="alert alert-warning alert-dismissible fade show">
    {!! $warning !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if ($error = $error ?? session('error'))
  <div class="alert alert-danger alert-dismissible fade show">
    {!! $error !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if ($errors->any())
  <div class="alert alert-danger alert-dismissible fade show">
    @foreach ($errors->all() as $error)
      {{ $error }}<br>
    @endforeach
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

@if (session('unregistered'))
  <div class="alert alert-warning">
    NIP {{ old('nip') }} belum terdaftar.
    <br>

    Silakan mendaftar di
    <a href="https://ipb.link/otoziswaf" class="text-white" style="text-decoration: underline;">
      ipb.link/otoziswaf</a>
    <br>

    {{-- Apabila ada pertanyaan atau keluhan, silakan hubungi kami.

    @php
      $kontak_wa = \setting('kontak_wa');
      $kontak_wa_plain = preg_replace('/\D/', '', $kontak_wa);
    @endphp

    @if ($kontak_wa)
      <div class="m-1">
        <a href="https://wa.me/{{ $kontak_wa_plain }}" class="btn btn-sm btn-secondary m-1">
          <i class="fab fa-fw fa-whatsapp mr-2"></i>
          {{ $kontak_wa }}
        </a>
      </div>
    @endif --}}
  </div>
@endif
