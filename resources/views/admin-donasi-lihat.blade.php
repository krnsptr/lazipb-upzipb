@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Donasi: #{{ $donasi->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          @push('js')
            <script>
              $('.form-control-plaintext.number').addClass('text-right text-monospace');
              $('#jumlah').addClass('text-primary').css('font-size', 'larger');
            </script>
          @endpush

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $muzaki->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nip" name="nip"
                      value="{{ $muzaki->nip }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Tanggal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="tanggal" name="tanggal"
                      value="{{ $donasi->tanggal }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Status
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="status" name="status"
                      value="{{ $donasi->status_text }}" disabled>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Zakat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="zakat" name="zakat"
                      value="{{ format_rupiah($donasi->zakat) }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Infak
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="infak" name="infak"
                      value="{{ format_rupiah($donasi->infak) }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Santunan Yatim
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="santunan"
                      name="santunan" value="{{ format_rupiah($donasi->santunan) }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Wakaf Tunai
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="wakaf"
                      name="wakaf" value="{{ format_rupiah($donasi->wakaf) }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jumlah
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext number" id="jumlah"
                      name="jumlah" value="{{ format_rupiah($donasi->jumlah) }}" disabled>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-8">
                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Keterangan
                  </label>
                  <div class="col-md-7 my-2">
                    {!! nl2br(e($donasi->keterangan)) !!}
                  </div>
                </div>
              </div>
            </div>

            @if ($link_kuitansi = $donasi->link_kuitansi)
              <hr class="mt-0">

              <div class="row">
                <div class="col-lg-8">
                  <div class="form-group row">
                    <label class="col-md-5 text-md-right font-weight-bold">
                      Link Kuitansi
                    </label>
                    <div class="col-md-7">
                      <a href="{{ $link_kuitansi }}">
                        {{ $link_kuitansi }}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            @endif

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.donasi_edit', $donasi) }}" class="btn btn-block btn-dark">
                  Edit
                </a>
              </div>

              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
