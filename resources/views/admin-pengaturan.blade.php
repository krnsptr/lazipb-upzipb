@extends ('layouts.argon.dashboard')

@section ('content')

<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <form action="{{ url()->current() }}" method="post" id="form" autocomplete="off">
      @csrf
      <div class="row">
        <div class="col px-1 px-md-0">
          <div class="card shadow">
            <div class="card-header">
              <h3 class="mb-0">
                Pengaturan
              </h3>
            </div>
            <div class="px-3">
              @messages
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-8">
                  <div class="form-group row">
                    <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                      Kontak WA
                    </label>
                    <div class="col-md-7">
                      <div class="input-group">
                        <input type="tel" name="kontak_wa" id="kontak_wa" class="form-control"
                          value="{{ setting('kontak_wa') }}" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <div class="row">
                <div class="col-lg-2 col-md-4 mx-auto">
                  <button type="submit" class="btn btn-block btn-default">
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

@endsection
