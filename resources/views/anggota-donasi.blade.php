@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <h3 class="mb-0">Laporan Donasi</h3>
        </div>
        <div class="card-body">
          <div class="px-3">
            @messages
          </div>

          <div class="row">
            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats shadow mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-muted mb-0">
                        Donasi Tahun {{ date('Y') }}
                      </h5>
                      <span class="h3 font-weight-bold mb-0">
                        {{ format_rupiah($jumlah['sum_donasi_now']) }}
                      </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                        <i class="far fa-fw fa-calendar-alt"></i>
                      </div>
                    </div>
                  </div>
                  <div class="text-sm text-muted mt-2">
                    <i class="fas fa-fw fa-check text-success"></i>
                    {{ format_number($jumlah['count_donasi_now']) }}
                    donasi berhasil
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats shadow mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-muted mb-0">
                        Donasi Tahun {{ date('Y') - 1 }}
                      </h5>
                      <span class="h3 font-weight-bold mb-0">
                        {{ format_rupiah($jumlah['sum_donasi_prev']) }}
                      </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-fw fa-reply"></i>
                      </div>
                    </div>
                  </div>
                  <div class="text-sm text-muted mt-2">
                    <i class="fas fa-fw fa-check text-success"></i>
                    {{ format_number($jumlah['count_donasi_prev']) }}
                    donasi berhasil
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-4 col-lg-6">
              <div class="card card-stats shadow mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-muted mb-0">
                        Donasi Keseluruhan
                      </h5>
                      <span class="h3 font-weight-bold mb-0">
                        {{ format_rupiah($jumlah['sum_donasi_all']) }}
                      </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-purple text-white rounded-circle shadow">
                        <i class="fas fa-fw fa-history"></i>
                      </div>
                    </div>
                  </div>
                  <div class="text-sm text-muted mt-2">
                    <i class="fas fa-fw fa-check text-success"></i>
                    {{ format_number($jumlah['count_donasi_all']) }}
                    donasi berhasil
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="dtToolbarContent col-lg-8 p-3 text-center">
            <div class="row">
              <div class="col-lg-5">
                <label class="mr-2">Periode:</label>
                <select class="form-control-sm" id="periode" autocomplete="off">
                  <option value="">Semua</option>
                  <option value="6month">6 Bulan Terakhir</option>
                  <option value="12month">12 Bulan Terakhir</option>
                  <option value="custom">Pilih Tanggal</option>
                  @foreach ($years as $year)
                    <option value="{{ $year }}">
                      Tahun {{ $year }}
                    </option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-5">
                <label class="mr-2">Tanggal:</label>
                <input type="text" class="form-control-sm" id="tanggal" autocomplete="off" disabled>
              </div>
              <div class="col-lg-1 py-1">
                <div class="dropdown">
                  <button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="download"
                    data-toggle="dropdown">
                    <i class="fas fa-fw fa-file-download mr-1"></i>
                    Unduh
                  </button>
                  <div class="dropdown-menu">
                    <a href="#" class="dropdown-item download" data-ext="xlsx">
                      <i class="fas fa-fw fa-file-excel mr-1 text-success"></i>
                      EXCEL
                    </a>
                    <a href="#" class="dropdown-item download" data-ext="pdf">
                      <i class="fas fa-fw fa-file-pdf mr-1 text-danger"></i>
                      PDF
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="summary d-none">
            <div class="row">
              <div class="col-lg-5 ml-auto my-1 py-2 border rounded shadow-sm">
                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Nama
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">:</span>
                    <span class="nama">{{ muzaki()->nama }}</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    NIP
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">:</span>
                    <span class="nip text-monospace">{{ muzaki()->nip }}</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Tanggal
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">:</span>
                    <span class="tanggal">#</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Donasi
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">:</span>
                    <strong class="count_donasi">#</strong>
                    donasi
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Berhasil
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">:</span>
                    <strong class="count_berhasil">#</strong>
                    donasi
                  </div>
                </div>
              </div>

              <div class="col-lg-5 mr-auto my-1 py-2 border rounded shadow-sm">
                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Zakat
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">: Rp</span>
                    <span class="text-monospace sum_dana_zakat">#</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Infak
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">: Rp</span>
                    <span class="text-monospace sum_dana_infak">#</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Santunan
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">: Rp</span>
                    <span class="text-monospace sum_dana_santunan">#</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Wakaf
                  </div>
                  <div class="col-8 text-right">
                    <span class="float-left">: Rp</span>
                    <span class="text-monospace sum_dana_wakaf">#</span>
                  </div>
                </div>

                <div class="row py-1">
                  <div class="col-4 font-weight-bold">
                    Jumlah
                  </div>
                  <div class="col-8 text-right font-weight-bold text-primary">
                    <span class="float-left">: Rp</span>
                    <span class="text-monospace sum_jumlah">#</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th>Tanggal</th>
                  <th>Status</th>
                  <th>Zakat</th>
                  <th>Infak</th>
                  <th>Santunan Yatim</th>
                  <th>Wakaf Tunai</th>
                  <th>Jumlah</th>
                  <th>Keterangan</th>
                  <th>Kuitansi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push ('css')
<link href="//cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css" crossorigin>
<link rel="stylesheet" href="//unpkg.com/scroll-hint@latest/css/scroll-hint.css" crossorigin>
<style>
  .w-4rem {
    width: 4rem !important;
  }

  td.jumlah {
    font-weight: bold;
    font-size: medium;
  }
</style>
@endpush

@push ('js')
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/l10n/id.js" crossorigin></script>
<script src="//unpkg.com/scroll-hint@latest/js/scroll-hint.min.js" crossorigin></script>
<script src="{{ asset('assets/js/datatables.extend.js') }}"></script>
<script>
  mapper = {};
  mapper['status'] = @json(App\Enums\DonasiStatus::asSelectArray());

  render = {};
  render['status'] = function (data) { return mapper['status'][data]; };

  render['tanggal'] = function (data) {
    if (!dayjs(data).isValid()) return '';
    return dayjs(data).format('DD-MM-YYYY');
  }

  render['keterangan'] = function (data) {
    data = String(data || '');
    return data.replace(/(?:\r\n|\r|\n)/g, '<br>');
  }

  render['link_kuitansi'] = function (data) {
    if (!data) {
      return '';
    }

    return $('<a></a>')
      .attr('href', data)
      .attr('class', 'btn btn-sm btn-primary')
      .html('<i class="fas fa-arrow-down fa-fw mr-1"></i> Unduh')
      .prop('outerHTML');
  }

  filter = {};

  for (f in filter) {
    for (key in mapper[f]) {
      filter[f].push({
        value: key,
        label: mapper[f][key]
      });
    }
  }
</script>
<script>
  tanggal_min = dayjs(null);
  tanggal_max = dayjs(null);

  flatpickr.localize(flatpickr.l10ns.id);

  fp_tanggal = flatpickr('#tanggal', {
    dateFormat: 'd-m-Y',
    mode: 'range',
    disableMobile: true,
  });

  status_classes = @json(App\Enums\DonasiStatus::getColors());

  meta = {};

  table = $('#table').DataTable({
    ajax: {
      data: function (data) {
        data.tanggal_min = tanggal_min.isValid() ? tanggal_min.format() : '';
        data.tanggal_max = tanggal_max.isValid() ? tanggal_max.format() : '';
      },
      dataSrc: function (data) {
        meta = data.meta;
        return data.data;
      },
    },
    lengthMenu: [
      [10, 12, 20, -1],
      [10, 12, 20, 'Semua'],
    ],
    columns: [
      {
        data: 'tanggal',
        name: 'tanggal',
        orderable: true,
        searchable: true,
        render: render['tanggal'],
      },
      {
        data: 'status',
        name: 'status',
        orderable: true,
        searchable: true,
        render: render['status'],
      },
      {
        data: 'zakat',
        name: 'zakat',
        className: 'text-right',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'infak',
        name: 'infak',
        className: 'text-right',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'santunan',
        name: 'santunan',
        className: 'text-right',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'wakaf',
        name: 'wakaf',
        className: 'text-right',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'jumlah',
        name: 'jumlah',
        className: 'text-right jumlah',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'keterangan',
        name: 'keterangan',
        defaultContent: '',
        render: render['keterangan'],
      },
      {
        data: 'link_kuitansi',
        name: 'link_kuitansi',
        defaultContent: '',
        render: render['link_kuitansi'],
      },
    ],
    order: [
      [0, 'desc'],
    ],
    rowCallback: function (row, data) {
      color = status_classes[data.status];

      if (color) {
        $(row).addClass('text-' + color);
      }
    },
    drawCallback: function (tr) {
      $('.summary').find('.tanggal').html(
        ''
        + dayjs(meta.tanggal_min).format('DD-MM-YYYY')
        + '&ensp;&ndash;&ensp;'
        + dayjs(meta.tanggal_max).format('DD-MM-YYYY')
      );

      $('.summary').find('.count_donasi').html(meta.count_donasi);
      $('.summary').find('.count_berhasil').html(meta.count_berhasil);

      $('.summary').find('.sum_dana_zakat').html(meta.sum_dana_zakat);
      $('.summary').find('.sum_dana_infak').html(meta.sum_dana_infak);
      $('.summary').find('.sum_dana_santunan').html(meta.sum_dana_santunan);
      $('.summary').find('.sum_dana_wakaf').html(meta.sum_dana_wakaf);

      $('.summary').find('.sum_jumlah').html(meta.sum_jumlah);

      $('.summary').removeClass('d-none');
    },
    initComplete: function () {
      table_width = $('#table').width();
      wrapper_width = $('#table').parent().parent().width();

      if (table_width / wrapper_width > 1.2) {
        new ScrollHint('.table-responsive', {
          i18n: { scrollable: 'Swipe' },
        });
      }
    },
  });

  $('.dtToolbar').replaceWith($('.dtToolbarContent'));

  $('#tanggal').change(function () {
    if ($('#periode').val() !== '') {
      $('#periode > option[value=custom]').prop('selected', true);

      tanggal_min = dayjs(fp_tanggal.selectedDates[0] || null).startOf('day');
      tanggal_max = dayjs(fp_tanggal.selectedDates[1] || null).endOf('day');
    }

    table.draw();
  });

  $('#periode').change(function () {
    periode = $(this).val();

    if (periode === '') {
      tanggal_min = dayjs(null);
      tanggal_max = dayjs(null);
    } else if (periode === '6month') {
      tanggal_min = dayjs().subtract(6, 'month').startOf('day');
      tanggal_max = dayjs();
    } else if (periode === '12month') {
      tanggal_min = dayjs().subtract(12, 'month').startOf('day');
      tanggal_max = dayjs();
    } else if (periode === 'custom') {
      tanggal_min = tanggal_min.isValid() ? tanggal_min : dayjs().subtract(12, 'month').startOf('day');
      tanggal_max = tanggal_max.isValid() ? tanggal_max : dayjs();
    } else {
      tanggal_min = dayjs().year(periode).startOf('year');
      tanggal_max = dayjs().year(periode).endOf('year');
    }

    if (tanggal_min.isValid() && tanggal_max.isValid()) {
      fp_tanggal.setDate([tanggal_min.toDate(), tanggal_max.toDate()]);
      $('#tanggal').prop('disabled', false);
    } else {
      fp_tanggal.clear();
      $('#tanggal').prop('disabled', true);
    }

    table.draw();
  });

  $('a.download').click(function () {
    url = new URL(@json(url()->current()));

    data = {
      action: 'download',
      ext: $(this).attr('data-ext'),
      tanggal_min: tanggal_min.isValid() ? tanggal_min.format() : '',
      tanggal_max: tanggal_max.isValid() ? tanggal_max.format() : '',
    };

    url.search = new URLSearchParams(data);
    location.href = url;
  });
</script>
@endpush

@endsection
