@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row">
        <div class="col-xl-4 col-lg-6 d-none">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Pengguna
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['anggota']) }} akun
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6">
          <div class="card card-stats mb-4">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-muted mb-0">
                    Muzaki
                  </h5>
                  <span class="h3 font-weight-bold mb-0">
                    {{ format_number($jumlah['muzaki']) }} orang
                  </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                    <i class="fas fa-fw fa-user-tie"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('js')
<script>
  $(document).ready(function() {
    if (window.opener !== null) {
      window.close();
    }
  });
</script>
@endpush
