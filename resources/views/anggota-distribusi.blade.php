@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <div class="row mb-0">
            <div class="col-md-6">
              <h3 class="mb-md-0">Laporan Distribusi</h3>
            </div>
            <div class="col-md-6 text-md-right">
              <small>
                <strong class="mr-2">Periode:</strong>
                {{ $tanggal_min->format('d-m-Y') }}
                &ndash;
                {{ $tanggal_max->format('d-m-Y') }}
              </small>
              <button type="button" class="btn btn-sm btn-primary ml-2"
                data-toggle="modal" data-target="#modal_periode">
                <i class="far fa-fw fa-calendar-alt mr-1"></i>
                Ubah
              </button>
            </div>
          </div>
        </div>
        <div class="card-body">
          @messages

          <div class="mb-3">
            <h4 class="mb-3">Penerimaan</h4>

            <div class="row">
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Zakat
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['donasi']['sum_zakat']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-money-bill-wave"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Infak
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['donasi']['sum_infak']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-coins"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Santunan Yatim
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['donasi']['sum_santunan']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-purple text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-child"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Wakaf Tunai
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['donasi']['sum_wakaf']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-mosque"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Total Donasi
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['donasi']['sum_jumlah']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gray-dark text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-wallet"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-3">
            <h4 class="mb-3">Penyaluran Per Bidang</h4>

            <div class="row">
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Pendidikan
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang'][
                            'sum_' . App\Enums\Bidang::coerce('Pendidikan')->value
                          ]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-graduation-cap"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Sosial-Kesehatan
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang'][
                            'sum_' . App\Enums\Bidang::coerce('Sosial-Kesehatan')->value
                          ]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-users"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Kewirausahaan
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang'][
                            'sum_' . App\Enums\Bidang::coerce('Kewirausahaan')->value
                          ]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-store"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Dakwah
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang'][
                            'sum_' . App\Enums\Bidang::coerce('Dakwah')->value
                          ]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-bullhorn"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Operasional
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang'][
                            'sum_' . App\Enums\Bidang::coerce('Operasional')->value
                          ]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-purple text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-tools"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Total Salur
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_bidang']['sum_all']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gray-dark text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-wallet"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="mb-3">
            <h4 class="mb-3">Penyaluran Zakat Per Asnaf</h4>

            <div class="row">
              @php $asnaf = App\Enums\Asnaf::coerce('Fakir-Miskin') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Fakir-Miskin
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-user-minus"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php $asnaf = App\Enums\Asnaf::coerce('Amil') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Amil
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-purple text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-user-cog"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php $asnaf = App\Enums\Asnaf::coerce('Mualaf') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Mualaf
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-user-plus"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php $asnaf = App\Enums\Asnaf::coerce('Gharimin') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Gharimin
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-user-tag"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php $asnaf = App\Enums\Asnaf::coerce('Fisabilillah') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Fisabilillah
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-walking"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php $asnaf = App\Enums\Asnaf::coerce('Ibnusabil') @endphp
              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Ibnusabil
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_' . $asnaf->value]) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-hiking"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="card card-stats shadow mb-4">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-muted mb-0">
                          Total Salur Zakat
                        </h5>
                        <span class="h3 font-weight-bold mb-0">
                          {{ format_rupiah($stats['distribusi_asnaf']['sum_all']) }}
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gray-dark text-white rounded-circle shadow">
                          <i class="fas fa-fw fa-wallet"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal_periode">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title title">Ubah Periode</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
              Periode
            </label>
            <div class="col-md-8 col-xl-7">
              <select class="form-control" id="periode" autocomplete="off">
                {{-- <option value="">Semua</option> --}}
                <option value="6month">6 Bulan Terakhir</option>
                <option value="12month">12 Bulan Terakhir</option>
                <option value="1month">Bulan Ini</option>
                <option value="custom">Pilih Bulan</option>
                @foreach ($years as $year)
                  <option value="{{ $year }}">
                    Tahun {{ $year }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
            Bulan
          </label>
          <div class="col-md-8 col-xl-7">
            <input type="text" class="form-control d-none" id="tanggal" autocomplete="off" disabled>

            <div class="input-group">
              <input type="text" class="form-control" id="tanggal_min" autocomplete="off" disabled>
              <div class="input-group-prepend input-group-append">
                <span class="input-group-text bg-secondary">&ndash;</span>
              </div>
              <input type="text" class="form-control" id="tanggal_max" autocomplete="off" disabled>
            </div>
          </div>
        </div>

        <br>

        <div class="row">
          <div class="col-lg-6 col-md-8 mx-auto my-1">
            <button type="button" class="btn btn-block btn-secondary" id="button_periode">
              Ubah
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push ('css')
<link href="//cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css" crossorigin>
<link rel="stylesheet" href="//unpkg.com/flatpickr@4.6.6/dist/plugins/monthSelect/style.css" crossorigin>
<style>
  .w-6rem {
    width: 6rem !important;
  }
</style>
@endpush

@push ('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js" crossorigin></script>
<script src="//unpkg.com/flatpickr@4.6.6/dist/plugins/monthSelect/index.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/l10n/id.js" crossorigin></script>
<script>
  tanggal_min = dayjs(null);
  tanggal_max = dayjs(null);

  flatpickr.localize(flatpickr.l10ns.id);

  fp_tanggal = flatpickr('#tanggal', {
    dateFormat: 'd-m-Y',
    mode: 'range',
    disableMobile: true,
  });

  fp_tanggal_min = flatpickr('#tanggal_min', {
    dateFormat: 'd-m-Y',
    disableMobile: true,
    plugins: [
      new monthSelectPlugin({
        shorthand: true,
        dateFormat: "M Y",
        altFormat: "M Y",
      }),
    ],
  });

  fp_tanggal_max = flatpickr('#tanggal_max', {
    dateFormat: 'd-m-Y',
    disableMobile: true,
    plugins: [
      new monthSelectPlugin({
        shorthand: true,
        dateFormat: "M Y",
        altFormat: "M Y",
      }),
    ],
  });

  $('#tanggal').change(function () {
    if ($('#periode').val() !== '') {
      $('#periode > option[value=custom]').prop('selected', true);

      tanggal_min = dayjs(fp_tanggal.selectedDates[0] || null).startOf('day');
      tanggal_max = dayjs(fp_tanggal.selectedDates[1] || null).endOf('day');
    }
  });

  function swapTanggal()
  {
    tanggal_min_copy = tanggal_min.clone();
    tanggal_min = tanggal_max.startOf('month');
    tanggal_max = tanggal_min_copy.endOf('month');

    fp_tanggal_min.setDate(tanggal_min.toDate());
    fp_tanggal_max.setDate(tanggal_max.toDate());
  }

  $('#tanggal_min').change(function () {
    if ($('#periode').val() !== '') {
      tanggal_min = dayjs(fp_tanggal_min.selectedDates[0] || null).startOf('month');

      if (tanggal_min.isAfter(tanggal_max)) {
        swapTanggal();
      }

      fp_tanggal.setDate([tanggal_min.toDate(), tanggal_max.toDate()], true);
    }
  });

  $('#tanggal_max').change(function () {
    if ($('#periode').val() !== '') {
      tanggal_max = dayjs(fp_tanggal_max.selectedDates[0] || null).endOf('month');

      if (tanggal_min.isAfter(tanggal_max)) {
        swapTanggal();
      }

      fp_tanggal.setDate([tanggal_min.toDate(), tanggal_max.toDate()], true);
    }
  });

  $('#periode').change(function () {
    periode = $(this).val();

    if (periode === '') {
      tanggal_min = dayjs(null);
      tanggal_max = dayjs(null);
    } else if (periode === '1month') {
      tanggal_min = dayjs().startOf('month');
      tanggal_max = dayjs().endOf('month');
    }  else if (periode === '6month') {
      tanggal_min = dayjs().subtract(6 - 1, 'month').startOf('month');
      tanggal_max = dayjs().endOf('month');
    } else if (periode === '12month') {
      tanggal_min = dayjs().subtract(12 - 1, 'month').startOf('month');
      tanggal_max = dayjs().endOf('month');
    } else if (periode === 'custom') {
      tanggal_min = tanggal_min.isValid() ? tanggal_min : dayjs().subtract(12 - 1, 'month').startOf('month');
      tanggal_max = tanggal_max.isValid() ? tanggal_max : dayjs().subtract(1, 'month').endOf('month');
    } else {
      tanggal_min = dayjs().year(periode).startOf('year');
      tanggal_max = dayjs().year(periode).endOf('year');
    }

    if (tanggal_min.isValid() && tanggal_max.isValid()) {
      fp_tanggal.setDate([tanggal_min.toDate(), tanggal_max.toDate()]);
      fp_tanggal_min.setDate(tanggal_min.toDate());
      fp_tanggal_max.setDate(tanggal_max.toDate());
      $('#tanggal, #tanggal_min, #tanggal_max').prop('disabled', false).addClass('bg-white');
    } else {
      fp_tanggal.clear();
      fp_tanggal_min.clear();
      fp_tanggal_max.clear();
      $('#tanggal, #tanggal_min, #tanggal_max').prop('disabled', true).removeClass('bg-white');
    }
  }).change();

  $('#modal_periode').on('hide.bs.modal', function (e) {
    if ($('.flatpickr-calendar').is(':visible')) {
      console.log('focus');
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  });

  $('#button_periode').click(function () {
    url = new URL(@json(url()->current()));

    data = {
      tanggal_min: tanggal_min.isValid() ? tanggal_min.format('YYYY-MM-DD') : '',
      tanggal_max: tanggal_max.isValid() ? tanggal_max.format('YYYY-MM-DD') : '',
    };

    url.search = new URLSearchParams(data);
    location.href = url;

    $('#modal_periode').modal('hide');
  });
</script>
@endpush
