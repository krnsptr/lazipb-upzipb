@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <div class="row mb-0">
            <div class="col-6">
              <h3 class="mb-0">Dasbor</h3>
            </div>
            <div class="col-6">

            </div>
          </div>
        </div>
        <div class="card-body">
          @messages
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
