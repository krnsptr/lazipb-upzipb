@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Edit Muzaki: #{{ $muzaki->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama Lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama" name="nama"
                      value="{{ $muzaki->nama }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nip" name="nip"
                      value="{{ $muzaki->nip }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NPWP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="npwp" name="npwp"
                      value="{{ $muzaki->npwp }}">
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat Email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control" id="email" name="email"
                      value="{{ $muzaki->email }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor Rekening
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nomor_rekening"
                      name="nomor_rekening" value="{{ $muzaki->nomor_rekening }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor Ponsel
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_hp" name="nomor_hp"
                      value="{{ $muzaki->nomor_hp }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor WhatsApp
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_wa" name="nomor_wa"
                      value="{{ $muzaki->nomor_wa }}" autocomplete="on" list="nomor_hp_autocomplete">

                    <datalist id="nomor_hp_autocomplete">
                      <option value=""></option>
                    </datalist>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <button type="submit" class="btn btn-block btn-primary">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@push ('js')
<script src="//cdn.jsdelivr.net/npm/imask@6.0.5/dist/imask.min.js" crossorigin></script>
<script>
  IMask($('#nomor_hp')[0], {
    mask: '{+62}000000000[000]',
  });

  IMask($('#nomor_wa')[0], {
    mask: '{+62}000000000[000]',
  });

  IMask($('#nomor_rekening')[0], {
    mask: /^\d+$/,
  });

  $('#nomor_hp').change(function () {
    value = $(this).val();
    $('#nomor_hp_autocomplete > option').eq(0).text(value).attr('value', value);
  }).change();
</script>
@endpush

@endsection
