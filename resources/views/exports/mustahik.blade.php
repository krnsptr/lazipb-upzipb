<style>
  table { border-collapse: collapse; font-size: small; }
  td, th { border: 1px solid #000; padding: 5px; }
  .number { text-align: right; }
</style>

<table>
  <thead>
    <tr>
      <th>id</th>
      <th>nama</th>
      <th>nik</th>
      <th>nrp</th>
      <th>nomor_hp</th>
      <th>asnaf</th>
      <th>id_kelurahan</th>
      <th>id_kecamatan</th>
      <th>id_kota</th>
      <th>id_provinsi</th>
      <th>nama_kelurahan</th>
      <th>nama_kecamatan</th>
      <th>nama_kota</th>
      <th>nama_provinsi</th>
      <th>alamat</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_mustahik as $mustahik)
      <tr>
        <td>{{ $mustahik->id }}</td>
        <td>{{ $mustahik->nama }}</td>
        <td>{{ $mustahik->nik }}</td>
        <th>{{ $mustahik->nrp }}</th>
        <th>{{ $mustahik->nomor_hp }}</th>
        <th>{{ $mustahik->asnaf_text }}</th>
        <th>{{ $mustahik->id_kelurahan }}</th>
        <th>{{ $mustahik->id_kecamatan }}</th>
        <th>{{ $mustahik->id_kota }}</th>
        <th>{{ $mustahik->id_provinsi }}</th>
        <th>{{ $mustahik->nama_kelurahan }}</th>
        <th>{{ $mustahik->nama_kecamatan }}</th>
        <th>{{ $mustahik->nama_kota }}</th>
        <th>{{ $mustahik->nama_provinsi }}</th>
        <th>{{ $mustahik->alamat }}</th>
      </tr>
    @endforeach
  </tbody>
</table>
