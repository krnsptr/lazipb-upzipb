<style>
  table { border-collapse: collapse; font-size: small; }
  td, th { border: 1px solid #000; padding: 5px; }
  .number { text-align: right; }
</style>

<table>
  <thead>
    <tr>
      <th>id</th>
      <th>nama</th>
      <th>email</th>
      <th>nip</th>
      <th>npwp</th>
      <th>nomor_hp</th>
      <th>nomor_wa</th>
      <th>nomor_rekening</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_muzaki as $muzaki)
      <tr>
        <td>{{ $muzaki->id }}</td>
        <td>{{ $muzaki->nama }}</td>
        <td>{{ $muzaki->email }}</td>
        <td>{{ $muzaki->nip }}</td>
        <td>{{ $muzaki->npwp }}</td>
        <td>{{ $muzaki->nomor_hp }}</td>
        <td>{{ $muzaki->nomor_wa }}</td>
        <td>{{ $muzaki->nomor_rekening }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
