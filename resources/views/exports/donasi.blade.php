<style>
  table { border-collapse: collapse; font-size: small; }
  td, th { border: 1px solid #000; padding: 5px; }
  .number { text-align: right; }
</style>

<table>
  <thead>
    <tr>
      <th>id</th>
      <th>nama</th>
      <th>nip</th>
      <th>tanggal</th>
      <th>status</th>
      <th>zakat</th>
      <th>infak</th>
      <th>santunan</th>
      <th>wakaf</th>
      <th>jumlah</th>
      <th>keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_donasi as $donasi)
      <tr>
        <td>{{ $donasi->id }}</td>
        <td>{{ $donasi->muzaki->nama }}</td>
        <td>{{ $donasi->muzaki->nip }}</td>
        <td>{{ $donasi->tanggal->format('Y-m-d H:i:s') }}</td>
        <td>
          {{ $donasi->status_enum->description }}
        </td>
        <td class="number">{{ $donasi->zakat }}</td>
        <td class="number">{{ $donasi->infak }}</td>
        <td class="number">{{ $donasi->santunan }}</td>
        <td class="number">{{ $donasi->wakaf }}</td>
        <td class="number">{{ $donasi->jumlah }}</td>
        <td>{!! nl2br(e($donasi->keterangan)) !!}</td>
      </tr>
    @endforeach
  </tbody>
</table>
