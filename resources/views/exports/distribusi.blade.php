<style>
  table { border-collapse: collapse; font-size: small; }
  td, th { border: 1px solid #000; padding: 5px; }
  .number { text-align: right; }
</style>

<table>
  <thead>
    <tr>
      <th>id</th>
      <th>tanggal</th>
      <th>item</th>
      <th>dana</th>
      <th>mustahik</th>
      <th>nik</th>
      <th>asnaf</th>
      <th>bidang</th>
      <th>sdg</th>
      <th>nominal</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_distribusi as $distribusi)
      <tr>
        <td>{{ $distribusi->id }}</td>
        <td>{{ $distribusi->tanggal->format('Y-m-d H:i:s') }}</td>
        <td>{{ $distribusi->item }}</td>
        <td>{{ $distribusi->dana_text }}</td>
        <td>{{ $distribusi->mustahik->nama ?? '-' }}</td>
        <td>{{ $distribusi->mustahik->nik ?? '-' }}</td>
        <td>{{ $distribusi->asnaf_text ?: '-' }}</td>
        <td>{{ $distribusi->bidang_text ?: '-' }}</td>
        <td>{{ $distribusi->sdg_text ?: '-' }}</td>
        <td class="number">{{ $distribusi->nominal }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
