<style>
  table { border-collapse: collapse; font-size: small; }
  td, th { border: 1px solid #000; padding: 5px; }
  .number { text-align: right; }
</style>

<table>
  <thead>
    <tr>
      <th>Tanggal</th>
      <th>Status</th>
      <th>Zakat</th>
      <th>Infak</th>
      <th>Santunan Yatim</th>
      <th>Wakaf Tunai</th>
      <th>Jumlah</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($list_donasi as $donasi)
      <tr>
        <td>{{ $donasi->tanggal->format('d-m-Y') }}</td>
        <td @if ($color = $donasi->status_enum->getColor())
          style="color: {{ $color }}"
          @endif>
          {{ $donasi->status_enum->description }}
        </td>
        <td class="number">{{ $donasi->zakat }}</td>
        <td class="number">{{ $donasi->infak }}</td>
        <td class="number">{{ $donasi->santunan }}</td>
        <td class="number">{{ $donasi->wakaf }}</td>
        <td class="number">{{ $donasi->jumlah }}</td>
        <td>{!! nl2br(e($donasi->keterangan)) !!}</td>
      </tr>
    @endforeach
  </tbody>
</table>
