@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Edit Distribusi: #{{ $distribusi->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          @push('js')
            <script>
              $('.form-control.number').addClass('text-monospace');
              $('.form-control.number').css('-moz-appearance', 'textfield');
              $('#nominal').addClass('text-primary').css('font-size', 'larger');
            </script>
          @endpush

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Tanggal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control bg-white" id="tanggal" name="tanggal"
                      value="{{ $distribusi->tanggal }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Dana
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" name="dana" id="dana" required>
                      <option value=""></option>
                      @foreach (App\Enums\Dana::asSelectArray() as $key => $value)
                        <option value="{{ $key }}" @if ($distribusi->dana === $key) selected @endif>
                          {{ $value }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Bidang
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" name="bidang" id="bidang">
                      <option value=""></option>
                      @foreach (App\Enums\Bidang::asSelectArray() as $key => $value)
                        <option value="{{ $key }}" @if ($distribusi->bidang === $key) selected @endif>
                          {{ $value }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    SDG
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" name="sdg" id="sdg">
                      <option value=""></option>
                      @foreach (App\Enums\Sdg::getInstances() as $sdg)
                        <option value="{{ $sdg->value }}"
                          {{ ($distribusi->sdg === $sdg->value) ? 'selected' : '' }}>
                          {{ $sdg->getText() }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Mustahik
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $distribusi->mustahik->nama ?? '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nik" name="nik"
                      value="{{ $distribusi->mustahik->nik ?? '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Asnaf
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="asnaf" name="asnaf"
                      value="{{ $distribusi->asnaf_text ?: '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Item
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="item" name="item"
                      value="{{ $distribusi->item }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nominal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="nominal"
                      name="nominal" value="{{ $distribusi->nominal }}" required>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <button type="submit" class="btn btn-block btn-primary">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css" crossorigin>
@endpush

@push ('js')
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js" crossorigin></script>

<script>
  flatpickr('#tanggal', {
    enableTime: true,
    enableSeconds: true,
    dateFormat: 'Y-m-d H:i:S',
    time_24hr: true,
  });
</script>
@endpush
