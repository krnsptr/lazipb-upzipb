@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <h3 class="mb-0">
            Impor Mustahik

            <span class="float-right">
              <a href="{{ route('admin.mustahik_templat') }}" class="btn btn-sm btn-dark">
                <i class="fas fa-download mr-1"></i>
                Templat XLSX
              </a>
            </span>
          </h3>
        </div>
        <div class="card-body">
          @messages

          <form method="post" action="{{ url()->current() }}" autocomplete="off" id="form">
            @csrf

            <div class="form-group row">
              <label class="col-md-4 col-xl-5 col-form-label text-md-right">
                File XLSX
              </label>
              <div class="col-md-8 col-xl-7">
                <input class="form-control" type="file" name="file" id="file" accept=".xlsx">
              </div>
            </div>

            <div class="progress" style="height: 1rem;">
              <div class="progress-bar progress-bar-striped progress-bar-animated" id="progressBar"
                style="width: 0%; height: 1rem; line-height: 1rem; font-weight: bold;">0%</div>
            </div>

            <div class="text-center mb-4">
              <button type="button" class="btn btn-warning mx-1" id="validasi" disabled>
                Validasi
              </button>

              <button type="submit" class="btn btn-primary mx-1" id="kirim" disabled>
                Impor
              </button>

              <button type="button" class="btn btn-secondary mx-1 d-none" id="unduh" disabled>
                Unduh Hasil
              </button>
            </div>
          </form>

          <div>
            <style>
              #table_wrapper table th {
                text-transform: capitalize;
              }
            </style>

            <div class="table-responsive" id="table_wrapper">
              <table class="table table-bordered" id="table">
                <thead>
                  <tr>
                    <th class="id">id</th>
                    <th class="nama">nama</th>
                    <th class="nik">nik</th>
                    <th class="code">[KODE]</th>
                    <th class="message">[INFO]</th>
                    <th class="nrp">nrp</th>
                    <th class="nomor_hp">nomor_hp</th>
                    <th class="asnaf">asnaf</th>
                    <th class="id_kelurahan">id_kelurahan</th>
                    <th class="id_kecamatan">id_kecamatan</th>
                    <th class="id_kota">id_kota</th>
                    <th class="id_provinsi">id_provinsi</th>
                    <th class="nama_kelurahan">nama_kelurahan</th>
                    <th class="nama_kecamatan">nama_kecamatan</th>
                    <th class="nama_kota">nama_kota</th>
                    <th class="nama_provinsi">nama_provinsi</th>
                    <th class="alamat">alamat</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('js')
<script src="//unpkg.com/xlsx@0.16.9/dist/xlsx.core.min.js"></script>
<script src="//unpkg.com/dayjs@1.8.21/dayjs.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
  items = [];
  mapEmailToRowId = {};
  duplicateCount = {};
  filename = '';
  progressValue = 0;
  progressMax = 100;
  validateOnly = true;

  // For reliability, please be careful when changing these values
  delay = 500;
  sleepEvery = 10;
  sleepDuration = 1000;

  function progressPercent() {
    if (progressMax < 1) return 0;
    return Math.min(100, Math.round(100 * progressValue / progressMax));
  }

  function handleFile(e) {
    files = e.target.files;
    if (!files.length) return;

    f = files[0];
    filename = f.name;
    reader = new FileReader();

    reader.onload = function(e) {
      data = new Uint8Array(e.target.result);
      workbook = XLSX.read(data, {
        type: 'array'
      });
      sheet = workbook.Sheets[workbook.SheetNames[0]];
      items = XLSX.utils.sheet_to_json(sheet);

      $('#table tbody').html('');
      mapEmailToRowId = {};

      header = $('thead > tr', '#table');
      row = header.clone();

      $('th', row).replaceWith(function() {
        return $('<td/>').addClass($(this).attr('class'));
      });

      $.each(items, function(index, item) {
        rowId = 'row_' + index;
        item._rowId = rowId;

        tr = row.clone();
        tr.attr('id', rowId);

        if (!item.nama || !item.nik) {
          Swal.fire('Error', 'Format sheet tidak sesuai.', 'error');
          items = [];
          return;
        }

        $.each(item, function(key, value) {
          $('td.' + key, tr).text(value);
        });

        tr.appendTo('#table tbody');
      });

      if (items.length > 0) {
        $('#validasi, #unduh').prop('disabled', false);

        progressValue = 0;
        progressMax = items.length;
      }
    };

    reader.readAsArrayBuffer(f);
  }

  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  function writeCell(rowId, key, value) {
    if (rowId) {
      tr = $('#' + rowId);
      td = tr.find('td[class=' + $.escapeSelector(key) + ']').first();
      td.text(value);

      if (key === 'code') {
        if (value === 'OK') td.attr('class', key + ' table-success');
        if (value === 'WARNING') td.attr('class', key + ' table-warning');
        if (value === 'ERROR') td.attr('class', key + ' table-danger');
      }
    }
  }

  function handleSuccess(json, ajaxSettings) {
    data = JSON.parse(ajaxSettings.data);
    rowId = data._rowId;

    if (json.warning) {
      writeCell(rowId, 'code', 'WARNING');
    } else {
      writeCell(rowId, 'code', 'OK');
    }

    writeCell(rowId, 'message', json.message);
  }

  function handleError(xhr, ajaxSettings) {
    json = JSON.parse(xhr.responseText);
    data = JSON.parse(ajaxSettings.data);
    rowId = data._rowId;

    if (json.errors != undefined) {
      text = '';
      $.each(json.errors, function(i, input) {
        $.each(input, function(i, message) {
          text += message + ' // ';
        });
      });
    } else {
      text = json.message || 'Terjadi kesalahan.';
    }

    writeCell(rowId, 'code', 'ERROR');
    writeCell(rowId, 'message', text);
  }

  function warnExit(event) {
    event.preventDefault();
    return event.returnValue = 'Proses sedang berjalan.';
  }

  function start() {
    window.addEventListener('beforeunload', warnExit);
  }

  function finish() {
    codeCount = {
      OK: 0,
      WARNING: 0,
      ERROR: 0,
    };

    messageCount = {};

    $('#table td.code').each(function () {
      code = $(this).text().trim();
      codeCount[code] = codeCount[code] === undefined ? 1 : codeCount[code] + 1;
    });

    $('#table td.message').each(function () {
      message = $(this).text().trim();
      messageCount[message] = messageCount[message] === undefined ? 1 : messageCount[message] + 1;
    });

    codeCountText = '';
    messageCountText = '';

    $.each(codeCount, function (code, count) {
      codeCountText += `${code}: ${count}; `;
    });

    $.each(messageCount, function (message, count) {
      messageCountText += `<small>[${count}] ${message}</small><br>`;
    });

    if (!validateOnly) {
      firstAttribute = $('#table td:eq(1)')[0].classList[0];
      firstItem = items[0];
      lastItem = items[items.length - 1];

      aksi = `${window.location.pathname}`;
      jumlah = items.length;
      input = `${firstItem[firstAttribute]} (..) ${lastItem[firstAttribute]}`;
      output = codeCount;
      detail = messageCount;

      $.ajax({
        url: @json(route('admin.log_tambah')),
        data: JSON.stringify({
          aksi: aksi,
          jumlah: jumlah,
          input: input,
          output: output,
          detail: detail,
        }),
        processData: false,
        contentType: 'application/json',
        dataType: 'json',
        type: 'POST',
      });
    }

    window.removeEventListener('beforeunload', warnExit);
    Swal.fire('Selesai', `${codeCountText} <br><br> ${messageCountText}`, 'success');
    $('#progressBar').removeClass('progress-bar-animated');
    $('#form :input:not(#unduh)').prop('disabled', false);
  }

  function incrementProgress() {
    progressValue++;
    text = progressPercent() + '%';
    $('#progressBar').css('width', text).text(text);

    if (progressPercent() >= 100) {
      finish();
    }
  };

  async function sendRequest(item) {
    url = $('#form').attr('action');

    if (validateOnly) {
      url += '?validateOnly=1';
    }

    $.ajax({
      url: url,
      data: JSON.stringify(item),
      processData: false,
      contentType: 'application/json',
      dataType: 'json',
      type: 'POST',
      success: function(json) {
        handleSuccess(json, this);
      },
      error: function(xhr) {
        handleError(xhr, this);
      },
      complete: function() {
        incrementProgress();
      },
    });
  }

  async function fakeRequest(item) {
    rowId = item._rowId;

    await sleep(200);

    handleSuccess({ message: 'mantap gan' }, { data: `{"rowId": "${rowId}"}` });

    incrementProgress();
  };

  async function kirim() {
    i = 0;

    start();
    progressValue = 0;
    $('td.code').attr('class', 'code').text('');
    $('td.message').attr('class', 'message').text('');
    $('#form :input:not(#unduh)').prop('disabled', true);

    while (i < progressMax) {
      await sleep(delay);

      if (sleepEvery && i % sleepEvery === 0) {
        await sleep(sleepDuration);
      }

      item = items[i];

      try {
        validateOnly = false;
        sendRequest(item);
      } catch (e) {
        console.log(e);
      }

      i++;
    }
  }

  async function validasi() {
    i = 0;

    start();
    progressValue = 0;
    $('td.code').attr('class', 'code').text('');
    $('td.message').attr('class', 'message').text('');
    $('#form :input:not(#unduh)').prop('disabled', true);

    while (i < progressMax) {
      await sleep(delay);

      if (sleepEvery && i % sleepEvery === 0) {
        await sleep(sleepDuration);
      }

      item = items[i];

      try {
        validateOnly = true;
        sendRequest(item);
      } catch (e) {
        console.log(e);
      }

      i++;
    }
  }

  function unduh() {
    date = dayjs().format('YYYY-MM-DD HH-mm-ss');
    workbook = XLSX.utils.table_to_book($('#table')[0]);
    name = 'Impor Mustahik _ ' + date + ' _ ' + filename;

    XLSX.writeFile(workbook, name);
  }

  $('#file').change(handleFile);
  $('#unduh').click(unduh);
  $('#validasi').click(validasi);

  $('#form').submit(function(e) {
    e.preventDefault();
    if (!items.length) return;
    kirim();
  });
</script>
@endpush
