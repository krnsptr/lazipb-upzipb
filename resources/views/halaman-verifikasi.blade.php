@extends ('layouts.argon.dashboard')

@section ('content')

@include ('layouts.argon.inc.header-guest')

<div class="container mt--8 mb--1">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center">Verifikasi Identitas</h1>
        </div>
        <div class="px-3 pt-3">
          @messages
        </div>
        <div class="card-body px-lg-5 pt-0 pb-0 text-center">

          <p class="mb-3">
            Silakan verifikasi identitas Anda dengan memasukkan
            NIP dan nomor rekening gaji yang terdaftar di UPZ IPB.
          </p>

          <form autocomplete="off" method="post" action="{{ url()->current() }}">
            @csrf

            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="far fa-fw fa-envelope"></i>
                  </span>
                </div>
                <input type="email" name="email" id="email" class="form-control"
                  value="{{ $email }}" placeholder="Alamat email" required readonly>
              </div>
            </div>

            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-id-card"></i>
                  </span>
                </div>
                <input type="text" name="nip" id="nip" class="form-control"
                  placeholder="Nomor Induk Pegawai" value="{{ old('nip') }}" required>
              </div>
            </div>

            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-secondary">
                    <i class="fas fa-fw fa-credit-card"></i>
                  </span>
                </div>
                <input type="text" name="nomor_rekening" id="nomor_rekening" class="form-control"
                  placeholder="Nomor Rekening" value="{{ old('nomor_rekening') }}" required>
              </div>
            </div>

            <button type="submit" class="btn btn-block btn-primary my-4">
              Verifikasi
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push ('js')
<script src="//cdn.jsdelivr.net/npm/imask@6.0.5/dist/imask.min.js" crossorigin></script>
<script>
  IMask($('#nip')[0], {
    mask: /^\d+$/,
  });

  IMask($('#nomor_rekening')[0], {
    mask: /^\d+$/,
  });
</script>
@endpush
