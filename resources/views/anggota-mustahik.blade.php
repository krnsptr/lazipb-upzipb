@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-lg-6 col-md-10 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h3 class="mb-0">
            Data Mustahik

            <div class="float-right">
              <small class="mr-2">Asnaf:</small>

              <span class="dropdown">
                <button class="btn btn-primary btn-sm dropdown-toggle" type="button"
                  data-toggle="dropdown">
                  {{ $asnaf_enum->is($asnaf_enum::All) ? 'Semua' : $asnaf_enum }}
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{ request()->fullUrlWithQuery(['page' => null, 'asnaf' => null]) }}"
                    class="dropdown-item">Semua</a>

                  @foreach ($list_asnaf as $key => $value)
                    <a href="{{ request()->fullUrlWithQuery(['page' => null, 'asnaf' => $key]) }}"
                      class="dropdown-item">{{ $value }}</a>
                  @endforeach
                </div>
              </span>
            </div>
          </h3>
        </div>
        <div class="table-responsive">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th>Nama</th>
                <th>Asnaf</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($list_mustahik as $mustahik)
                <tr>
                  <td>
                    <a href="{{ route('anggota.mustahik_lihat', $mustahik) }}">
                      {{ $mustahik->nama }}
                    </a>
                  </td>
                  <td>{{ $mustahik->asnaf_text }}</td>
                </tr>
              @empty
                <tr>
                  <td colspan="2" class="text-center">Belum ada data.</td>
                </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          <style>
            ul.pagination {
              justify-content: center;
            }
          </style>

          {{ $list_mustahik->appends(request()->query())->links() }}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
