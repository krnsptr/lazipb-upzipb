@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <div class="row">
      <div class="col px-1 px-md-0">
        <div class="card shadow">
          <div class="card-header">
            <h3 class="mb-0">
              Log Admin
            </h3>
          </div>
          <div class="px-3">
            @messages
          </div>
          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th style="max-width: 4rem;">ID</th>
                  <th style="max-width: 6rem;">Tanggal</th>
                  <th>Aksi</th>
                  <th>Jumlah</th>
                  <th>Input</th>
                  <th>Output</th>
                  <th></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push ('css')
<link href="//cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push ('js')
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
<script src="{{ asset('assets/js/datatables.extend.js') }}"></script>
<script>
  mapper = {};
  render = {};
  filter = {};

  render['tanggal'] = function (data) {
    if (!dayjs(data).isValid()) return '';
    return dayjs(data).format('YYYY-MM-DD');
  }

  for (f in filter) {
    for (key in mapper[f]) {
      filter[f].push({
        value: key,
        label: mapper[f][key]
      });
    }
  }

  filter['aksi'] = @json(App\Models\AdminLog::distinct('aksi')->pluck('aksi'));
</script>
<script>
  aksi = '<button type="button" onclick="lihat(this)" class="btn btn-sm btn-primary">Lihat</button>';

  function lihat(button) {
    id = $(button).closest('tr').attr('id');
    window.open(@json(route('admin.log_lihat', '')) + '/' + id);
  }

  table = $('#table').DataTable({
    columns: [
      {
        data: 'id',
        name: 'id',
        orderable: true,
        searchable: true,
      },
      {
        data: 'tanggal',
        name: 'tanggal',
        orderable: true,
        searchable: true,
        render: render['tanggal'],
      },
      {
        data: 'aksi',
        name: 'aksi',
        searchable: true,
        className: 'text-monospace',
      },
      {
        data: 'jumlah',
        name: 'jumlah',
        className: 'text-right',
        visible: false,
      },
      {
        data: 'input',
        name: 'input',
      },
      {
        data: 'output',
        name: 'output',
        visible: false,
      },
      {
        data: null,
        defaultContent: aksi,
        searchable: false,
        orderable: false,
      },
    ],
    order: [
      [0, 'desc'],
    ],
  });

  yadcf.init(table, [
    {
      column_number: 0,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm w-4rem mt-1',
      filter_delay: 1000,
      filter_match_mode: 'exact',
    },
    {
      column_number: 1,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm w-4rem mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 2,
      filter_type: 'select',
      filter_match_mode: 'exact',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      data: filter['aksi'],
    },
    {
      column_number: 3,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
  ]);
</script>
@endpush
