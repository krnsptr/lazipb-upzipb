@extends('layouts.argon.dashboard')

@section ('content')

<div class="header pb-2 pb-xl-4 pt-8 d-flex align-items-center"></div>

<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-12">
      <div class="card shadow h-100">
        <div class="card-header">
          <h3 class="mb-0">
            Profil Mustahik
          </h3>
        </div>
        <div class="card-body">
          @messages

          <div class="row no-gutters">
            <div class="col-lg-3 col-md-6 mx-auto mb-3 d-flex flex-column pb-1">
              <div class="row">
                <div class="col-lg-10 mx-auto">
                  <div class="">
                    <div class="p-3 text-center">
                      <img src="{{ $mustahik->foto_url }}" class="border shadow mw-100">
                    </div>
                  </div>

                  <ul class="list-group mt-1" style="font-size: smaller;">
                    <li class="list-group-item">
                      <div class="row no-gutters">
                        <div class="col-4 font-weight-bold">
                          NIK
                        </div>
                        <div class="col">
                          {{ $mustahik->nik_masked }}
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="row no-gutters">
                        <div class="col-4 font-weight-bold">
                          NRP
                        </div>
                        <div class="col">
                          {{ $mustahik->nrp ?? '-' }}
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="w-100 d-lg-none"></div>

            <div class="col">
              <ul class="list-group">
                <li class="list-group-item">
                  <div class="row">
                    <div class="col-lg-3 col-6 font-weight-bold">
                      Nama Lengkap
                    </div>
                    <div class="col">
                      {{ $mustahik->nama }}
                    </div>
                  </div>
                </li>

                <li class="list-group-item">
                  <div class="row">
                    <div class="col-lg-3 col-6 font-weight-bold">
                      Asnaf
                    </div>
                    <div class="col">
                      {{ $mustahik->asnaf_text }}
                    </div>
                  </div>
                </li>

                <li class="list-group-item">
                  <div class="row">
                    <div class="col-lg-3 col-6 font-weight-bold">
                      Kab./Kota
                    </div>
                    <div class="col">
                      {{ $mustahik->nama_kota }}
                    </div>
                  </div>
                </li>

                <li class="list-group-item">
                  <div class="row">
                    <div class="col-lg-3 col-6 font-weight-bold">
                      Provinsi
                    </div>
                    <div class="col">
                      {{ $mustahik->nama_provinsi }}
                    </div>
                  </div>
                </li>
              </ul>

              <div class="row my-3">
                <div class="col-lg-12 mx-auto">
                  <div class="card shadow h-100">
                    <div class="card-header">
                      <h3 class="mb-0">
                        Bantuan Diterima
                      </h3>
                    </div>
                    <div class="table-responsive" style="max-height: 52vh;">
                      <table class="table" id="table_bantuan">
                        <thead class="thead-light">
                          <tr>
                            <th>Tanggal</th>
                            <th>Bantuan</th>
                            <th class="text-right">Nominal</th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse ($mustahik->distribusi as $distribusi)
                            <tr>
                              <td>
                                {{ $distribusi->tanggal->isoFormat('MMM YYYY') }}
                              </td>
                              <td>
                                {{ $distribusi->item }}
                              </td>
                              <td class="text-right font-weight-bold">
                                {{ format_number($distribusi->nominal) }}
                              </td>
                            </tr>

                            @if ($loop->last)
                              <tr>
                                <th colspan="2" scope="row" class="font-weight-bold text-primary">
                                  Total
                                </th>
                                <td class="text-right font-weight-bold text-primary">
                                  {{ format_rupiah($mustahik->distribusi()->sum('nominal')) }}
                                </td>
                              </tr>
                            @endif
                          @empty
                            <tr>
                              <td colspan="3" class="text-center">
                                Belum ada data.
                              </td>
                            </tr>
                          @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push ('css')
<link rel="stylesheet" href="//unpkg.com/scroll-hint@latest/css/scroll-hint.css" crossorigin>
@endpush

@push ('js')
<script src="//unpkg.com/scroll-hint@latest/js/scroll-hint.min.js" crossorigin></script>
<script>
  table = $('#table_bantuan');
  table_width = table.width();
  wrapper_width = table.parent().parent().width();

  if (table_width / wrapper_width > 1.2) {
    new ScrollHint(table.closest('.table-responsive'), {
      i18n: { scrollable: 'Swipe' },
    });
  }
</script>
@endpush
