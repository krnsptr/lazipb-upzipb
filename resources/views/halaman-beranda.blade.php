@extends ('layouts.argon.dashboard')

@section ('content')

@include ('layouts.argon.inc.header-guest')

<div class="container mt--8 mb--1">
  <div class="row mt--2 justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">
        <div class="card-header bg-transparent">
          <h1 class="text-center">Masuk sebagai Muzaki</h1>
        </div>
        <div class="px-3 pt-3">
          @messages
        </div>
        <div class="card-body px-lg-5 pt-0 pb-0 text-center">
          <img src="{{ asset('assets/img/brand/favicon.png') }}" style="max-height: 100px;">

          <p class="my-3">
            Selamat datang di Unit Pengumpul Zakat IPB.
            Silakan masuk dengan Email IPB.
          </p>
        </div>
        <div class="card-footer bg-transparent text-center">
          <a href="{{ route('oauth.google.login') }}" class="btn btn-block btn-primary">
            Masuk
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push ('js')
  <script>
    $('.password-toggle').click(function() {
      inputGroup = $(this).closest('.input-group');
      input = $(inputGroup).find('input');
      input.attr('type', (input.attr('type') === 'text') ? 'password' : 'text');
      input.next().find('i').toggleClass('d-none');
    });
  </script>
@endpush
