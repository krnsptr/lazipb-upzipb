<nav id="navbar-main" class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark bg-primary">
  <div class="container px-4 px-lg-0">
    <!-- Brand -->
    <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/logo.png') }}" alt="{{ config('app.name') }}">
    </a>

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global"
      aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapse header -->
    <div class="navbar-collapse collapse" id="navbar_global">
      <div class="navbar-collapse-header">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/logo.png') }}" alt="{{ config('app.name') }}">
            </a>
          </div>

          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false"
              aria-label="Toggle navigation">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navbar items -->
      <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
        <li class="nav-item d-none">
          <a href="{{ route('anggota.dasbor') }}"
            class="nav-link @ifroute('anggota.dasbor') active @endifroute">
            <i class="fas fa-columns fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Dasbor</span>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('anggota.donasi') }}"
            class="nav-link @ifroute('anggota.donasi') active @endifroute">
            <i class="fas fa-hand-holding-heart fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Laporan Donasi</span>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('anggota.distribusi') }}"
            class="nav-link @ifroute('anggota.distribusi') active @endifroute">
            <i class="fas fa-cubes fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Laporan Distribusi</span>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('anggota.profil') }}"
            class="nav-link @ifroute('anggota.profil') active @endifroute">
            <i class="fas fa-user fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Profil Saya</span>
          </a>
        </li>

        <li class="nav-item">
          <a href="https://masjid.ipb.ac.id/?page_id=5119" target="_blank"
            class="nav-link">
            <i class="fas fa-mosque fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Profil UPZ</span>
          </a>
        </li>

        <li class="nav-item d-md-none">
          <a href="{{ route('anggota.keluar') }}"
            class="nav-link @ifroute('anggota.keluar') active @endifroute"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span class="nav-link-inner--text">Keluar</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav align-items-center d-none d-md-flex ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="{{ auth('anggota')->user()->nama }}"
                  src="{{ asset('assets/img/user.png') }}">
              </span>
              <div class="media-body ml-2 d-none d-lg-block">
                <span class="mb-0 text-sm font-weight-bold">
                  {{ auth('anggota')->user()->nama }}
                </span>
              </div>
            </div>
          </a>

          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <a href="{{ route('anggota.keluar') }}" class="dropdown-item"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
              <span>Keluar</span>
            </a>
          </div>
        </li>
      </ul>

      <form method="post" action="{{ route('anggota.keluar') }}" id="logout-form"
        style="display: none;">
        @csrf
      </form>
    </div>
  </div>
</nav>
