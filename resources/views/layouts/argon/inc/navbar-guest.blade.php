<nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark bg-primary">
  <div class="container px-4">
    <!-- Brand -->
    <a class="navbar-brand" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/logo.png') }}" />
    </a>

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse"
      data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapse header -->
    <div class="collapse navbar-collapse" id="navbar-collapse-main">
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/logo.png') }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
              aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navbar items -->
      <ul class="navbar-nav ml-auto">
        @if (!app()->isProduction() && !\setting('_auth_disabled'))
          <li class="nav-item">
            <a href="{{ route('anggota.masuk') }}"
              class="nav-link nav-link-icon @ifroute('anggota.masuk') active @endifroute">
              <i class="fas fa-sign-in-alt fa-fw mr-1"></i>
              <span class="nav-link-inner--text">Masuk</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('anggota.daftar') }}"
              class="nav-link nav-link-icon @ifroute('anggota.daftar') active @endifroute">
              <i class="fas fa-user-plus fa-fw mr-1"></i>
              <span class="nav-link-inner--text">Daftar</span>
            </a>
          </li>
        @else
          @push('js')
            <script>
              $('.navbar-top .navbar-toggler').hide();
            </script>
          @endpush
        @endif
      </ul>
    </div>
  </div>
</nav>
