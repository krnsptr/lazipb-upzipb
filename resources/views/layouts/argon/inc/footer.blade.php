<div class="container-fluid mt-auto">
  <footer class="footer py-3">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6 my-1">
        <div class="copyright text-center text-xl-left text-muted">
          &copy; {{ date('Y') }}
          <a href="{{ url('/') }}" target="_blank">
            {{ config('app.name') }}
          </a>
        </div>
      </div>
      <div class="col-xl-6 my-1">
        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
          <li class="nav-item text-sm">
            {{-- Dikembangkan oleh Ahawebs --}}
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>
