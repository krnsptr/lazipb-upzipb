<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
  <div class="container-fluid">

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse"
      data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Brand -->
    <a class="navbar-brand py-0" href="{{ url('/') }}">
      <img src="{{ asset('assets/img/brand/logo.png') }}" class="navbar-brand-img"
        alt="{{ config('app.name') }}">
    </a>

    <!-- User -->
    <ul class="nav align-items-center d-md-none">
      <li class="nav-item dropdown">
        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <div class="media align-items-center">
            <span class="avatar avatar-sm rounded-circle">
              <img alt="{{ auth('admin')->user()->nama }}"
                src="{{ asset('assets/img/user.png') }}"
                class="p-1 bg-secondary">
            </span>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
          <a href="{{ route('admin.keluar') }}" class="dropdown-item"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt fa-fw mr-1"></i>
            <span>Keluar</span>
          </a>
        </div>
      </li>
    </ul>

    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('/') }}">
              <img src="{{ asset('assets/img/brand/logo.png') }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse"
              data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
              aria-label="Toggle sidenav">
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>

      <!-- Navigation -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link @ifroute('admin.dasbor') active @endifroute"
            href="{{ route('admin.dasbor') }}">
            <i class="fas fa-columns fa-fw mr-1"></i> Dasbor
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.donasi') active @endifroute"
            href="{{ route('admin.donasi') }}">
            <i class="fas fa-hand-holding-heart fa-fw mr-1"></i> Donasi
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.distribusi') active @endifroute"
            href="{{ route('admin.distribusi') }}">
            <i class="fas fa-cubes fa-fw mr-1"></i> Distribusi
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.muzaki') active @endifroute"
            href="{{ route('admin.muzaki') }}">
            <i class="fas fa-user-tie fa-fw mr-1"></i> Muzaki
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.mustahik') active @endifroute"
            href="{{ route('admin.mustahik') }}">
            <i class="fas fa-user-shield fa-fw mr-1"></i> Mustahik
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.pengaturan') active @endifroute"
            href="{{ route('admin.pengaturan') }}">
            <i class="fas fa-cog fa-fw mr-1"></i> Pengaturan
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link @ifroute('admin.log') active @endifroute"
            href="{{ route('admin.log') }}">
            <i class="fas fa-clipboard-list fa-fw mr-1"></i> Log Admin
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
