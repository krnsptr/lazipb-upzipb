<!doctype html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {!! SEO::generate() !!}

  <link href="{{ asset('assets/img/brand/favicon.png') }}" rel="icon" type="image/png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
    identity="sha384-DAJaoFM+NLp3k3GoMHDDu5sPgje1j1W28zhdM3bOIgdbOGOX50oQ3zhhpQlEe0EO"
    crossorigin="anonymous">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
    crossorigin="anonymous">

  <link rel="stylesheet" href="{{ asset('skins/argon/vendor/nucleo/css/nucleo.css') }}">
  <link rel="stylesheet" href="{{ asset('skins/argon/css/argon-dashboard.css?v=1.1.0') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css?v=2') }}">

  <style>
    .navbar {
      padding-top: 5px;
      padding-bottom: 5px;
    }

    .navbar-brand img {
      max-height: 50px;
      padding-top: 3px;
      padding-bottom: 3px;
    }
  </style>
</head>

<body class="{{ $body_class ?? '' }}">
  @guest ('anggota')
    @auth ('admin')
      @include ('layouts.argon.inc.sidebar-admin')
    @endauth
  @endguest

  <div class="main-content d-flex flex-column" style="min-height: 100vh;" id="app">
    @auth ('anggota')
      @include ('layouts.argon.inc.navbar-anggota')
    @endauth

    @guest ('anggota')
      @auth ('admin')
        @include ('layouts.argon.inc.navbar-admin')
      @endauth

      @guest
        @include ('layouts.argon.inc.navbar-guest')
      @endguest
    @endguest

    @yield ('content')

    @include ('layouts.argon.inc.footer')
  </div>

  @stack ('css')

  <script src="{{ asset('skins/argon/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('skins/argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('skins/argon/js/argon-dashboard.js?v=1.1.0') }}"></script>

  <script>
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      if (originalOptions.type === 'POST' || options.type === 'POST') {
        options.beforeSend = function (xhr) {
         xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        };
      }
    });
  </script>

  @stack ('js')
</body>

</html>
