@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Edit Donasi: #{{ $donasi->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          @push('js')
            <script>
              $('.form-control.number').addClass('text-right text-monospace');
              $('.form-control.number').css('-moz-appearance', 'textfield');
              $('#jumlah').addClass('text-primary').css('font-size', 'larger');

              $('#zakat, #infak, #santunan, #wakaf').change(function () {
                jumlah = 0;

                $('#zakat, #infak, #santunan, #wakaf').each(function () {
                  jumlah += Number($(this).val());
                });

                $('#jumlah').val(jumlah);
              }).change();
            </script>
          @endpush

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $donasi->muzaki->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nip" name="nip"
                      value="{{ $donasi->muzaki->nip }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Tanggal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control bg-white" id="tanggal" name="tanggal"
                      value="{{ $donasi->tanggal }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Status
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" name="status" id="status" required>
                      <option value=""></option>
                      @foreach (App\Enums\DonasiStatus::asSelectArray() as $key => $value)
                        <option value="{{ $key }}" @if ($donasi->status === $key) selected @endif>
                          {{ $value }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Zakat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="zakat" name="zakat"
                      value="{{ $donasi->zakat }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Infak
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="infak" name="infak"
                      value="{{ $donasi->infak }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Santunan Yatim
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="santunan"
                      name="santunan" value="{{ $donasi->santunan }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Wakaf Tunai
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="wakaf"
                      name="wakaf" value="{{ $donasi->wakaf }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Jumlah
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="number" class="form-control number" id="jumlah"
                      name="jumlah" value="{{ $donasi->jumlah }}" required readonly>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Keterangan
                  </label>
                  <div class="col-md-7 my-2">
                    <textarea class="form-control" name="keterangan" id="keterangan"
                      rows="5">{{ $donasi->keterangan }}</textarea>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group row">
                  <label class="col-md-5 col-form-label text-md-right font-weight-bold">
                    Link kuitansi
                  </label>
                  <div class="col-md-7 my-2">
                    <input type="url" class="form-control" id="link_kuitansi" name="link_kuitansi"
                      value="{{ $donasi->link_kuitansi }}">
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <button type="submit" class="btn btn-block btn-primary">
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css" crossorigin>
@endpush

@push ('js')
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js" crossorigin></script>

<script>
  flatpickr('#tanggal', {
    enableTime: true,
    enableSeconds: true,
    dateFormat: 'Y-m-d H:i:S',
    time_24hr: true,
  });
</script>
@endpush
