@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Muzaki: #{{ $muzaki->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama Lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nama" name="nama"
                      value="{{ $muzaki->nama }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nip" name="nip"
                      value="{{ $muzaki->nip }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NPWP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="npwp" name="npwp"
                      value="{{ $muzaki->npwp ?? '-' }}" disabled>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat Email
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="email" class="form-control-plaintext" id="email" name="email"
                      value="{{ $muzaki->email ?? '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor Ponsel
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nomor_hp" name="nomor_hp"
                      value="{{ $muzaki->nomor_hp ?? '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor WhatsApp
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nomor_wa" name="nomor_wa"
                      value="{{ $muzaki->nomor_wa ?? '-' }}" disabled>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor Rekening
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control-plaintext" id="nomor_rekening"
                      name="nomor_rekening" value="{{ $muzaki->nomor_rekening ?? '-' }}" disabled>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.muzaki_edit', $muzaki) }}" class="btn btn-block btn-dark">
                  Edit
                </a>
              </div>

              @if ($muzaki->anggota)
                <div class="col-lg-3 col-md-4 mx-auto my-1">
                  <a href="{{ route('admin.muzaki_masuk', $muzaki) }}" class="btn btn-block btn-primary">
                    Masuk
                  </a>
                </div>
              @endif

              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
