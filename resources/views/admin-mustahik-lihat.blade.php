@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Mustahik: #{{ $mustahik->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Nama Lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nama }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    NIK
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nik }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    NRP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nrp ?? '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Asnaf
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <ul>
                      @foreach ($mustahik->asnaf_enum->getFlags() as $flag)
                        <li>{{ $flag }}</li>
                      @endforeach
                    </ul>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Nomor Ponsel
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nomor_hp ?? '-' }}
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Alamat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->alamat ?? '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Provinsi
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nama_provinsi ?? '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Kota/Kab.
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nama_kota ?? '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Kecamatan
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nama_kecamatan ?? '-' }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Kel./Desa
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $mustahik->nama_kelurahan ?? '-' }}
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Foto
                  </label>
                  <div class="col-md-8 col-xl-7 py-3">
                    <img src="{{ $mustahik->foto_url }}" class="border shadow mw-100"
                      style="max-height: 150px;">
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.mustahik_edit', $mustahik) }}" class="btn btn-block btn-dark">
                  Edit
                </a>
              </div>

              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <br>

  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h3 class="mb-0">
            Bantuan Diterima
          </h3>
        </div>
        <div class="table-responsive" style="max-height: 52vh;">
          <table class="table" id="table_bantuan">
            <thead class="thead-light">
              <tr>
                <th>Tanggal</th>
                <th>Bantuan</th>
                <th class="text-right">Nominal</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($mustahik->distribusi->sortBy('tanggal') as $distribusi)
                <tr>
                  <td>
                    {{ $distribusi->tanggal->isoFormat('MMM YYYY') }}
                  </td>
                  <td>
                    {{ $distribusi->item }}
                  </td>
                  <td class="text-right font-weight-bold">
                    {{ format_number($distribusi->nominal) }}
                  </td>
                </tr>
                @if ($loop->last)
                  <tr>
                    <th colspan="2" scope="row" class="font-weight-bold text-primary">
                      Total
                    </th>
                    <td class="text-right font-weight-bold text-primary">
                      {{ format_rupiah($mustahik->distribusi()->sum('nominal')) }}
                    </td>
                  </tr>
                @endif
              @empty
                <tr>
                  <td colspan="3" class="text-center">
                    Belum ada data.
                  </td>
                </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
