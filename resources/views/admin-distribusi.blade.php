@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header bg-gradient-primary py-3 pt-md-6">
  <div class="container-fluid">
    <div class="row">
      <div class="col px-1 px-md-0">
        <div class="card shadow">
          <div class="card-header">
            <h3 class="mb-0">
              Distribusi

              <span class="float-right">
                <a href="#ekspor_modal" class="btn btn-sm btn-secondary" data-toggle="modal">
                  Ekspor
                </a>

                <a href="{{ route('admin.distribusi_impor') }}" class="btn btn-sm btn-dark">
                  Impor
                </a>
              </span>
            </h3>
          </div>
          <div class="px-3">
            @messages
          </div>
          <div class="table-responsive">
            <table class="table datatables" id="table">
              <thead class="thead-light">
                <tr>
                  <th>Tanggal</th>
                  <th>Item</th>
                  <th style="min-width: 5rem;">Dana</th>
                  <th>Mustahik</th>
                  <th style="min-width: 5rem;">Asnaf</th>
                  <th style="min-width: 5rem;">Bidang</th>
                  <th style="min-width: 5rem;">SDG</th>
                  <th>Nominal</th>
                  <th style="min-width: 5rem;">ID</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="ekspor_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title title">Ekspor Distribusi</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
              Periode
            </label>
            <div class="col-md-8 col-xl-7">
              <select class="form-control" id="periode" autocomplete="off">
                {{-- <option value="">Semua</option> --}}
                <option value="6month">6 Bulan Terakhir</option>
                <option value="12month">12 Bulan Terakhir</option>
                <option value="custom">Pilih Tanggal</option>
                @foreach ($years as $year)
                  <option value="{{ $year }}">
                    Tahun {{ $year }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
            Tanggal
          </label>
          <div class="col-md-8 col-xl-7">
            <input type="text" class="form-control" id="tanggal" autocomplete="off" disabled>
          </div>
        </div>

        <br>

        <div class="row">
          <div class="col-lg-6 col-md-8 mx-auto my-1">
            <button type="button" class="btn btn-block btn-secondary" id="ekspor_button">
              Ekspor
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="hapus_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title title">Hapus Distribusi <span class="id_text"></span></h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="hapus_form" method="post" autocomplete="off">
          @csrf

          <h4 class="text-danger">
            Aksi ini bersifat permanen (tidak dapat diurungkan).
          </h4>

          <p>Masukkan kata sandi untuk melanjutkan.</p>

          <div class="form-group row">
            <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
              Password
            </label>
            <div class="col-md-8 col-xl-7">
              <input type="password" class="form-control" id="password" name="password" required>
            </div>
          </div>

          <br>

          <div class="row">
            <div class="col-lg-6 col-md-8 mx-auto my-1">
              <button type="submit" class="btn btn-block btn-danger" id="ekspor_button">
                Hapus
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
<link href="//cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.css" crossorigin>
<style>
  .w-6rem {
    width: 6rem !important;
  }
</style>
@endpush

@push ('js')
<script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.4/jquery.dataTables.yadcf.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.35/dayjs.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/flatpickr.min.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.6/dist/l10n/id.js" crossorigin></script>
<script src="{{ asset('assets/js/datatables.extend.js') }}"></script>
<script>
  mapper = {};
  mapper['dana'] = @json(App\Enums\Dana::asSelectArray());
  mapper['asnaf'] = @json(App\Enums\Asnaf::asSelectArray());
  mapper['bidang'] = @json(App\Enums\Bidang::asSelectArray());
  mapper['sdg'] = @json(App\Enums\Sdg::asSelectArray());

  render = {};
  render['dana'] = function (data) { return mapper['dana'][data]; };
  render['asnaf'] = function (data) { return mapper['asnaf'][data]; };
  render['bidang'] = function (data) { return mapper['bidang'][data]; };
  render['sdg'] = function (data) { return mapper['sdg'][data]; };

  render['tanggal'] = function (data) {
    if (!dayjs(data).isValid()) return '';
    return dayjs(data).format('YYYY-MM-DD');
  }

  filter = {};
  filter['dana'] = [];
  filter['asnaf'] = [];
  filter['bidang'] = [];
  filter['sdg'] = [];

  for (f in filter) {
    for (key in mapper[f]) {
      filter[f].push({
        value: key,
        label: mapper[f][key]
      });
    }
  }
</script>
<script>
  aksi = '<button type="button" onclick="lihat(this)" class="btn btn-sm btn-primary">Lihat</button>';
  aksi += '<button type="button" onclick="hapus(this)" class="btn btn-sm btn-danger">Hapus</button>';

  function lihat(button) {
    id = $(button).closest('tr').attr('id');
    window.open(@json(route('admin.distribusi_lihat', '')) + '/' + id);
  }

  function hapus(button) {
    id = $(button).closest('tr').attr('id');
    $('.id_text').text(': #' + id);
    $('#hapus_form').attr('action', @json(route('admin.distribusi_hapus', '')) + '/' + id);
    $('#hapus_modal').modal('show');
  }

  table = $('#table').DataTable({
    columns: [
      {
        data: 'tanggal',
        name: 'tanggal',
        orderable: true,
        searchable: true,
        render: render['tanggal'],
      },
      {
        data: 'item',
        name: 'item',
        searchable: true,
      },
      {
        data: 'dana',
        name: 'dana',
        orderable: true,
        searchable: true,
        defaultContent: '-',
        render: render['dana'],
      },
      {
        data: 'mustahik_wd.nama',
        name: 'mustahik_wd.nama',
        defaultContent: '-',
        searchable: true,
      },
      {
        data: 'asnaf',
        name: 'asnaf',
        orderable: true,
        searchable: true,
        defaultContent: '-',
        render: render['asnaf'],
      },
      {
        data: 'bidang',
        name: 'bidang',
        orderable: true,
        searchable: true,
        defaultContent: '-',
        render: render['bidang'],
      },
      {
        data: 'sdg',
        name: 'sdg',
        orderable: true,
        searchable: true,
        defaultContent: '-',
        render: render['sdg'],
      },
      {
        data: 'nominal',
        name: 'nominal',
        className: 'text-right text-primary font-weight-bold',
        render: $.fn.dataTable.render.number(' ', '.', 0),
      },
      {
        data: 'id',
        name: 'id',
        orderable: true,
        searchable: true,
      },
      {
        data: null,
        defaultContent: aksi,
        searchable: false,
        orderable: false,
      },
    ],
    order: [
      [0, 'desc'],
    ],
  });

  yadcf.init(table, [
    {
      column_number: 0,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm w-6rem mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 1,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 2,
      filter_type: 'select',
      filter_match_mode: 'exact',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      data: filter['dana'],
    },
    {
      column_number: 3,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      filter_delay: 1000,
    },
    {
      column_number: 4,
      filter_type: 'select',
      filter_match_mode: 'exact',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      data: filter['asnaf'],
    },
    {
      column_number: 5,
      filter_type: 'select',
      filter_match_mode: 'exact',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      data: filter['bidang'],
    },
    {
      column_number: 6,
      filter_type: 'select',
      filter_match_mode: 'exact',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm mt-1',
      data: filter['sdg'],
    },
    {
      column_number: 8,
      filter_type: 'text',
      filter_reset_button_text: false,
      filter_default_label: '',
      style_class: 'form-control form-control-sm w-4rem mt-1',
      filter_delay: 1000,
      filter_match_mode: 'exact',
    },
  ]);
</script>

<script>
  tanggal_min = dayjs(null);
  tanggal_max = dayjs(null);

  flatpickr.localize(flatpickr.l10ns.id);

  fp_tanggal = flatpickr('#tanggal', {
    dateFormat: 'd-m-Y',
    mode: 'range',
    disableMobile: true,
  });

  $('#tanggal').change(function () {
    if ($('#periode').val() !== '') {
      $('#periode > option[value=custom]').prop('selected', true);

      tanggal_min = dayjs(fp_tanggal.selectedDates[0] || null).startOf('day');
      tanggal_max = dayjs(fp_tanggal.selectedDates[1] || null).endOf('day');
    }
  });

  $('#periode').change(function () {
    periode = $(this).val();

    if (periode === '') {
      tanggal_min = dayjs(null);
      tanggal_max = dayjs(null);
    } else if (periode === '6month') {
      tanggal_min = dayjs().subtract(6, 'month').startOf('day');
      tanggal_max = dayjs();
    } else if (periode === '12month') {
      tanggal_min = dayjs().subtract(12, 'month').startOf('day');
      tanggal_max = dayjs();
    } else if (periode === 'custom') {
      tanggal_min = tanggal_min.isValid() ? tanggal_min : dayjs().subtract(12, 'month').startOf('day');
      tanggal_max = tanggal_max.isValid() ? tanggal_max : dayjs();
    } else {
      tanggal_min = dayjs().year(periode).startOf('year');
      tanggal_max = dayjs().year(periode).endOf('year');
    }

    if (tanggal_min.isValid() && tanggal_max.isValid()) {
      fp_tanggal.setDate([tanggal_min.toDate(), tanggal_max.toDate()]);
      $('#tanggal').prop('disabled', false).addClass('bg-white');
    } else {
      fp_tanggal.clear();
      $('#tanggal').prop('disabled', true).removeClass('bg-white');
    }
  }).change();

  $('#ekspor_modal').on('hide.bs.modal', function (e) {
    if ($('.flatpickr-calendar').is(':visible')) {
      console.log('focus');
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  });

  $('#ekspor_button').click(function () {
    url = new URL(@json(route('admin.distribusi_ekspor')));

    data = {
      tanggal_min: tanggal_min.isValid() ? tanggal_min.format() : '',
      tanggal_max: tanggal_max.isValid() ? tanggal_max.format() : '',
    };

    url.search = new URLSearchParams(data);
    location.href = url;

    $('#ekspor_modal').modal('hide');
  });
</script>
@endpush
