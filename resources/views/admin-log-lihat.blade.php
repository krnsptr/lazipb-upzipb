@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Log Admin: #{{ $log->id }}
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Tanggal
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $log->tanggal }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Aksi
                  </label>
                  <div class="col-md-8 col-xl-7 text-monospace">
                    {{ $log->aksi }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Jumlah
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $log->jumlah }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Input
                  </label>
                  <div class="col-md-8 col-xl-7">
                    {{ $log->input }}
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Output
                  </label>
                  @php
                    $output = json_decode($log->output);
                  @endphp
                  <div class="col-md-8 col-xl-7">
                    @if (is_object($output) || is_array($output))
                      <ul>
                        @foreach ((array) $output as $key => $value)
                          <li>
                            {{ $key }}:
                            {{ $value }}
                          </li>
                        @endforeach
                      </ul>
                    @else
                      {{ $output }}
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 text-md-right font-weight-bold">
                    Detail
                  </label>
                  @php
                    $detail = $log->detail;
                  @endphp
                  <div class="col-md-8 col-xl-7">
                    @if (is_object($detail) || is_array($detail))
                      <ul>
                        @foreach ((array) $detail as $key => $value)
                          <li>
                            ({{ $value }})
                            {{ $key }}
                          </li>
                        @endforeach
                      </ul>
                    @else
                      {{ $detail }}
                    @endif
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <a href="{{ route('admin.dasbor') }}" class="btn btn-block btn-secondary">
                  Kembali
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
