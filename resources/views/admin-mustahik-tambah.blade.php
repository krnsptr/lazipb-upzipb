@extends ('layouts.argon.dashboard')

@section ('content')
<div class="header pb-6 pb-xl-5 pt-8 d-flex align-items-center">
  <span class="mask bg-gradient-primary opacity-8"></span>
</div>
<div class="container-fluid mt--7 mt-lg--6 mb-3">
  <div class="row">
    <div class="col-12 mx-auto">
      <div class="card shadow h-100">
        <div class="card-header">
          <h2 class="mb-0">
            Tambah Mustahik
          </h2>
        </div>

        <div class="card-body">
          @messages

          <form autocomplete="off" method="post" action="{{ url()->current() }}" id="form">
            @csrf
            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nama Lengkap
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nama" name="nama"
                      value="{{ old('nama') }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NIK
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nik" name="nik"
                      value="{{ old('nik') }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    NRP
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="nrp" name="nrp"
                      value="{{ old('nrp') }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Asnaf
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="asnaf" name="asnaf"
                      value="{{ old('asnaf') }}" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Nomor Ponsel
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="tel" class="form-control" id="nomor_hp" name="nomor_hp"
                      value="{{ old('nomor_hp') }}">
                  </div>
                </div>
              </div>

              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Provinsi
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_provinsi" name="id_provinsi"></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kota/Kab.
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_kota" name="id_kota"></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kecamatan
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_kecamatan" name="id_kecamatan"></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Kel./Desa
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <select class="form-control" id="id_kelurahan" name="id_kelurahan"></select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Alamat
                  </label>
                  <div class="col-md-8 col-xl-7">
                    <input type="text" class="form-control" id="alamat" name="alamat"
                      value="{{ old('alamat') }}">
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row no-gutters">
              <div class="col-xl-6 mb-3 d-flex flex-column pb-1">
                <div class="form-group row">
                  <label class="col-md-4 col-xl-5 col-form-label text-md-right font-weight-bold">
                    Foto
                  </label>
                  <div class="col-md-8 col-xl-7 py-2">
                    <img src="{{ $mustahik->foto_url }}" class="d-block border shadow mw-100 m-2"
                      id="foto-img" style="max-height: 150px;">
                    <button type="button" class="btn btn-secondary mt-2" data-toggle="modal"
                      data-target="#foto-edit-modal">
                      Edit
                    </button>
                    <button type="button" class="btn btn-danger mt-2" onclick="hapusFoto();">
                      Hapus
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <hr class="mt-0">

            <div class="row">
              <div class="col-lg-3 col-md-4 mx-auto my-1">
                <button type="submit" class="btn btn-block btn-success">
                  Tambah
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="foto-edit-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title title">Edit Foto</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <label>Foto</label><br>
        <input type="file" accept="image/*" id="foto-input" autocomplete="off">
        <div id="croppie" class="mx-auto" style="height: 200px; display: none;"></div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
          Batal
        </button>
        <button type="button" class="btn btn-primary" id="foto-save-button" disabled>
          Simpan
        </button>
      </div>
    </div>
  </div>
</div>
@endsection

@push ('css')
<link href="//cdn.jsdelivr.net/npm/croppie@2.6.5/croppie.min.css" rel="stylesheet" crossorigin>
<link href="//unpkg.com/selectize-bootstrap4-theme@2.0.2/dist/css/selectize.bootstrap4.css"
  rel="stylesheet" crossorigin>
<link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" crossorigin>
<link href="{{ asset('assets/css/style-select2.css') }}" rel="stylesheet">
@endpush

@push ('js')
<script src="//cdn.jsdelivr.net/npm/croppie@2.6.5/croppie.min.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9" crossorigin></script>
<script src="//unpkg.com/selectize@0.12.6/dist/js/standalone/selectize.min.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js" crossorigin></script>
<script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/i18n/id.js" crossorigin></script>
<script src="{{ asset('assets/js/select2-cascade.js') }}"></script>

<script src="//cdn.jsdelivr.net/npm/imask@6.0.5/dist/imask.min.js" crossorigin></script>
<script>
  IMask($('#nomor_hp')[0], {
    mask: '{+62}800000000[000]',
    lazy: false,
  });
</script>

<script>
  asnaf = @json($mustahik->asnaf_text);

  asnafList = @json(App\Enums\Asnaf::getKeys());

  asnafOptions = $.map(asnafList, function (item) {
    return { value: item, text: item };
  });

  $('#asnaf').selectize({
    options: asnafOptions,
    delimiter: ', ',
  });
</script>

<script>
  $.fn.select2.defaults.set('language', 'id');

  $.ajax({
    url: @json(route('api.web.provinsi')),
    dataType: 'json',
    success: function(data) {
      id_provinsi = @json((string) ($mustahik->id_provinsi ?? old('id_provinsi')));

      $('#id_provinsi').select2({ data: data }).val(id_provinsi).change();
      $('#id_kota').select2();
      $('#id_kecamatan').select2();
      $('#id_kelurahan').select2();
    }
  });

  new Select2Cascade(
    $('#id_provinsi'),
    $('#id_kota'),
    @json(route('api.web.kota')) + '/?id_provinsi=:parentId:'
  ).then(function(parent, child) {
    setTimeout(function() {
      id_kota = @json((string) ($mustahik->id_kota ?? old('id_kota')));
      child.val(id_kota).change();
    }, 200);
  });

  new Select2Cascade(
    $('#id_kota'),
    $('#id_kecamatan'),
    @json(route('api.web.kecamatan')) + '/?id_kota=:parentId:'
  ).then(function(parent, child) {
    setTimeout(function() {
      id_kecamatan = @json((string) ($mustahik->id_kecamatan ?? old('id_kecamatan')));
      child.val(id_kecamatan).change();
    }, 200);
  });

  new Select2Cascade(
    $('#id_kecamatan'),
    $('#id_kelurahan'),
    @json(route('api.web.kelurahan')) + '/?id_kecamatan=:parentId:'
  ).then(function(parent, child) {
    setTimeout(function() {
      id_kelurahan = @json((string) ($mustahik->id_kelurahan ?? old('id_kelurahan')));
      child.val(id_kelurahan).change();
    }, 200);
  });
</script>

<script>
  var foto_default = @json($mustahik::make()->foto_url);

  var picture;

  function hapusFoto() {
    $('#foto-img').attr('src', foto_default);
    picture = '';
  }

  var myCroppie = $('#croppie').croppie({
    viewport: {
      width: 200,
      height: 200,
      type: 'circle',
    },
  });

  $('#foto-input').change(function() {
    myCroppie.croppie('bind', {
      url: window.URL.createObjectURL(this.files[0])
    });

    $('#croppie').show();
    $('#foto-save-button').prop('disabled', false);
  });

  $('#foto-save-button').click(function() {
    myCroppie.croppie('result', {
      type: 'blob',
      circle: false,
      format: 'jpeg',
    }).then(function(blob) {
      picture = blob;
      $('#foto-img').attr('src', window.URL.createObjectURL(blob));
      $('#foto-edit-modal').modal('hide');
    });
  });

  $('#form').submit(function() {
    var form = this;

    Swal.fire({
      title: 'Mengirim',
      text: 'Harap tunggu',
      allowOutsideClick: function() {
        return !swal.isLoading()
      },
      onOpen: function() {
        try {
          Swal.showLoading();

          var formData = new FormData(form);

          if (picture !== undefined) {
            if (picture === '') {
              formData.append('foto', '');
              formData.append('hapus_foto', 1);
            } else {
              formData.append('foto', picture, 'foto.jpeg');
            }
          }

          $.ajax({
            url: $(form).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
              Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: data.message,
              }).then(function() {
                location.href = data.redirect;
              });
            },
            error: function(xhr) {
              json = JSON.parse(xhr.responseText);

              if (json.errors != undefined) {
                text = '';
                $.each(json.errors, function(i, input) {
                  $.each(input, function(i, message) {
                    text += message + '<br>';
                  });
                });
              } else {
                text = json.message || 'Terjadi kesalahan.';
              }

              Swal.fire({
                icon: 'error',
                title: 'Gagal',
                html: text,
              });
            },
          });
        } catch (err) {
          Swal.fire({
            icon: 'error',
            title: 'Gagal',
            html: err,
          });
        }
      }
    });

    return false;
  });
</script>
@endpush
