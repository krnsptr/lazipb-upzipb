<?php

namespace App\Exceptions;

use Illuminate\Http\Exceptions\ThrottleRequestsException;

class ThrottleEmailException extends ThrottleRequestsException
{
    public function render()
    {
        return \response()->view('errors.429-email', [], 429);
    }
}
