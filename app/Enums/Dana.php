<?php

namespace App\Enums;

use App\Enum;

final class Dana extends Enum
{
    public const Zakat              = 1;
    public const Infak              = 2;
    public const Santunan           = 3;
    public const Wakaf              = 4;

    public const DETAIL = [
        self::Zakat => ['text' => 'Zakat'],
        self::Infak => ['text' => 'Infak'],
        self::Santunan => ['text' => 'Santunan Yatim'],
        self::Wakaf => ['text' => 'Wakaf Tunai'],
    ];

    /**
     * Get patterns for coercing.
     *
     * @return array
     */
    protected static function patterns(): array
    {
        return [
            '/^za/i' => self::Zakat,
            '/^in/i' => self::Infak,
            '/^sa/i' => self::Santunan,
            '/^wa/i' => self::Wakaf,
        ];
    }
}
