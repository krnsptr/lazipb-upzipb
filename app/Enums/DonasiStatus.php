<?php

namespace App\Enums;

use App\Enum;

final class DonasiStatus extends Enum
{
    public const AutodebitProses = 1;
    public const AutodebitBerhasil = 2;
    public const AutodebitGagal = 3;
    public const AutodebitBelumNisab = 4;
    public const BelumTerdaftar = 5;
    public const TransferRekening = 6;
    public const TunaiKasir = 7;

    public const DETAIL = [
        self::AutodebitProses => ['text' => 'Otodebet Akan Diproses', 'color' => 'orange'],
        self::AutodebitBerhasil => ['text' => 'Otodebet Berhasil'],
        self::AutodebitGagal => ['text' => 'Otodebet Gagal', 'color' => 'red'],
        self::AutodebitBelumNisab => ['text' => 'Otodebet Belum Nisab'],
        self::BelumTerdaftar => ['text' => 'Belum Terdaftar'],
        self::TransferRekening => ['text' => 'Transfer Rekening'],
        self::TunaiKasir => ['text' => 'Tunai Kasir'],
    ];

    public static function Berhasil(): array
    {
        return [
            self::AutodebitBerhasil,
            self::TransferRekening,
            self::TunaiKasir,
        ];
    }

    public static function getColors(): array
    {
        return \collect(self::getInstances())->mapWithKeys(function (self $status) {
            return [$status->value => $status->getColor()];
        })->toArray();
    }

    public function getColor(): string
    {
        return $this->detail['color'] ?? '';
    }
}
