<?php

namespace App\Enums;

use App\Enum;

final class Sdg extends Enum
{
    public const SDG_01 = 1;
    public const SDG_02 = 2;
    public const SDG_03 = 3;
    public const SDG_04 = 4;
    public const SDG_05 = 5;
    public const SDG_06 = 6;
    public const SDG_07 = 7;
    public const SDG_08 = 8;
    public const SDG_09 = 9;
    public const SDG_10 = 10;
    public const SDG_11 = 11;
    public const SDG_12 = 12;
    public const SDG_13 = 13;
    public const SDG_14 = 14;
    public const SDG_15 = 15;
    public const SDG_16 = 16;
    public const SDG_17 = 17;

    public const DETAIL = [
        1 => ['title' => 'Tanpa Kemiskinan'],
        2 => ['title' => 'Tanpa Kelaparan'],
        3 => ['title' => 'Kehidupan Sehat dan Sejahtera'],
        4 => ['title' => 'Pendidikan Berkualitas'],
        5 => ['title' => 'Kesetaraan Gender'],
        6 => ['title' => 'Air Bersih dan Sanitasi Layak'],
        7 => ['title' => 'Energi Bersih dan Terjangkau'],
        8 => ['title' => 'Pekerjaan Layak dan Pertumbuhan Ekonomi'],
        9 => ['title' => 'Industri, Inovasi, dan Infrastruktur'],
        10 => ['title' => 'Berkurangnya Kesenjangan'],
        11 => ['title' => 'Kota dan Pemukiman yang Berkelanjutan'],
        12 => ['title' => 'Konsumsi dan Produksi yang Bertanggung Jawab'],
        13 => ['title' => 'Penanganan Perubahan Iklim'],
        14 => ['title' => 'Ekosistem Lautan'],
        15 => ['title' => 'Ekosistem Daratan'],
        16 => ['title' => 'Perdamaian, Keadlian, dan Kelembagaan yang Tangguh'],
        17 => ['title' => 'Kemitraan untuk Mencapai Tujuan'],
    ];

    public function getText(): string
    {
        return sprintf("%02d. %s", $this->value, $this->detail['title']);
    }

    public function getLink(): string
    {
        return sprintf(
            '<a href="http://sdgs.bappenas.go.id/tujuan-%d" target="_blank">%s</a>',
            $this->value,
            $this->getText(),
        );
    }
}
