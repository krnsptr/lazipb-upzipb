<?php

namespace App\Enums;

use App\EnumFlagged;

final class Asnaf extends EnumFlagged
{
    public const FakirMiskin    = self::Fakir | self::Miskin;
    public const Fakir          = 1 << (1 - 1);
    public const Miskin         = 1 << (2 - 1);
    public const Amil           = 1 << (3 - 1);
    public const Mualaf         = 1 << (4 - 1);
    public const Riqab          = 1 << (5 - 1);
    public const Gharimin       = 1 << (6 - 1);
    public const Fisabilillah   = 1 << (7 - 1);
    public const Ibnusabil      = 1 << (8 - 1);

    public const All            = (2 ** 8) - 1;
    public const None           = 0;

    /**
     * Get removed constants for enum.
     *
     * @return array
     */
    protected static function removedConstants(): array
    {
        return [
            'Fakir',
            'Miskin',
            'Riqab',
        ];
    }

    /**
     * Get patterns for coercing.
     *
     * @return array
     */
    protected static function patterns(): array
    {
        return [
            '/^fa.*mi.*/i' => self::FakirMiskin,
            '/^fa/i' => self::Fakir,
            '/^mi/i' => self::Miskin,
            '/^am/i' => self::Amil,
            '/^mu/i' => self::Mualaf,
            '/^ri/i' => self::Riqab,
            '/^g/i' => self::Gharimin,
            '/^fi/i' => self::Fisabilillah,
            '/^ib/i' => self::Ibnusabil,
        ];
    }
}
