<?php

namespace App\Enums;

use App\Enum;

final class Bidang extends Enum
{
    public const Pendidikan         = 1;
    public const SosialKesehatan    = 2;
    public const Kewirausahaan      = 3;
    public const Dakwah             = 4;
    public const Operasional        = 5;

    /**
     * Get patterns for coercing.
     *
     * @return array
     */
    protected static function patterns(): array
    {
        return [
            '/^pend/i' => self::Pendidikan,
            '/^(sos|kes)/i' => self::SosialKesehatan,
            '/^kew/i' => self::Kewirausahaan,
            '/^da/i' => self::Dakwah,
            '/^op/i' => self::Operasional,
        ];
    }
}
