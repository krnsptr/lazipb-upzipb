<?php

namespace App\Libraries\Akaunting\Setting;

use Akaunting\Setting\Manager as Manager;
use App\Libraries\Akaunting\Setting\Drivers\JsonPrettyDriver;

class SettingManager extends Manager
{
    public function createJsonDriver()
    {
        $path = \config('setting.json.path');

        return new JsonPrettyDriver($this->container['files'], $path);
    }
}
