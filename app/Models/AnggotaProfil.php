<?php

namespace App\Models;

use App\Model;
use App\Models\Anggota;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AnggotaProfil extends Model
{
    use HasFactory;

    protected $table = 'anggota_profil';
    public $incrementing = false;

    protected $fillable = [
        // 'id',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'detail' => 'object',
    ];

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'id');
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'detail' => 'present|array',
        ];
    }

    protected function getRulesForCompleteness(): array
    {
        return [
            'detail' => 'present|array',
        ];
    }

    public function isComplete(): bool
    {
        $data = $this->getData();
        $rules = $this->getRulesForCompleteness();
        $validator = \Validator::make($data, $rules);

        return $validator->passes();
    }
}
