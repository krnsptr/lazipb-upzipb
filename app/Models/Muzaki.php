<?php

namespace App\Models;

use App\Model;
use App\Models\Anggota;
use App\Models\Donasi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;

class Muzaki extends Model
{
    use HasFactory;

    public const REGEX = [
        'nip' => '/^(\d{18}|\d{16})$/',
        'npwp' => '/^\d{15}$/',
        'nomor_hp' => '/^\+628\d{2}\d{4}\d{2,5}$/',
        'nomor_wa' => '/^\+628\d{2}\d{4}\d{2,5}$/',
        'nomor_rekening' => '/^\d{10}$/',
    ];

    protected $table = 'muzaki';

    protected $fillable = [
        // 'id',
        'nama',
        'email',
        'nip',
        'npwp',
        'nomor_hp',
        'nomor_wa',
        'nomor_rekening',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'detail' => 'object',
    ];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'email', 'email');
    }

    public function donasi()
    {
        return $this->hasMany(Donasi::class, 'id_muzaki');
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'nama' => 'required|string|max:191',
            'email' => [
                'nullable',
                'email',
                'max:191',
                Rule::unique($this->table)->ignore($this->id),
            ],
            'nip' => [
                'required',
                'string',
                'regex:' . self::REGEX['nip'],
                Rule::unique($this->table)->ignore($this->id),
            ],
            'npwp' => 'nullable|string|regex:' . self::REGEX['npwp'],
            'nomor_hp' => 'nullable|string|regex:' . self::REGEX['nomor_hp'],
            'nomor_wa' => 'nullable|string|regex:' . self::REGEX['nomor_wa'],
            'nomor_rekening' => 'nullable|string|regex:' . self::REGEX['nomor_rekening'],
            'detail' => 'present|array',
        ];
    }

    public function save(array $options = []): bool
    {
        if ($this->isDirty('email')) {
            $email_old = $this->getOriginal('email');

            $anggota = Anggota::where('email', $this->email)->first()
                ?? Anggota::where('email', $email_old)->firstOrNew();

            if ($anggota && $this->email) {
                $anggota->nama = $this->nama;
                $anggota->email = $this->email;
                $anggota->password = \bcrypt($anggota->email);
            }
        }

        \DB::beginTransaction();

        $saved = parent::save($options);

        if (isset($anggota)) {
            $anggota->save();
        }

        \DB::commit();

        return $saved;
    }

    private function setDigits(string $key, $value)
    {
        if (is_string($value) && $value !== '') {
            $value = preg_replace('/\D/', '', $value);
        }

        $this->attributes[$key] = $value;
    }

    private function setPhone(string $key, $value)
    {
        if (is_string($value) && $value !== '') {
            $value = preg_replace('/\D/', '', $value);
            $value = preg_replace('/^0/', '62', $value);
            $value = preg_replace('/^62/', '+62', $value);
        }

        $this->attributes[$key] = $value;
    }

    public function setNipAttribute($value)
    {
        $this->setDigits('nip', $value);
    }

    public function setNpwpAttribute($value)
    {
        $this->setDigits('npwp', $value);
    }

    public function setNomorRekeningAttribute($value)
    {
        $this->setDigits('nomor_rekening', $value);
    }

    public function setNomorHpAttribute($value)
    {
        $this->setPhone('nomor_hp', $value);
    }

    public function setNomorWaAttribute($value)
    {
        $this->setPhone('nomor_wa', $value);
    }
}
