<?php

namespace App\Models;

use App\Enums\Asnaf;
use App\Model;
use App\Models\Distribusi;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use App\Models\Wilayah\Kota;
use App\Models\Wilayah\Provinsi;
use BenSampo\Enum\Traits\QueriesFlaggedEnums;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class Mustahik extends Model
{
    use HasFactory;
    use QueriesFlaggedEnums;

    public const FOTO_DIR = 'mustahik/foto';
    public const FOTO_DEFAULT = '_default.png';

    public const REGEX = [
        'nik' => '/^\d{16}$/',
        'nrp' => '/^[A-Z]\d[A-Z0-9]\d{6,8}$/',
        'nomor_hp' => '/^\+628\d{2}\d{4}\d{2,5}$/',
    ];

    protected $table = 'mustahik';

    protected $fillable = [
        // 'id',
        // 'id_kelurahan',
        // 'id_kecamatan',
        // 'id_kota',
        // 'id_provinsi',
        'nama',
        'nik',
        'nrp',
        'nomor_hp',
        'asnaf',
        'alamat',
        'nama_kelurahan',
        'nama_kecamatan',
        'nama_kota',
        'nama_provinsi',
        // 'foto',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'id_kelurahan' => 'integer',
        'id_kecamatan' => 'integer',
        'id_kota' => 'integer',
        'id_provinsi' => 'integer',
        'asnaf' => 'integer',
        'detail' => 'object',
    ];

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'id_kelurahan');
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'id_kecamatan');
    }

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'id_kota');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function distribusi()
    {
        return $this->hasMany(Distribusi::class, 'id_mustahik');
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'id_kelurahan' => 'nullable|integer|exists:wilayah_kelurahan,id',
            'id_kecamatan' => 'nullable|integer|exists:wilayah_kecamatan,id',
            'id_kota' => 'nullable|integer|exists:wilayah_kota,id',
            'id_provinsi' => 'nullable|integer|exists:wilayah_provinsi,id',
            'nama' => 'required|string|max:191',
            'nik' => [
                'required',
                'string',
                'regex:' . self::REGEX['nik'],
                Rule::unique($this->table)->ignore($this->id),
            ],
            'nrp' => 'nullable|string|max:191',
            'nomor_hp' => 'nullable|string|regex:' . self::REGEX['nomor_hp'],
            'asnaf' => [
                'required',
                'integer',
                'min:' . 1,
                'max:' . Asnaf::All,
            ],
            'alamat' => 'nullable|string|max:191',
            'nama_kelurahan' => 'nullable|string|max:191',
            'nama_kecamatan' => 'nullable|string|max:191',
            'nama_kota' => 'nullable|string|max:191',
            'nama_provinsi' => 'nullable|string|max:191',
            'foto' => 'nullable|string|max:191',
            'detail' => 'present|array',
        ];
    }

    public function save(array $options = []): bool
    {
        $foto_old = $this->getOriginal('foto');

        $saved = parent::save($options);

        if (!$this->wasRecentlyCreated && $this->wasChanged('foto')) {
            $this->unlinkFotoOld($foto_old);
        }

        return $saved;
    }

    public function getAsnafEnumAttribute(): ?Asnaf
    {
        return new Asnaf($this->asnaf);
    }

    public function getAsnafTextAttribute(): string
    {
        return (string) $this->asnaf_enum;
    }

    public function setAsnafAttribute($asnaf)
    {
        $value = $asnaf;

        if (is_numeric($asnaf)) {
            $asnaf = (int) $asnaf;
        } elseif (is_string($asnaf)) {
            $asnaf = Asnaf::coerce($asnaf);

            if ($asnaf === null) {
                throw ValidationException::withMessages([
                    'asnaf' => "Asnaf tidak valid: $value",
                ]);
            }

            $asnaf = $asnaf->value;
        }

        $this->attributes['asnaf'] = $asnaf;
    }

    private function setDigits(string $key, $value)
    {
        if (is_string($value) && $value !== '') {
            $value = preg_replace('/\D/', '', $value);
        }

        $this->attributes[$key] = $value;
    }

    private function setPhone(string $key, $value)
    {
        if (is_string($value) && $value !== '') {
            $value = preg_replace('/\D/', '', $value);
            $value = preg_replace('/^0/', '62', $value);
            $value = preg_replace('/^62/', '+62', $value);
        }

        $this->attributes[$key] = $value;
    }

    public function setNikAttribute($value)
    {
        $this->setDigits('nik', $value);
    }

    public function getNikMaskedAttribute(): string
    {
        $nik = (string) $this->nik;
        $length = strlen($nik);

        for ($i = 0; $i < $length; $i++) {
            if ($i > 2 && $i < $length - 2 && !($i % 4 === 1)) {
                $nik[$i] = '*';
            }
        }

        return $nik;
    }

    public function setNomorHpAttribute($value)
    {
        $this->setPhone('nomor_hp', $value);
    }

    public function getFotoUrlAttribute(): string
    {
        $dir = static::FOTO_DIR;
        $file = $this->attributes['foto'] ?? static::FOTO_DEFAULT;

        return \Storage::disk('public')->url("$dir/$file");
    }

    public function setFotoAttribute($foto): void
    {
        if ($foto instanceof UploadedFile) {
            \Validator::make(compact('foto'), [
                'foto' => 'image|max:5000',
            ])->validate();

            $id = (int) $this->id;
            $uuid = (string) \Str::orderedUuid();
            $extension = $foto->extension();
            $name = "$id--$uuid.$extension";
            $dir = static::FOTO_DIR;

            \Storage::disk('public')->putFileAs($dir, $foto, $name);

            $this->attributes['foto'] = $name;
        } else {
            $this->attributes['foto'] = $foto;
        }
    }
    public function unlinkFotoOld($file): void
    {
        $dir = static::FOTO_DIR;

        if (empty($file) || $file === static::FOTO_DEFAULT) {
            return;
        }

        \Storage::disk('public')->delete("$dir/$file");
    }

    public function setIdKelurahanAttribute($id_kelurahan): void
    {
        $kelurahan = Kelurahan::find($id_kelurahan);

        if ($kelurahan) {
            $kelurahan->load('kecamatan.kota.provinsi');

            $this->attributes['id_kelurahan'] = $kelurahan->id;
            $this->attributes['id_kecamatan'] = $kelurahan->kecamatan->id;
            $this->attributes['id_kota'] = $kelurahan->kecamatan->kota->id;
            $this->attributes['id_provinsi'] = $kelurahan->kecamatan->kota->provinsi->id;

            $this->attributes['nama_kelurahan'] = $kelurahan->nama;
            $this->attributes['nama_kecamatan'] = $kelurahan->kecamatan->nama;
            $this->attributes['nama_kota'] = $kelurahan->kecamatan->kota->nama;
            $this->attributes['nama_provinsi'] = $kelurahan->kecamatan->kota->provinsi->nama;
        }
    }
}
