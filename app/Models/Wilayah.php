<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    use Cachable;

    public $timestamps = false;
}
