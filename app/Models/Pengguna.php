<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Anggota;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class Pengguna extends Authenticatable
{
    use Notifiable;

    public const FOTO_DIR = '';
    public const FOTO_DEFAULT = '_default.png';

    protected $fillable = [
        // 'id',
        'nama',
        'email',
        'password',
        // 'remember_token',
        // 'created_at',
        // 'updated_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected function getRules(): array
    {
        return [
            'nama' => 'required|string|max:191',
            'email' => [
                'required',
                'email',
                'max:191',
                Rule::unique($this->table)->ignore($this->id),
            ],
            'password' => 'required|string|max:191',
            'foto' => 'nullable|string|max:191',
        ];
    }

    protected function getData(): array
    {
        return $this->getAttributes();
    }

    public function getValidator(): Validator
    {
        return \Validator::make($this->getData(), $this->getRules());
    }

    public function save(array $options = []): bool
    {
        $this->getValidator()->validate();

        $foto_old = $this->getOriginal('foto');

        $save = parent::save($options);

        if (!$this->wasRecentlyCreated && $this->wasChanged('foto')) {
            $this->unlinkFotoOld($foto_old);
        }

        return $save;
    }

    public function getFotoUrlAttribute(): string
    {
        $dir = static::FOTO_DIR;
        $file = $this->attributes['foto'] ?? static::FOTO_DEFAULT;

        return \Storage::disk('public')->url("$dir/$file");
    }

    public function setFotoAttribute($foto): void
    {
        if ($foto instanceof UploadedFile) {
            \Validator::make(compact('foto'), [
                'foto' => 'image|max:5000',
            ])->validate();

            $id = (int) $this->id;
            $uuid = (string) \Str::orderedUuid();
            $extension = $foto->extension();
            $name = "$id--$uuid.$extension";
            $dir = static::FOTO_DIR;

            \Storage::disk('public')->putFileAs($dir, $foto, $name);

            $this->attributes['foto'] = $name;
        } elseif (is_string($foto)) {
            $this->attributes['foto'] = $foto;
        }
    }

    public function unlinkFotoOld(): void
    {
        $dir = static::FOTO_DIR;
        $file = $this->changes['foto'];

        if (empty($file) || $file === static::FOTO_DEFAULT) {
            return;
        }

        \Storage::disk('public')->delete("$dir/$file");
    }

    public function isAdmin(): bool
    {
        return $this instanceof Admin;
    }

    public function isAnggota(): bool
    {
        return $this instanceof Anggota;
    }
}
