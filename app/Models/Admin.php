<?php

namespace App\Models;

use App\Models\Pengguna;

class Admin extends Pengguna
{
    public const FOTO_DIR = 'admin/foto';

    protected $table = 'admin';
}
