<?php

namespace App\Models;

use App\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdminLog extends Model
{
    use HasFactory;

    protected $table = 'admin_log';

    protected $fillable = [
        // 'id',
        'tanggal',
        'aksi',
        'jumlah',
        'input',
        'output',
        'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'tanggal' => 'datetime',
        'jumlah' => 'integer',
        'detail' => 'object',
    ];

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'tanggal' => 'required|date',
            'aksi' => 'required|string',
            'jumlah' => 'required|integer',
            'input' => 'required|string',
            'output' => 'required|string',
            'detail' => 'present|array',
        ];
    }
}
