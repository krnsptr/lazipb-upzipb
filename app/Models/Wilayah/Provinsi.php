<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kota;

class Provinsi extends Wilayah
{
    protected $table = 'wilayah_provinsi';

    public function kota()
    {
        return $this->hasMany(Kota::class, 'id_provinsi');
    }
}
