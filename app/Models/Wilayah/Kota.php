<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Provinsi;

class Kota extends Wilayah
{
    protected $table = 'wilayah_kota';

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class, 'id_kota');
    }
}
