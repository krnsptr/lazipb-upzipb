<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kelurahan;
use App\Models\Wilayah\Kota;

class Kecamatan extends Wilayah
{
    protected $table = 'wilayah_kecamatan';

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'id_kota');
    }
    public function kelurahan()
    {
        return $this->hasMany(Kelurahan::class, 'id_kecamatan');
    }
}
