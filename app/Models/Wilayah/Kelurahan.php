<?php

namespace App\Models\Wilayah;

use App\Models\Wilayah;
use App\Models\Wilayah\Kecamatan;

class Kelurahan extends Wilayah
{
    protected $table = 'wilayah_kelurahan';

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'id_kecamatan');
    }
}
