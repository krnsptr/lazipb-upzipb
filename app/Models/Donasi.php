<?php

namespace App\Models;

use App\Enums\DonasiStatus;
use App\Model;
use App\Models\Muzaki;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class Donasi extends Model
{
    use HasFactory;

    protected $table = 'donasi';

    protected $fillable = [
        // 'id',
        // 'id_muzaki',
        'tanggal',
        'status',
        'zakat',
        'infak',
        'santunan',
        'wakaf',
        'jumlah',
        'keterangan',
        'link_kuitansi',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
        'zakat' => 0,
        'infak' => 0,
        'santunan' => 0,
        'wakaf' => 0,
    ];

    protected $casts = [
        'tanggal' => 'datetime',
        'status' => 'integer',
        'zakat' => 'integer',
        'infak' => 'integer',
        'santunan' => 'integer',
        'wakaf' => 'integer',
        'jumlah' => 'integer',
        'detail' => 'object',
    ];

    public function muzaki()
    {
        return $this->belongsTo(Muzaki::class, 'id_muzaki');
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        return [
            'tanggal' => 'required|date',
            'status' => [
                'required',
                Rule::in(DonasiStatus::getValues()),
            ],
            'zakat' => 'required|integer|min:0|max:4294967295',
            'infak' => 'required|integer|min:0|max:4294967295',
            'santunan' => 'required|integer|min:0|max:4294967295',
            'wakaf' => 'required|integer|min:0|max:4294967295',
            'jumlah' => [
                'required',
                'integer',
                'min:0',
                'max:4294967295',
                Rule::in([$this->getJumlah()]),
            ],
            'link_kuitansi' => 'nullable|url|max:255',
            'keterangan' => 'nullable|string|max:65535',
            'detail' => 'object',
            'detail' => 'present|array',
        ];
    }

    public function getStatusEnumAttribute(): ?DonasiStatus
    {
        return DonasiStatus::coerce($this->status);
    }

    public function getStatusTextAttribute(): string
    {
        return DonasiStatus::getDescription($this->status);
    }

    public function setTanggalAttribute($tanggal)
    {
        if (is_string($tanggal)) {
            $tanggal = str_replace('/', '-', $tanggal);
            $tanggal = str_replace('.', ':', $tanggal);

            $formats = [
                'Y-m-d',
                'Y-m-d H:i',
                'Y-m-d H:i:s',
                'd-m-Y',
                'd-m-Y H:i',
                'd-m-Y H:i:s',
            ];

            $valid = false;

            foreach ($formats as $format) {
                $validator = \Validator::make(compact('tanggal'), [
                    'tanggal' => "date_format:$format",
                ]);

                if ($validator->passes()) {
                    $valid = true;
                    break;
                }
            }

            if (!$valid) {
                \Validator::validate(compact('tanggal'), [
                    'tanggal' => "date_format:{$formats[0]}",
                ]);
            }

            $tanggal = Carbon::parse($tanggal)->format('Y-m-d H:i:s');
        }

        $this->attributes['tanggal'] = $tanggal;
    }

    public function setStatusAttribute($status)
    {
        if (is_numeric($status)) {
            $status = (int) $status;
        } elseif (is_string($status)) {
            $patterns = [
                '/proses/i' => DonasiStatus::AutodebitProses,
                '/tidak.*berhasil/i' => DonasiStatus::AutodebitGagal,
                '/berhasil/i' => DonasiStatus::AutodebitBerhasil,
                '/gagal/i' => DonasiStatus::AutodebitGagal,
                '/belum.*nis/i' => DonasiStatus::AutodebitBelumNisab,
                '/belum.*daf/i' => DonasiStatus::BelumTerdaftar,
                '/transfer/i' => DonasiStatus::TransferRekening,
                '/tunai/i' => DonasiStatus::TunaiKasir,
            ];

            foreach ($patterns as $pattern => $value) {
                if (preg_match($pattern, $status)) {
                    $status = $value;
                    break;
                }
            }

            if (is_string($status)) {
                $status = \Str::studly($status);
            }
        }

        $status = DonasiStatus::coerce($status);

        if ($status === null) {
            throw ValidationException::withMessages([
                'status' => 'Status tidak valid',
            ]);
        }

        $this->attributes['status'] = $status->value;
    }

    public function getJumlah(): int
    {
        return $this->zakat + $this->infak + $this->santunan + $this->wakaf;
    }

    public function setJumlah(): void
    {
        $this->jumlah = $this->getJumlah();
    }

    public function scopeBerhasil($query)
    {
        return $query->whereIn('status', DonasiStatus::Berhasil());
    }
}
