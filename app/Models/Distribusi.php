<?php

namespace App\Models;

use App\Enums\Asnaf;
use App\Enums\Bidang;
use App\Enums\Dana;
use App\Enums\Sdg;
use App\Model;
use App\Models\Mustahik;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class Distribusi extends Model
{
    use HasFactory;

    protected $table = 'distribusi';

    protected $fillable = [
        // 'id',
        // 'id_mustahik',
        'asnaf',
        'sdg',
        'bidang',
        'dana',
        'tanggal',
        'item',
        'nominal',
        // 'detail',
        // 'created_at',
        // 'updated_at',
    ];

    protected $attributes = [
        'detail' => '{}',
    ];

    protected $casts = [
        'id_mustahik' => 'integer',
        'asnaf' => 'integer',
        'sdg' => 'integer',
        'bidang' => 'integer',
        'dana' => 'integer',
        'tanggal' => 'datetime',
        'nominal' => 'integer',
        'detail' => 'object',
    ];

    public function mustahik()
    {
        return $this->belongsTo(Mustahik::class, 'id_mustahik');
    }

    public function mustahik_wd()
    {
        return $this->mustahik()->withDefault();
    }

    protected function getData(): array
    {
        return array_merge($this->getAttributes(), [
            'detail' => json_decode($this->attributes['detail'], true),
        ]);
    }

    protected function getRules(): array
    {
        if ($this->mustahik->asnaf_enum ?? false) {
            $asnaf_list = $this->mustahik->asnaf_enum->getFlagValues();
        } else {
            $asnaf_list = Asnaf::getValues();
        }

        return [
            'id_mustahik' => [
                'nullable',
                Rule::requiredIf(fn () => $this->dana === Dana::Zakat),
                'integer',
                'exists:mustahik,id',
            ],
            'asnaf' => [
                'nullable',
                Rule::requiredIf(fn () => $this->dana === Dana::Zakat),
                Rule::in($asnaf_list),
            ],
            'sdg' => [
                'nullable',
                Rule::in(Sdg::getValues()),
            ],
            'bidang' => [
                'nullable',
                Rule::in(Bidang::getValues()),
            ],
            'dana' => [
                'required',
                Rule::in(Dana::getValues()),
            ],
            'tanggal' => 'required|date',
            'item' => 'required|string|max:191',
            'nominal' => 'required|integer|min:0|max:4294967295',
            'detail' => 'present|array',
        ];
    }

    public function getAsnafEnumAttribute(): ?Asnaf
    {
        return Asnaf::coerce($this->asnaf);
    }

    public function getAsnafTextAttribute(): string
    {
        return (string) $this->asnaf_enum;
    }

    public function setAsnafAttribute($asnaf)
    {
        $value = $asnaf;

        if (is_numeric($asnaf)) {
            $asnaf = (int) $asnaf;
        } elseif (is_string($asnaf)) {
            $asnaf = Asnaf::coerce($asnaf);

            if ($asnaf === null) {
                throw ValidationException::withMessages([
                    'asnaf' => "Asnaf tidak valid: $value",
                ]);
            }

            $asnaf = $asnaf->value;
        }

        $this->attributes['asnaf'] = $asnaf;
    }

    public function getSdgEnumAttribute(): ?Sdg
    {
        return Sdg::coerce($this->sdg);
    }

    public function getSdgTextAttribute(): string
    {
        return (string) $this->sdg_enum;
    }

    public function setSdgAttribute($sdg)
    {
        $value = $sdg;

        if (is_numeric($sdg)) {
            $sdg = (int) $sdg;
        } elseif (is_string($sdg)) {
            $sdg = Sdg::coerce($sdg);

            if ($sdg === null) {
                throw ValidationException::withMessages([
                    'sdg' => "Sdg tidak valid: $value",
                ]);
            }

            $sdg = $sdg->value;
        }

        $this->attributes['sdg'] = $sdg;
    }

    public function getBidangEnumAttribute(): ?Bidang
    {
        return Bidang::coerce($this->bidang);
    }

    public function getBidangTextAttribute(): string
    {
        return (string) $this->bidang_enum;
    }

    public function setBidangAttribute($bidang)
    {
        $value = $bidang;

        if (is_numeric($bidang)) {
            $bidang = (int) $bidang;
        } elseif (is_string($bidang)) {
            $bidang = Bidang::coerce($bidang);

            if ($bidang === null) {
                throw ValidationException::withMessages([
                    'bidang' => "Bidang tidak valid: $value",
                ]);
            }

            $bidang = $bidang->value;
        }

        $this->attributes['bidang'] = $bidang;
    }

    public function getDanaEnumAttribute(): ?Dana
    {
        return Dana::coerce($this->dana);
    }

    public function getDanaTextAttribute(): string
    {
        return (string) $this->dana_enum;
    }

    public function setDanaAttribute($dana)
    {
        $value = $dana;

        if (is_numeric($dana)) {
            $dana = (int) $dana;
        } elseif (is_string($dana)) {
            $dana = Dana::coerce($dana);

            if ($dana === null) {
                throw ValidationException::withMessages([
                    'dana' => "Dana tidak valid: $value",
                ]);
            }

            $dana = $dana->value;
        }

        $this->attributes['dana'] = $dana;
    }

    public function setTanggalAttribute($tanggal)
    {
        if (is_string($tanggal)) {
            $tanggal = str_replace('/', '-', $tanggal);
            $tanggal = str_replace('.', ':', $tanggal);

            $formats = [
                'Y-m-d',
                'Y-m-d H:i',
                'Y-m-d H:i:s',
                'd-m-Y',
                'd-m-Y H:i',
                'd-m-Y H:i:s',
            ];

            $valid = false;

            foreach ($formats as $format) {
                $validator = \Validator::make(compact('tanggal'), [
                    'tanggal' => "date_format:$format",
                ]);

                if ($validator->passes()) {
                    $valid = true;
                    break;
                }
            }

            if (!$valid) {
                \Validator::validate(compact('tanggal'), [
                    'tanggal' => "date_format:{$formats[0]}",
                ]);
            }

            $tanggal = Carbon::parse($tanggal)->format('Y-m-d H:i:s');
        }

        $this->attributes['tanggal'] = $tanggal;
    }
}
