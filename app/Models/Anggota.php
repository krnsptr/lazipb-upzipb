<?php

namespace App\Models;

use App\Models\AnggotaProfil;
use App\Models\Muzaki;
use App\Models\Pengguna;
use App\Notifications\Anggota\ResetPasswordNotification;
use App\Notifications\Anggota\VerifyEmailNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Anggota extends Pengguna
{
    use HasFactory;

    public const FOTO_DIR = 'anggota/foto';

    protected $table = 'anggota';

    protected $fillable = [
        // 'id',
        'nama',
        'email',
        'password',
        'foto',
        // 'remember_token',
        // 'email_verified_at',
        // 'created_at',
        // 'updated_at',
    ];

    protected $appends = [
        'is_email_verified',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profil()
    {
        return $this->hasOne(AnggotaProfil::class, 'id')->withDefault();
    }

    public function muzaki()
    {
        return $this->hasOne(Muzaki::class, 'email', 'email');
    }

    protected function getRules(): array
    {
        return parent::getRules();
    }

    public function getIsEmailVerifiedAttribute(): bool
    {
        return !empty($this->email_verified_at);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }
}
