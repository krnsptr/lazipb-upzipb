<?php

use App\Models\Admin;
use App\Models\Anggota;
use App\Models\Muzaki;
use Carbon\Carbon;

if (!function_exists('admin')) {
    /**
     * Get authenticated admin
     *
     * @return Admin|null $admin
     */
    function admin(): ?Admin
    {
        return \auth('admin')->user();
    }
}

if (!function_exists('anggota')) {
    /**
     * Get authenticated anggota
     *
     * @return Anggota|null $anggota
     */
    function anggota(): ?Anggota
    {
        return \auth('anggota')->user();
    }
}

if (!function_exists('muzaki')) {
    /**
     * Get authenticated muzaki
     *
     * @return Muzaki|null $muzaki
     */
    function muzaki(): ?Muzaki
    {
        return anggota() ? anggota()->muzaki : null;
    }
}

if (!function_exists('format_date')) {
    function format_date($date = null): ?string
    {
        if (!($date instanceof Carbon)) {
            return null;
        }

        return $date->isoFormat('D MMMM YYYY');
    }
}

if (!function_exists('format_datetime')) {
    function format_datetime($date = null): ?string
    {
        if (!($date instanceof Carbon)) {
            return null;
        }

        return "{$date->isoFormat('D MMMM YYYY')}, {$date->format('H.i')}";
    }
}

if (!function_exists('format_time')) {
    function format_time($date = null): ?string
    {
        if (!($date instanceof Carbon)) {
            return null;
        }

        return "{$date->format('H:i')}";
    }
}

if (!function_exists('format_number')) {
    function format_number($number): ?string
    {
        if ($number === null) {
            return null;
        }

        return number_format($number, 0, ',', '.');
    }
}

if (!function_exists('format_rupiah')) {
    function format_rupiah($number): ?string
    {
        if ($number === null) {
            return null;
        }

        return 'Rp' . format_number($number);
    }
}
