<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MuzakiTemplate implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \collect([
            [
                'nama',
                'email',
                'nip',
                'npwp',
                'nomor_hp',
                'nomor_wa',
                'nomor_rekening',
            ],
            [
                'Baris judul jangan diedit/dihapus',
                'xxx1@apps.ipb.ac.id',
                ' ' . str_repeat('1', 18),
                ' ' . str_repeat('1', 15),
                ' ' . '08' . str_repeat('1', 10),
                ' ' . '08' . str_repeat('1', 10),
                ' ' . str_repeat('1', 10),
            ],
            [
                'Baris contoh silakan dihapus/ditimpa',
                'xxx2@apps.ipb.ac.id',
                ' ' . str_repeat('2', 18),
                ' ' . str_repeat('2', 15),
                ' ' . '08' . str_repeat('2', 10),
                ' ' . '08' . str_repeat('2', 10),
                ' ' . str_repeat('2', 10),
            ],
        ]);
    }
}
