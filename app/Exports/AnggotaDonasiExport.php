<?php

namespace App\Exports;

use App\Models\Muzaki;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class AnggotaDonasiExport implements
    FromView,
    ShouldAutoSize,
    WithTitle
{
    use Exportable;

    public string $ext;
    public Muzaki $muzaki;
    public Carbon $tanggal_min;
    public Carbon $tanggal_max;
    public $query;

    public function __construct(string $ext, Muzaki $muzaki, ?Carbon $tanggal_min, ?Carbon $tanggal_max)
    {
        $this->ext = $ext;
        $this->muzaki = $muzaki;
        $this->query = $this->muzaki->donasi()->orderBy('tanggal');

        if ($tanggal_min) {
            $this->query->where('tanggal', '>=', $tanggal_min);
            $this->tanggal_min = $tanggal_min;
        } else {
            $this->tanggal_min = Carbon::parse((clone $this->query)->min('tanggal'));
        }

        if ($tanggal_max) {
            $this->query->where('tanggal', '<=', $tanggal_max);
            $this->tanggal_max = $tanggal_max;
        } else {
            $this->tanggal_max = Carbon::parse((clone $this->query)->max('tanggal'));
        }
    }

    public function view(): View
    {
        $list_donasi = (clone $this->query)->get();

        return \view('exports.anggota-donasi', compact(
            'list_donasi',
        ));
    }

    public function title(): string
    {
        return "Laporan Donasi";
    }

    public function filename(): string
    {
        $tanggal_min = $this->tanggal_min->format('Y-m-d');
        $tanggal_max = $this->tanggal_max->format('Y-m-d');

        $filename = "{$this->title()} _ $tanggal_min - $tanggal_max.{$this->ext}";

        return $filename;
    }
}
