<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DistribusiTemplate implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return \collect([
            [
                'tanggal',
                'dana',
                'nik',
                'asnaf',
                'bidang',
                'sdg',
                'nominal',
                'item',
            ],
            [
                '2021-01-31',
                'Zakat',
                ' ' . str_repeat('0', 16),
                'Amil',
                'Operasional',
                16,
                2_500_000,
                "Baris judul jangan diedit/dihapus",
            ],
            [
                '2021-01-31',
                'Infak',
                ' ' . str_repeat('1', 16),
                'Fakir Miskin',
                'Pendidikan',
                2,
                500_000,
                "Baris contoh silakan dihapus/ditimpa",
            ],
            [
                '2021-01-31',
                'Santunan Yatim',
                '',
                '',
                'Sosial Kesehatan',
                3,
                1_000_000,
                "NIK & asnaf wajib untuk zakat",
            ],
        ]);
    }
}
