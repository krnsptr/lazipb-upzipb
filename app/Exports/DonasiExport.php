<?php

namespace App\Exports;

use App\Models\Donasi;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class DonasiExport extends DefaultValueBinder implements
    FromView,
    ShouldAutoSize,
    WithTitle,
    WithCustomValueBinder
{
    use Exportable;

    public string $ext;
    public Carbon $tanggal_min;
    public Carbon $tanggal_max;
    public $query;

    public function __construct(string $ext, ?Carbon $tanggal_min, ?Carbon $tanggal_max)
    {
        $this->ext = $ext;
        $this->query = Donasi::query()->orderBy('tanggal');

        if ($tanggal_min) {
            $this->query->where('tanggal', '>=', $tanggal_min);
            $this->tanggal_min = $tanggal_min;
        } else {
            $this->tanggal_min = Carbon::parse((clone $this->query)->min('tanggal'));
        }

        if ($tanggal_max) {
            $this->query->where('tanggal', '<=', $tanggal_max);
            $this->tanggal_max = $tanggal_max;
        } else {
            $this->tanggal_max = Carbon::parse((clone $this->query)->max('tanggal'));
        }
    }

    public function view(): View
    {
        $list_donasi = (clone $this->query)->get();

        $list_donasi->load('muzaki:id,nama,nip');

        return \view('exports.donasi', compact(
            'list_donasi',
        ));
    }

    public function title(): string
    {
        return "Data Donasi";
    }

    public function filename(): string
    {
        $tanggal_min = $this->tanggal_min->format('Y-m-d');
        $tanggal_max = $this->tanggal_max->format('Y-m-d');

        $filename = "{$this->title()} _ $tanggal_min - $tanggal_max.{$this->ext}";

        return $filename;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_string($value) && is_numeric($value) && strlen($value) >= 10) {
            $cell->setValueExplicit(" $value", DataType::TYPE_STRING);
            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
