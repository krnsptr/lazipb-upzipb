<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MustahikTemplate implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return \collect([
            [
                'nama',
                'nik',
                'nrp',
                'nomor_hp',
                'asnaf',
                'id_kelurahan',
                'id_kecamatan',
                'id_kota',
                'id_provinsi',
                'nama_kelurahan',
                'nama_kecamatan',
                'nama_kota',
                'nama_provinsi',
                'alamat',
            ],
            [
                'UPZ Al-Hurriyyah IPB',
                ' ' . str_repeat('0', 16),
                '',
                ' ' . '08' . str_repeat('0', 10),
                'Amil',
                '',
                '',
                '',
                '',
                'Babakan',
                'Dramaga',
                'Kab. Bogor',
                'Jawa Barat',
                'Masjid Al-Hurriyyah Lt. 1',
            ],
            [
                'Baris judul jangan diedit/dihapus',
                ' ' . str_repeat('1', 16),
                'A' . str_repeat('1', 8),
                ' ' . '08' . str_repeat('1', 10),
                'Fakir Miskin',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                str_repeat('x', 20) . '1',
            ],
            [
                'Baris contoh silakan dihapus/ditimpa',
                ' ' . str_repeat('2', 16),
                'B' . str_repeat('2', 8),
                ' ' . '08' . str_repeat('2', 10),
                'Mualaf, Gharimin',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                str_repeat('x', 20) . '2',
            ],
        ]);
    }
}
