<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DonasiTemplate implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \collect([
            [
                'nip',
                'tanggal',
                'status',
                'zakat',
                'infak',
                'santunan',
                'wakaf',
                'jumlah',
                'keterangan',
                'link_kuitansi',
            ],
            [
                ' ' . str_repeat('1', 18),
                '2021-01-31',
                'Berhasil',
                300_000,
                200_000,
                100_000,
                '0',
                600_000,
                "Baris judul jangan diedit/dihapus",
                '',
            ],
            [
                ' ' . str_repeat('2', 18),
                '2021-01-31',
                'Gagal',
                300_000,
                200_000,
                100_000,
                400_000,
                1_000_000,
                "Baris contoh silakan dihapus/ditimpa",
                '',
            ],
            [
                ' ' . str_repeat('3', 18),
                '2021-01-31 23:59:59',
                'Proses',
                300_000,
                200_000,
                100_000,
                '0',
                600_000,
                "Gunakan Alt+Enter untuk membuat multiline text",
                '',
            ],
            [
                ' ' . str_repeat('4', 18),
                '2021-01-31',
                'Tunai',
                300_000,
                200_000,
                100_000,
                400_000,
                1_000_000,
                "Jika hanya tanggal, waktu otomatis menjadi 00:00:00",
                '',
            ],
        ]);
    }
}
