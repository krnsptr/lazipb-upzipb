<?php

namespace App\Exports;

use App\Models\Muzaki;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class MuzakiExport extends DefaultValueBinder implements
    FromView,
    ShouldAutoSize,
    WithTitle,
    WithCustomValueBinder
{
    use Exportable;

    public string $ext;
    public $query;

    public function __construct(string $ext)
    {
        $this->ext = $ext;
        $this->query = Muzaki::query();
    }

    public function view(): View
    {
        $list_muzaki = (clone $this->query)->get();

        return \view('exports.muzaki', compact(
            'list_muzaki',
        ));
    }

    public function title(): string
    {
        return "Data Muzaki";
    }

    public function filename(): string
    {
        $date = date('Y-m-d H-i-s');
        $filename = "{$this->title()} _ $date.{$this->ext}";

        return $filename;
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_string($value) && is_numeric($value) && strlen($value) >= 10) {
            $cell->setValueExplicit(" $value", DataType::TYPE_STRING);
            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
