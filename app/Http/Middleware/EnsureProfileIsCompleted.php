<?php

namespace App\Http\Middleware;

use App\Models\Anggota;
use Closure;
use Illuminate\Auth\AuthenticationException;

class EnsureProfileIsCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $anggota = $request->user('anggota');

        if (!$anggota) {
            throw new AuthenticationException();
        }

        /** @var Anggota $anggota */

        if (!$anggota->profil || !$anggota->profil->isComplete()) {
            $message = 'Ayo segera lengkapi profilmu!';

            if ($request->expectsJson()) {
                return \abort(403, $message);
            } else {
                return \redirect()->route('anggota.profil')->withWarning($message);
            }
        }

        return $next($request);
    }
}
