<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $pengguna = $request->user();

        if (!$pengguna) {
            throw new AuthenticationException();
        }

        if ($pengguna instanceof MustVerifyEmail && !$pengguna->hasVerifiedEmail()) {
            $message = 'Ayo segera verifikasikan alamat email-mu!';

            if ($request->expectsJson()) {
                return \abort(403, $message);
            } else {
                return \redirect()->route('anggota.dasbor')->withWarning($message);
            }
        }

        return $next($request);
    }
}
