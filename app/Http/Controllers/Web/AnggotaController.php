<?php

namespace App\Http\Controllers\Web;

use App\Enums\Asnaf;
use App\Enums\Bidang;
use App\Enums\Dana;
use App\Exports\AnggotaDonasiExport;
use App\Http\Controllers\Controller;
use App\Models\Distribusi;
use App\Models\Donasi;
use App\Models\Mustahik;
use Artesaos\SEOTools\Facades\SEOTools;
use Barryvdh\DomPDF\Facade as DomPDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:anggota');

        $this->middleware('profile.completed')->except([
            'dasbor',
            'profil',
        ]);
    }

    public function dasbor()
    {
        SEOTools::setTitle('Dasbor');

        return \view('anggota-dasbor');
    }

    public function profil(Request $request)
    {
        $muzaki = \muzaki();

        if ($request->isMethod('post')) {
            $muzaki->fill($request->only([
                'nomor_hp',
                'nomor_wa',
            ]));

            $muzaki->save();

            return \redirect()->back()->withSuccess('Profil berhasil disimpan.');
        }

        SEOTools::setTitle('Profil Saya');

        return \view('anggota-profil', compact(
            'muzaki',
        ));
    }

    public function donasi(Request $request)
    {
        if ($request->wantsJson() || $request->input('action') === 'download') {
            $q = \muzaki()->donasi();

            if ($tanggal_min = $request->input('tanggal_min')) {
                $tanggal_min = Carbon::parse($tanggal_min);
                $q->where('tanggal', '>=', $tanggal_min);
            }

            if ($tanggal_max = $request->input('tanggal_max')) {
                $tanggal_max = Carbon::parse($tanggal_max);
                $q->where('tanggal', '<=', $tanggal_max);
            }

            if ($request->input('action') === 'download') {
                $ext = $request->input('ext');

                if ($ext === 'xlsx' || $ext === 'pdf') {
                    $export = new AnggotaDonasiExport($ext, \muzaki(), $tanggal_min, $tanggal_max);
                    $filename = $export->filename();

                    if ($ext === 'pdf') {
                        $pdf = DomPDF::loadHTML($export->view());
                        return $pdf->stream($filename);
                    }

                    return $export->download($filename);
                }

                \abort(404);
            }

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            $meta = [
                'tanggal_min' => $tanggal_min ?? (clone $q)->min('tanggal'),
                'tanggal_max' => $tanggal_max ?? (clone $q)->max('tanggal'),
                'count_donasi' => (clone $q)->count(),
                'count_berhasil' => (clone $q)->berhasil()->count(),
                'sum_jumlah' => \format_number((clone $q)->berhasil()->sum('jumlah')),
            ];

            $meta['sum_dana_zakat'] = \format_number((clone $q)->berhasil()->sum('zakat'));
            $meta['sum_dana_infak'] = \format_number((clone $q)->berhasil()->sum('infak'));
            $meta['sum_dana_santunan'] = \format_number((clone $q)->berhasil()->sum('santunan'));
            $meta['sum_dana_wakaf'] = \format_number((clone $q)->berhasil()->sum('wakaf'));

            $data['meta'] = $meta;

            return \response()->json($data);
        }

        $q = \muzaki()->donasi()->berhasil();
        $q_now = (clone $q)->whereYear('tanggal', date('Y'));
        $q_prev = (clone $q)->whereYear('tanggal', date('Y') - 1);

        $jumlah = [];
        $jumlah['sum_donasi_all'] = (clone $q)->sum('jumlah');
        $jumlah['sum_donasi_now'] = (clone $q_now)->sum('jumlah');
        $jumlah['sum_donasi_prev'] = (clone $q_prev)->sum('jumlah');
        $jumlah['count_donasi_all'] = (clone $q)->count('jumlah');
        $jumlah['count_donasi_now'] = (clone $q_now)->count('jumlah');
        $jumlah['count_donasi_prev'] = (clone $q_prev)->count('jumlah');

        $years = (clone $q)->toBase()->selectRaw('DISTINCT YEAR(tanggal) AS year')->pluck('year');
        $years = $years->sort()->reverse();

        SEOTools::setTitle("Laporan Donasi");

        return \view('anggota-donasi', compact(
            'jumlah',
            'years',
        ));
    }

    public function distribusi(Request $request)
    {
        $request->validate([
            'tanggal_min' => 'nullable|date',
            'tanggal_max' => 'nullable|date|after:tanggal_min',
        ]);

        $tanggal_min = Carbon::parse($request->input('tanggal_min') ?? \now()->subMonth(6 - 1)->startOfMonth());
        $tanggal_max = Carbon::parse($request->input('tanggal_max') ?? \now()->endOfMonth());

        $tanggal_min->startOfDay();
        $tanggal_max->endOfDay();

        $q_donasi = Donasi::query()
            ->berhasil()
            ->whereBetween('tanggal', [$tanggal_min, $tanggal_max]);

        $q_distribusi = Distribusi::query()
            ->whereBetween('tanggal', [$tanggal_min, $tanggal_max])
            ->where(function ($q) {
                $q
                    ->where('dana', '<>', Dana::Zakat)
                    ->orWhere(function ($q) {
                        $q
                            ->where('dana', '=', Dana::Zakat)
                            ->whereDoesntHave('mustahik', function ($q) {
                                $q->where('nama', 'LIKE', 'baznas%');
                            });
                    });
            });

        $sum_zakat_excluded = Distribusi::query()
            ->whereBetween('tanggal', [$tanggal_min, $tanggal_max])
            ->where('dana', '=', Dana::Zakat)
            ->whereHas('mustahik', function ($q) {
                $q->where('nama', 'LIKE', 'baznas%');
            })
            ->sum('nominal');

        $result_donasi = (clone $q_donasi)->toBase()->select([
            \DB::raw('SUM(zakat) AS sum_zakat'),
            \DB::raw('SUM(infak) AS sum_infak'),
            \DB::raw('SUM(santunan) AS sum_santunan'),
            \DB::raw('SUM(wakaf) AS sum_wakaf'),
            \DB::raw('SUM(jumlah) AS sum_jumlah'),
        ])->first();

        $selects_bidang = [];

        foreach (Bidang::getValues() as $value) {
            $selects_bidang[] = \DB::raw(
                "SUM(CASE WHEN bidang = $value THEN nominal ELSE 0 END) AS sum_$value"
            );
        }

        $selects_bidang[] = \DB::raw('SUM(nominal) AS sum_all');

        $result_distribusi_bidang = (clone $q_distribusi)
            ->toBase()
            ->select($selects_bidang)
            ->first();

        $selects_asnaf = [];

        foreach (Asnaf::getValues() as $value) {
            $selects_asnaf[] = \DB::raw(
                "SUM(CASE WHEN asnaf = $value THEN nominal ELSE 0 END) AS sum_$value"
            );
        }

        $selects_asnaf[] = \DB::raw('SUM(nominal) AS sum_all');

        $result_distribusi_asnaf = (clone $q_distribusi)
            ->toBase()
            ->where('dana', Dana::Zakat)
            ->select($selects_asnaf)
            ->first();

        $stats = [];

        foreach ((array) $result_donasi as $key => $value) {
            if ($key === 'sum_zakat' || $key === 'sum_jumlah') {
                $value -= $sum_zakat_excluded;
            }

            $stats['donasi'][$key] = $value ?? 0;
        }

        foreach ((array) $result_distribusi_bidang as $key => $value) {
            $stats['distribusi_bidang'][$key] = $value ?? 0;
        }

        foreach ((array) $result_distribusi_asnaf as $key => $value) {
            $stats['distribusi_asnaf'][$key] = $value ?? 0;
        }

        $years = Donasi::query()->toBase()->selectRaw('DISTINCT YEAR(tanggal) AS year')->pluck('year');
        $years = $years->sort()->reverse();

        return \view('anggota-distribusi', compact(
            'tanggal_min',
            'tanggal_max',
            'stats',
            'years',
        ));
    }

    public function mustahik(Request $request)
    {
        $asnaf = $request->input('asnaf');

        if ($asnaf === null) {
            $asnaf_enum = Asnaf::All();
        } else {
            $asnaf_enum = Asnaf::coerce((int) $asnaf);

            if (!$asnaf_enum) {
                return \redirect(\url()->current());
            }
        }

        $list_asnaf = Asnaf::asSelectArray();

        $q = Mustahik::query();

        if ($asnaf_enum->isNot(Asnaf::All)) {
            $q->hasFlag('asnaf', $asnaf_enum->value);
        }

        $list_mustahik = $q->paginate(10);

        return \view('anggota-mustahik', compact(
            'asnaf_enum',
            'list_asnaf',
            'list_mustahik',
        ));
    }

    public function mustahik_lihat(Mustahik $mustahik)
    {
        return \view('anggota-mustahik-lihat', compact(
            'mustahik',
        ));
    }
}
