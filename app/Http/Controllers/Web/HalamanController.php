<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Muzaki;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HalamanController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->only('beranda');
    }

    public function beranda()
    {
        return \view('halaman-beranda');
    }

    public function verifikasi(Request $request)
    {
        $email = \session('email');

        if ($request->isMethod('post')) {
            $nip = $request->input('nip');
            $nomor_rekening = $request->input('nomor_rekening');

            $muzaki = Muzaki::where('nip', $nip)->first();

            if (!$muzaki) {
                return \redirect('/')->with('unregistered', true)->withInput();
            }

            if ($muzaki->nomor_rekening !== $nomor_rekening) {
                return \redirect()->back()->withInput()->withError(
                    'Identitas tersebut tidak cocok. Harap cek kembali.'
                );
            }

            $muzaki->email = $email;
            $muzaki->save();

            $anggota = $muzaki->anggota;

            /** @var \Illuminate\Contracts\Auth\StatefulGuard $guard */
            $guard = Auth::guard('anggota');
            $guard->login($anggota);

            return \redirect()->route('anggota.dasbor');
        }

        return \view('halaman-verifikasi', compact(
            'email',
        ));
    }
}
