<?php

namespace App\Http\Controllers\Web;

use App\Enums\Dana;
use App\Exports\DistribusiExport;
use App\Exports\DistribusiTemplate;
use App\Exports\DonasiExport;
use App\Exports\DonasiTemplate;
use App\Exports\MustahikExport;
use App\Exports\MustahikTemplate;
use App\Exports\MuzakiExport;
use App\Exports\MuzakiTemplate;
use App\Http\Controllers\Controller;
use App\Models\AdminLog;
use App\Models\Anggota;
use App\Models\Distribusi;
use App\Models\Donasi;
use App\Models\Mustahik;
use App\Models\Muzaki;
use Artesaos\SEOTools\Facades\SEOTools;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dasbor()
    {
        $jumlah = [
            'anggota' => Anggota::count(),
            'muzaki' => Muzaki::count(),
        ];

        SEOTools::setTitle('Admin | Dasbor');

        return \view('admin-dasbor', compact(
            'jumlah',
        ));
    }

    public function pengaturan(Request $request)
    {
        if ($request->isMethod('post')) {
            $keys = [
                'kontak_wa',
            ];

            foreach ($keys as $key) {
                \setting([$key => $request->input($key)]);
            }

            \setting()->save();

            return \redirect()->back()->withSuccess(
                'Pengaturan berhasil disimpan.'
            );
        }

        SEOTools::setTitle("Admin | Pengaturan");

        return \view('admin-pengaturan');
    }

    public function muzaki(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Muzaki::select([
                'muzaki.id',
                'muzaki.nama',
                'muzaki.email',
                'muzaki.nip',
            ]);

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Muzaki");

        return \view('admin-muzaki');
    }

    public function muzaki_templat()
    {
        $template = new MuzakiTemplate();
        $file_name = 'Templat Impor Muzaki';

        return $template->download("$file_name.xlsx");
    }

    public function muzaki_ekspor()
    {
        $export = new MuzakiExport('xlsx');
        $filename = $export->filename();
        return $export->download($filename);
    }

    public function muzaki_impor(Request $request)
    {
        if ($request->isMethod('post')) {
            $id = $request->input('id');
            $nip = $request->input('nip');
            $nama = $request->input('nama');
            $email = $request->input('email');
            $validateOnly = $request->boolean('validateOnly');

            if ($id) {
                $muzaki = Muzaki::findOrFail($id);
            } else {
                $muzaki = Muzaki::where('nip', $nip)->firstOrNew();
            }

            if ($email) {
                $anggota = $muzaki->anggota()->firstOrNew();
                $anggota->nama = $nama;
                $anggota->email = $email;

                if ($anggota->isDirty('email')) {
                    $anggota->password = \bcrypt($anggota->email);
                }
            }

            $muzaki->fill($request->all());

            if ($validateOnly) {
                $muzaki->getValidator()->validate();

                if (isset($anggota)) {
                    $anggota->getValidator()->validate();
                }

                if (!$muzaki->exists) {
                    $status = 'Valid.';
                } else {
                    $warning = true;
                    $status = 'Data akan ditimpa.';
                }
            } else {
                $muzaki->save(); // $anggota->save() handled by $muzaki->save()

                if ($muzaki->wasRecentlyCreated) {
                    $status = 'Berhasil diimpor.';
                } else {
                    $warning = true;
                    $status = 'Berhasil ditimpa.';
                }
            }

            $json = [
                'message' => "$status",
                'warning' => $warning ?? null,
            ];

            return \response()->json($json);
        }

        SEOTools::setTitle("Admin | Impor Muzaki");

        return \view('admin-muzaki-impor');
    }

    public function muzaki_lihat(Muzaki $muzaki)
    {
        SEOTools::setTitle("Admin | Muzaki: {$muzaki->nama}");

        return \view('admin-muzaki-lihat', compact(
            'muzaki',
        ));
    }

    public function muzaki_edit(Request $request, Muzaki $muzaki)
    {
        if ($request->isMethod('post')) {
            $muzaki->fill($request->all());

            $muzaki->save();

            return \redirect()
                ->route('admin.muzaki_lihat', $muzaki)
                ->withSuccess('Muzaki berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Muzaki: {$muzaki->nama}");

        return \view('admin-muzaki-edit', compact(
            'muzaki',
        ));
    }

    public function muzaki_hapus(Request $request, Muzaki $muzaki)
    {
        $request->validate([
            'password' => 'password:admin',
        ]);

        $muzaki->delete();

        AdminLog::create([
            'tanggal' => \now(),
            'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->path(), '/')),
            'jumlah' => 1,
            'input' => "(#$muzaki->id) {$muzaki->nama}",
            'output' => json_encode('OK'),
            'detail' => [],
        ]);

        return \redirect()->route('admin.muzaki')->withSuccess('Muzaki berhasil dihapus.');
    }

    public function muzaki_masuk(Muzaki $muzaki)
    {
        $anggota = $muzaki->anggota()->firstOrFail();

        /** @var \Illuminate\Contracts\Auth\StatefulGuard $guard */
        $guard = \auth()->guard('anggota');
        $guard->login($anggota);

        return \redirect()->route('anggota.dasbor');
    }

    public function mustahik(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Mustahik::select([
                'mustahik.id',
                'mustahik.nama',
                'mustahik.nik',
            ]);

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Mustahik");

        return \view('admin-mustahik');
    }

    public function mustahik_templat()
    {
        $template = new MustahikTemplate();
        $file_name = 'Templat Impor Mustahik';

        return $template->download("$file_name.xlsx");
    }

    public function mustahik_ekspor()
    {
        $export = new MustahikExport('xlsx');
        $filename = $export->filename();
        return $export->download($filename);
    }

    public function mustahik_impor(Request $request)
    {
        if ($request->isMethod('post')) {
            $id = $request->input('id');
            $nik = $request->input('nik');
            $validateOnly = $request->boolean('validateOnly');

            if ($id) {
                $mustahik = Mustahik::findOrFail($id);
            } else {
                $mustahik = Mustahik::where('nik', $nik)->firstOrNew();
            }

            $mustahik->fill($request->all());

            if ($validateOnly) {
                $mustahik->getValidator()->validate();

                if (!$mustahik->exists) {
                    $status = 'Valid.';
                } else {
                    $warning = true;
                    $status = 'Data akan ditimpa.';
                }
            } else {
                $mustahik->save();

                if ($mustahik->wasRecentlyCreated) {
                    $status = 'Berhasil diimpor.';
                } else {
                    $warning = true;
                    $status = 'Berhasil ditimpa.';
                }
            }

            $json = [
                'message' => "$status",
                'warning' => $warning ?? null,
            ];

            return \response()->json($json);
        }

        SEOTools::setTitle("Admin | Impor Mustahik");

        return \view('admin-mustahik-impor');
    }

    public function mustahik_tambah(Request $request, Mustahik $mustahik)
    {
        if ($request->isMethod('post')) {
            $mustahik->fill($request->all());
            $mustahik->id_kelurahan = $request->input('id_kelurahan');

            if ($request->hasFile('foto')) {
                $mustahik->foto = $request->file('foto');
            } elseif ($request->boolean('hapus_foto')) {
                $mustahik->foto = null;
            }

            $mustahik->save();

            AdminLog::create([
                'tanggal' => \now(),
                'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->path(), '/')),
                'jumlah' => 1,
                'input' => "(#$mustahik->id) {$mustahik->nama}",
                'output' => json_encode('OK'),
                'detail' => [],
            ]);

            $redirect = \route('admin.mustahik_lihat', $mustahik);
            $message = 'Mustahik berhasil disimpan.';

            if ($request->wantsJson()) {
                return \response()->json([
                    'message' => $message,
                    'redirect' => $redirect,
                ]);
            }

            return \redirect($redirect)->withSuccess($message);
        }

        SEOTools::setTitle("Admin | Tambah Mustahik");

        return \view('admin-mustahik-tambah', compact(
            'mustahik',
        ));
    }

    public function mustahik_lihat(Mustahik $mustahik)
    {
        SEOTools::setTitle("Admin | Mustahik: {$mustahik->nama}");

        return \view('admin-mustahik-lihat', compact(
            'mustahik',
        ));
    }

    public function mustahik_edit(Request $request, Mustahik $mustahik)
    {
        if ($request->isMethod('post')) {
            $mustahik->fill($request->all());
            $mustahik->id_kelurahan = $request->input('id_kelurahan');

            if ($request->hasFile('foto')) {
                $mustahik->foto = $request->file('foto');
            } elseif ($request->boolean('hapus_foto')) {
                $mustahik->foto = null;
            }

            $mustahik->save();

            $redirect = \route('admin.mustahik_lihat', $mustahik);
            $message = 'Mustahik berhasil disimpan.';

            if ($request->wantsJson()) {
                return \response()->json([
                    'message' => $message,
                    'redirect' => $redirect,
                ]);
            }

            return \redirect($redirect)->withSuccess($message);
        }

        SEOTools::setTitle("Admin | Edit Mustahik: {$mustahik->nama}");

        return \view('admin-mustahik-edit', compact(
            'mustahik',
        ));
    }

    public function mustahik_hapus(Request $request, Mustahik $mustahik)
    {
        $request->validate([
            'password' => 'password:admin',
        ]);

        $mustahik->delete();

        AdminLog::create([
            'tanggal' => \now(),
            'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->path(), '/')),
            'jumlah' => 1,
            'input' => "(#$mustahik->id) {$mustahik->nama}",
            'output' => json_encode('OK'),
            'detail' => [],
        ]);

        return \redirect()->route('admin.mustahik')->withSuccess('Mustahik berhasil dihapus.');
    }

    public function donasi(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Donasi::select([
                'donasi.id',
                'donasi.id_muzaki',
                'donasi.tanggal',
                'donasi.status',
                // 'donasi.zakat',
                // 'donasi.infak',
                // 'donasi.santunan',
                // 'donasi.wakaf',
                'donasi.jumlah',
                // 'donasi.keterangan',
            ])->with('muzaki');

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        $years = Donasi::query()->toBase()->selectRaw('DISTINCT YEAR(tanggal) AS year')->pluck('year');
        $years = $years->sort()->reverse();

        SEOTools::setTitle("Admin | Donasi");

        return \view('admin-donasi', compact(
            'years',
        ));
    }

    public function donasi_templat()
    {
        $template = new DonasiTemplate();
        $file_name = 'Templat Impor Donasi';

        return $template->download("$file_name.xlsx");
    }

    public function donasi_ekspor(Request $request)
    {
        $tanggal_min = Carbon::parse($request->input('tanggal_min'));
        $tanggal_max = Carbon::parse($request->input('tanggal_max'));

        $export = new DonasiExport('xlsx', $tanggal_min, $tanggal_max);
        $filename = $export->filename();
        return $export->download($filename);
    }

    public function donasi_impor(Request $request)
    {
        if ($request->isMethod('post')) {
            $id = $request->input('id');
            $nip = $request->input('nip');
            $validateOnly = $request->boolean('validateOnly');

            $muzaki = Muzaki::where('nip', $nip)->first();

            if (!$muzaki) {
                \abort(404, 'NIP tidak terdaftar.');
            }

            try {
                $tanggal = $request->input('tanggal');
                $tanggal = str_replace('/', '-', $tanggal);
                $tanggal = str_replace('.', ':', $tanggal);
                $tanggal = Carbon::parse($tanggal);
            } catch (InvalidFormatException $e) {
                \abort(400, 'Format tanggal tidak sesuai.');
            }

            $q = $muzaki->donasi()->latest('id');

            if ($id) {
                $donasi = (clone $q)->findOrFail($id);
            } else {
                $donasi = (clone $q)->where('tanggal', $tanggal)->firstOrNew();
            }

            $donasi->id_muzaki = $muzaki->id;
            $donasi->fill($request->all());

            if ($donasi->exists) {
                $status = "Donasi sudah ada.";
            }

            if ($validateOnly) {
                $donasi->getValidator()->validate();

                if (!$donasi->exists) {
                    $status = 'Valid.';
                } else {
                    $warning = true;
                    $status .= ' Data akan ditimpa.';
                }
            } else {
                \DB::beginTransaction();

                $donasi->save();

                \DB::commit();

                if ($donasi->wasRecentlyCreated) {
                    $status = 'Berhasil diimpor.';
                } else {
                    $warning = true;
                    $status .= ' Berhasil ditimpa.';
                }
            }

            $json = [
                'message' => "$status",
                'warning' => $warning ?? null,
            ];

            return \response()->json($json);
        }

        SEOTools::setTitle("Admin | Impor Donasi");

        return \view('admin-donasi-impor');
    }

    public function donasi_lihat(Donasi $donasi)
    {
        $muzaki = $donasi->muzaki;

        SEOTools::setTitle("Admin | Donasi: {$donasi->nama}");

        return \view('admin-donasi-lihat', compact(
            'donasi',
            'muzaki',
        ));
    }

    public function donasi_edit(Request $request, Donasi $donasi)
    {
        if ($request->isMethod('post')) {
            $donasi->fill($request->all());

            $donasi->save();

            return \redirect()
                ->route('admin.donasi_lihat', $donasi)
                ->withSuccess('Donasi berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Donasi: {$donasi->id}");

        return \view('admin-donasi-edit', compact(
            'donasi',
        ));
    }

    public function donasi_hapus(Request $request, Donasi $donasi)
    {
        $request->validate([
            'password' => 'password:admin',
        ]);

        $donasi->delete();

        AdminLog::create([
            'tanggal' => \now(),
            'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->path(), '/')),
            'jumlah' => 1,
            'input' => "(#$donasi->id) {$donasi->tanggal}",
            'output' => json_encode('OK'),
            'detail' => [],
        ]);

        return \redirect()->route('admin.donasi')->withSuccess('Donasi berhasil dihapus.');
    }

    public function distribusi(Request $request)
    {
        if ($request->wantsJson()) {
            $q = Distribusi::select([
                'distribusi.id',
                'distribusi.id_mustahik',
                'distribusi.asnaf',
                'distribusi.sdg',
                'distribusi.bidang',
                'distribusi.dana',
                'distribusi.tanggal',
                'distribusi.item',
                'distribusi.nominal',
                // 'distribusi.detail',
                // 'distribusi.created_at',
                // 'distribusi.updated_at',
            ])->with('mustahik_wd');

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        $years = Distribusi::query()
            ->toBase()
            ->selectRaw('DISTINCT YEAR(tanggal) AS year')
            ->pluck('year');

        $years = $years->sort()->reverse();

        SEOTools::setTitle("Admin | Distribusi");

        return \view('admin-distribusi', compact(
            'years',
        ));
    }

    public function distribusi_templat()
    {
        $template = new DistribusiTemplate();
        $file_name = 'Templat Impor Distribusi';

        return $template->download("$file_name.xlsx");
    }

    public function distribusi_impor(Request $request)
    {
        if ($request->isMethod('post')) {
            $id = $request->input('id');
            $nik = $request->input('nik');
            $validateOnly = $request->boolean('validateOnly');

            try {
                $tanggal = $request->input('tanggal');
                $tanggal = str_replace('/', '-', $tanggal);
                $tanggal = str_replace('.', ':', $tanggal);
                $tanggal = Carbon::parse($tanggal);
            } catch (InvalidFormatException $e) {
                \abort(400, 'Format tanggal tidak sesuai.');
            }

            $mustahik = Mustahik::where('nik', $nik)->first();

            $distribusi_new = new Distribusi();
            $distribusi_new->id_mustahik = $mustahik->id ?? null;
            $distribusi_new->fill($request->all());

            if (!$nik) {
                if ($distribusi_new->dana === Dana::Zakat) {
                    \abort(400, 'NIK wajib diisi.');
                }
            } else {
                if (!$mustahik) {
                    \abort(400, 'NIK tidak terdaftar.');
                }
            }

            $q = Distribusi::latest('id');

            if ($id) {
                $distribusi = (clone $q)->findOrFail($id);
            } else {
                $distribusi = (clone $q)->firstOrNew([
                    'id_mustahik' => $distribusi_new->id_mustahik,
                    'asnaf' => $distribusi_new->asnaf,
                    'dana' => $distribusi_new->dana,
                    'tanggal' => $distribusi_new->tanggal,
                    'item' => $distribusi_new->item,
                ]);
            }

            $distribusi->id_mustahik = $distribusi_new->id_mustahik;
            $distribusi->fill($request->all());

            $exists = $distribusi->exists;

            if ($exists) {
                $status = "Distribusi sudah ada.";
            }

            if ($validateOnly) {
                $distribusi->getValidator()->validate();

                if (!$exists) {
                    $status = 'Valid.';
                } else {
                    $warning = true;
                    $status .= ' Data akan ditimpa.';
                }
            } else {
                \DB::beginTransaction();

                $distribusi->save();

                \DB::commit();

                if ($distribusi->wasRecentlyCreated) {
                    $status = 'Berhasil diimpor.';
                } else {
                    $warning = true;
                    $status .= ' Berhasil ditimpa.';
                }
            }

            $json = [
                'message' => "$status",
                'warning' => $warning ?? null,
            ];

            return \response()->json($json);
        }

        SEOTools::setTitle("Admin | Impor Distribusi");

        return \view('admin-distribusi-impor');
    }

    public function distribusi_ekspor(Request $request)
    {
        $tanggal_min = Carbon::parse($request->input('tanggal_min'));
        $tanggal_max = Carbon::parse($request->input('tanggal_max'));

        $export = new DistribusiExport('xlsx', $tanggal_min, $tanggal_max);
        $filename = $export->filename();
        return $export->download($filename);
    }

    public function distribusi_lihat(Distribusi $distribusi)
    {
        $distribusi->load('mustahik');

        SEOTools::setTitle("Admin | Distribusi: {$distribusi->nama}");

        return \view('admin-distribusi-lihat', compact(
            'distribusi',
        ));
    }

    public function distribusi_edit(Request $request, Distribusi $distribusi)
    {
        if ($request->isMethod('post')) {
            $distribusi->fill($request->all());
            $distribusi->save();

            return \redirect()
                ->route('admin.distribusi_lihat', $distribusi)
                ->withSuccess('Distribusi berhasil disimpan.');
        }

        SEOTools::setTitle("Admin | Edit Distribusi: {$distribusi->id}");

        return \view('admin-distribusi-edit', compact(
            'distribusi',
        ));
    }

    public function distribusi_hapus(Request $request, Distribusi $distribusi)
    {
        $request->validate([
            'password' => 'password:admin',
        ]);

        $distribusi->delete();

        AdminLog::create([
            'tanggal' => \now(),
            'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->path(), '/')),
            'jumlah' => 1,
            'input' => "(#$distribusi->id) {$distribusi->tanggal}",
            'output' => json_encode('OK'),
            'detail' => [],
        ]);

        return \redirect()->route('admin.distribusi')->withSuccess('Distribusi berhasil dihapus.');
    }

    public function log(Request $request)
    {
        if ($request->wantsJson()) {
            $q = AdminLog::select([
                'admin_log.id',
                'admin_log.tanggal',
                'admin_log.aksi',
                'admin_log.jumlah',
                'admin_log.input',
                'admin_log.output',
            ]);

            $dt = DataTables::eloquent($q);

            $data = $dt->make()->getData(true);

            return \response()->json($data);
        }

        SEOTools::setTitle("Admin | Log");

        return \view('admin-log');
    }

    public function log_lihat(AdminLog $log)
    {
        SEOTools::setTitle("Admin | Log Admin: #{$log->id}");

        return \view('admin-log-lihat', compact(
            'log',
        ));
    }

    public function log_tambah(Request $request)
    {
        AdminLog::create([
            'tanggal' => \now(),
            'aksi' => preg_replace('/^admin\/|\/\d+$/', '', trim($request->input('aksi'), '/')),
            'jumlah' => $request->input('jumlah'),
            'input' => $request->input('input'),
            'output' => json_encode($request->input('output')),
            'detail' => $request->input('detail'),
        ]);

        return \response()->json(['message' => 'OK']);
    }
}
