<?php

namespace App\Http\Controllers\Web\Auth\Anggota;

use App\Http\Controllers\Controller;
use App\Models\Anggota;
use App\Models\Muzaki;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function login()
    {
        return Socialite::driver('google')->stateless()->with([
            'hd' => 'apps.ipb.ac.id',
        ])->redirect();
    }

    public function callback()
    {
        $user = Socialite::driver('google')->stateless()->user();

        $name = $user->getName();
        $email = $user->getEmail();

        if (!\Str::endsWith($email, '@apps.ipb.ac.id')) {
            return \redirect('/')->withError(
                "Email $email tidak sesuai. Harap gunakan email @apps.ipb.ac.id."
            );
        }

        $muzaki = Muzaki::where('email', $email)->first();

        if (!$muzaki) {
            \session(['email' => $email]);

            return \redirect()->route('halaman.verifikasi');
        }

        $anggota = Anggota::where('email', $email)->first();

        if (!$anggota) {
            $anggota = new Anggota();
            $anggota->nama = $name;
            $anggota->email = $email;
            $anggota->password = \bcrypt($email);
            $anggota->save();
        }

        /** @var \Illuminate\Contracts\Auth\StatefulGuard $guard */
        $guard = Auth::guard('anggota');
        $guard->login($anggota);

        return \redirect()->route('anggota.dasbor');
    }
}
