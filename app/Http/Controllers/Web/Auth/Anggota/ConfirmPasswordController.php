<?php

namespace App\Http\Controllers\Web\Auth\Anggota;

use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Support\Facades\Auth;

class ConfirmPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Confirm Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password confirmations and
    | uses a simple trait to include the behavior. You're free to explore
    | this trait and override any functions that require customization.
    |
    */
    use ConfirmsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:anggota');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('anggota');
    }

    /**
     * Where to redirect users when the intended url fails.
     *
     * @return string
     */
    public function redirectTo()
    {
        return \route('anggota.dasbor');
    }

    /**
     * Display the password confirmation view.
     *
     * @return \Illuminate\Http\Response
     */
    public function showConfirmForm()
    {
        SEOTools::setTitle('Konfirmasi kata sandi');
        SEOTools::setCanonical(\route('anggota.password.konfirmasi'));

        return \view('auth.anggota-password-konfirmasi');
    }
}
