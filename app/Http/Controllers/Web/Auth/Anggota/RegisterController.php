<?php

namespace App\Http\Controllers\Web\Auth\Anggota;

use App\Http\Controllers\Controller;
use App\Models\Anggota;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('anggota');
    }

    /**
     * Where to redirect users after registration.
     *
     * @return string
     */
    public function redirectTo()
    {
        return \route('anggota.dasbor');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        SEOTools::setTitle('Daftar');
        SEOTools::setCanonical(\route('anggota.daftar'));

        return \view('auth.anggota-daftar');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string|min:8',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $email = $request->input('email');

        if (Anggota::where('email', $email)->exists()) {
            return \redirect()->route('anggota.masuk')->withError(
                "Alamat email $email sudah terdaftar."
            );
        }

        $anggota = $this->create($request->all());

        \event(new Registered($anggota));

        $this->guard()->login($anggota);

        return $this->registered($request, $anggota) ?: \redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Anggota
     */
    protected function create(array $data)
    {
        return Anggota::create([
            'nama' => $data['nama'],
            'email' => $data['email'],
            'password' => \bcrypt($data['password']),
        ]);
    }
}
