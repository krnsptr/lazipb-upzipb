<?php

namespace App\Http\Controllers\Web\Auth\Anggota;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('email.throttle:3,5')->only('sendResetLinkEmail');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('anggota');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        SEOTools::setTitle('Lupa kata sandi');
        SEOTools::setCanonical(\route('anggota.password.lupa'));

        return \view('auth.anggota-password-lupa');
    }
}
