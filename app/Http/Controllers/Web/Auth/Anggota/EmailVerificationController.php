<?php

namespace App\Http\Controllers\Web\Auth\Anggota;

use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class EmailVerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */
    use VerifiesEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:anggota');
        $this->middleware('throttle:6,1')->only('verify');
        $this->middleware('email.throttle:3,5')->only('resend');
    }

    /**
     * Where to redirect users after verifying their email.
     *
     * @return string
     */
    public function redirectTo()
    {
        return \route('anggota.profil');
    }

    /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function error(Request $request)
    {
        SEOTools::setTitle('Verifikasi email gagal');
        SEOTools::setCanonical(\route('anggota.email.verifikasi.gagal'));

        if ($request->user()->hasVerifiedEmail()) {
            return \redirect()->route('anggota.dasbor');
        } else {
            return \view('auth.anggota-email-verifikasi-gagal');
        }
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        if (!$request->hasValidSignature() || $request->route('id') != $request->user()->getKey()) {
            return $this->error($request);
        }

        if ($request->user()->markEmailAsVerified()) {
            \event(new Verified($request->user()));
            \session()->flash('success', 'Verifikasi email berhasil. Ayo lengkapi profil!');
        }

        return \redirect($this->redirectPath());
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return \redirect($this->redirectPath());
        }

        $request->user()->sendEmailVerificationNotification();
        \session()->flash('success', 'Email verifikasi berhasil dikirim ulang.');

        return \back();
    }
}
