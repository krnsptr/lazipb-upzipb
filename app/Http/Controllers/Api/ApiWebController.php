<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use App\Models\Wilayah\Kota;
use App\Models\Wilayah\Provinsi;
use Illuminate\Http\Request;

class ApiWebController extends Controller
{
    public function provinsi()
    {
        $data = Provinsi::select('id', 'nama AS text')
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }

    public function kota(Request $request)
    {
        $data = Kota::select('id', 'nama AS text')
        ->where('id_provinsi', $request->input('id_provinsi'))
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }

    public function kecamatan(Request $request)
    {
        $data = Kecamatan::select('id', 'nama AS text')
        ->where('id_kota', $request->input('id_kota'))
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }

    public function kelurahan(Request $request)
    {
        $data = Kelurahan::select('id', 'nama AS text')
        ->where('id_kecamatan', $request->input('id_kecamatan'))
        ->orderBy('nama')
        ->get();

        return \response()->json($data);
    }
}
