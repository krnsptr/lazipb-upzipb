<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('web/provinsi', 'ApiWebController@provinsi')->name('api.web.provinsi');
Route::get('web/kota', 'ApiWebController@kota')->name('api.web.kota');
Route::get('web/kecamatan', 'ApiWebController@kecamatan')->name('api.web.kecamatan');
Route::get('web/kelurahan', 'ApiWebController@kelurahan')->name('api.web.kelurahan');
