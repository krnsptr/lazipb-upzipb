<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HalamanController@beranda');

Route::get('verifikasi', 'HalamanController@verifikasi')->name('halaman.verifikasi');
Route::post('verifikasi', 'HalamanController@verifikasi');

Route::prefix('admin')->namespace('Auth\Admin')->group(function () {
    Route::get('masuk', 'LoginController@showLoginForm')->name('admin.masuk');
    Route::post('masuk', 'LoginController@login');

    Route::post('keluar', 'LoginController@logout')->name('admin.keluar');
});

Route::namespace('Auth\Anggota')->group(function () {
    Route::get('/oauth/google/login', 'GoogleController@login')->name('oauth.google.login');
    Route::get('/oauth/google/callback', 'GoogleController@callback')->name('oauth.google.callback');

    if (\App::isProduction() || \setting('_auth_disabled')) {
        Route::get('masuk', fn () => \redirect('/'))->name('anggota.masuk');
        Route::post('keluar', 'LoginController@logout')->name('anggota.keluar');

        return;
    }

    Route::get('daftar', 'RegisterController@showRegistrationForm')->name('anggota.daftar');
    Route::post('daftar', 'RegisterController@register');

    Route::get('masuk', 'LoginController@showLoginForm')->name('anggota.masuk');
    Route::post('masuk', 'LoginController@login');

    Route::post('keluar', 'LoginController@logout')->name('anggota.keluar');

    Route::get('password/lupa', 'ForgotPasswordController@showLinkRequestForm')
    ->name('anggota.password.lupa');

    Route::post('password/lupa', 'ForgotPasswordController@sendResetLinkEmail');

    Route::get('password/reset/{token?}', 'ResetPasswordController@showResetForm')
    ->name('anggota.password.reset');

    Route::post('password/reset/{token?}', 'ResetPasswordController@reset');

    Route::get('email/verifikasi/gagal', 'EmailVerificationController@error')
    ->name('anggota.email.verifikasi.gagal');

    Route::post('email/verifikasi/kirim', 'EmailVerificationController@resend')
    ->name('anggota.email.verifikasi.kirim');

    Route::get('email/verifikasi/cek/{id}', 'EmailVerificationController@verify')
    ->name('anggota.email.verifikasi.cek');
});

Route::prefix('admin')->middleware('auth:admin')->group(function () {
    Route::get('/', fn () => \redirect()->route('admin.dasbor'));
    Route::get('dasbor', 'AdminController@dasbor')->name('admin.dasbor');

    Route::get('pengaturan', 'AdminController@pengaturan')->name('admin.pengaturan');
    Route::post('pengaturan', 'AdminController@pengaturan');

    Route::prefix('muzaki')->group(function () {
        Route::get('', 'AdminController@muzaki')->name('admin.muzaki');

        Route::get('templat', 'AdminController@muzaki_templat')->name('admin.muzaki_templat');
        Route::get('ekspor', 'AdminController@muzaki_ekspor')->name('admin.muzaki_ekspor');
        Route::get('impor', 'AdminController@muzaki_impor')->name('admin.muzaki_impor');
        Route::post('impor', 'AdminController@muzaki_impor');

        Route::get('lihat/{muzaki}', 'AdminController@muzaki_lihat')->name('admin.muzaki_lihat');
        Route::get('masuk/{muzaki}', 'AdminController@muzaki_masuk')->name('admin.muzaki_masuk');

        Route::get('edit/{muzaki}', 'AdminController@muzaki_edit')->name('admin.muzaki_edit');
        Route::post('edit/{muzaki}', 'AdminController@muzaki_edit');

        Route::post('hapus/{muzaki}', 'AdminController@muzaki_hapus')->name('admin.muzaki_hapus');
    });

    Route::prefix('mustahik')->group(function () {
        Route::get('', 'AdminController@mustahik')->name('admin.mustahik');

        Route::get('templat', 'AdminController@mustahik_templat')->name('admin.mustahik_templat');
        Route::get('ekspor', 'AdminController@mustahik_ekspor')->name('admin.mustahik_ekspor');
        Route::get('impor', 'AdminController@mustahik_impor')->name('admin.mustahik_impor');
        Route::post('impor', 'AdminController@mustahik_impor');

        Route::get('tambah', 'AdminController@mustahik_tambah')->name('admin.mustahik_tambah');
        Route::post('tambah', 'AdminController@mustahik_tambah');

        Route::get('lihat/{mustahik}', 'AdminController@mustahik_lihat')->name('admin.mustahik_lihat');

        Route::get('edit/{mustahik}', 'AdminController@mustahik_edit')->name('admin.mustahik_edit');
        Route::post('edit/{mustahik}', 'AdminController@mustahik_edit');

        Route::post('hapus/{mustahik}', 'AdminController@mustahik_hapus')->name('admin.mustahik_hapus');
    });

    Route::prefix('donasi')->group(function () {
        Route::get('', 'AdminController@donasi')->name('admin.donasi');

        Route::get('templat', 'AdminController@donasi_templat')->name('admin.donasi_templat');
        Route::get('ekspor', 'AdminController@donasi_ekspor')->name('admin.donasi_ekspor');
        Route::get('impor', 'AdminController@donasi_impor')->name('admin.donasi_impor');
        Route::post('impor', 'AdminController@donasi_impor');

        Route::get('lihat/{donasi}', 'AdminController@donasi_lihat')->name('admin.donasi_lihat');

        Route::get('edit/{donasi}', 'AdminController@donasi_edit')->name('admin.donasi_edit');
        Route::post('edit/{donasi}', 'AdminController@donasi_edit');

        Route::post('hapus/{donasi}', 'AdminController@donasi_hapus')->name('admin.donasi_hapus');
    });

    Route::prefix('distribusi')->group(function () {
        Route::get('', 'AdminController@distribusi')->name('admin.distribusi');

        Route::get('templat', 'AdminController@distribusi_templat')->name('admin.distribusi_templat');
        Route::get('ekspor', 'AdminController@distribusi_ekspor')->name('admin.distribusi_ekspor');
        Route::get('impor', 'AdminController@distribusi_impor')->name('admin.distribusi_impor');
        Route::post('impor', 'AdminController@distribusi_impor');

        Route::get('lihat/{distribusi}', 'AdminController@distribusi_lihat')->name('admin.distribusi_lihat');

        Route::get('edit/{distribusi}', 'AdminController@distribusi_edit')->name('admin.distribusi_edit');
        Route::post('edit/{distribusi}', 'AdminController@distribusi_edit');

        Route::post('hapus/{distribusi}', 'AdminController@distribusi_hapus')->name('admin.distribusi_hapus');
    });

    Route::prefix('log')->group(function () {
        Route::get('', 'AdminController@log')->name('admin.log');
        Route::post('tambah', 'AdminController@log_tambah')->name('admin.log_tambah');
        Route::get('lihat/{log}', 'AdminController@log_lihat')->name('admin.log_lihat');
    });
});

Route::middleware('auth:anggota')->group(function () {
    Route::get('dasbor', fn () => \redirect()->route('anggota.donasi'))->name('anggota.dasbor');

    Route::get('profil', 'AnggotaController@profil')->name('anggota.profil');
    Route::post('profil', 'AnggotaController@profil');

    Route::get('donasi', 'AnggotaController@donasi')->name('anggota.donasi');

    Route::get('distribusi', 'AnggotaController@distribusi')->name('anggota.distribusi');
});
