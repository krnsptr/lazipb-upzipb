<?php

namespace Database\Factories;

use App\Enums\Bidang;
use App\Enums\Dana;
use App\Enums\Sdg;
use App\Models\Distribusi;
use App\Models\Mustahik;
use Illuminate\Database\Eloquent\Factories\Factory;

class DistribusiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Distribusi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        return [
            'sdg' => Sdg::getRandomValue(),
            'bidang' => Bidang::getRandomValue(),
            'dana' => Dana::getRandomValue(),
            // 'tanggal' => $faker->dateTimeBetween('-10 year', 'now'),
            'item' => $faker->words($faker->numberBetween(1, 3), true),
            'nominal' => $faker->numberBetween(100, 1000) * 1000,
        ];
    }

    public function withMustahik(Mustahik $mustahik)
    {
        return $this->state(function () use ($mustahik) {
            return [
                'id_mustahik' => $mustahik->id,
                'asnaf' => \collect($mustahik->asnaf_enum->getFlags())->random()->value,
                'dana' => $this->faker->randomElement([
                    Dana::Zakat,
                    Dana::Infak,
                ]),
            ];
        });
    }

    public function withoutMustahik()
    {
        return $this->state(function () {
            return [
                'dana' => $this->faker->randomElement([
                    Dana::Infak,
                    Dana::Santunan,
                    Dana::Wakaf,
                ]),
            ];
        });
    }
}
