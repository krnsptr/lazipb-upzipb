<?php

namespace Database\Factories;

use App\Enums\DonasiStatus;
use App\Models\Donasi;
use Illuminate\Database\Eloquent\Factories\Factory;

class DonasiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Donasi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        $status = DonasiStatus::getRandomInstance();

        $zakat = $faker->numberBetween(100, 1000);
        $infak = $faker->boolean() ? $faker->numberBetween(0, $zakat) : 0;
        $santunan = $faker->boolean() ? $faker->numberBetween(0, $zakat) : 0;
        $wakaf = $faker->boolean() ? $faker->numberBetween(0, $zakat) : 0;

        $zakat *= 1000;
        $infak *= 1000;
        $santunan *= 1000;
        $wakaf *= 1000;

        $link_kuitansi = \asset('assets/pdf/sample.pdf');

        return [
            // 'tanggal' => $faker->dateTimeBetween('-10 year', 'now'),
            'status' => $status->value,
            'zakat' => $zakat,
            'infak' => $infak,
            'santunan' => $santunan,
            'wakaf' => $wakaf,
            'link_kuitansi' => $status->in($status::Berhasil()) ? $link_kuitansi : null,
            'keterangan' => "- aaa\n- bbb\n- ccc\n",
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Donasi $laporan) {
            $laporan->setJumlah();
        });
    }
}
