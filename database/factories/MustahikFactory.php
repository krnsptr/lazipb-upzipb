<?php

namespace Database\Factories;

use App\Enums\Asnaf;
use App\Models\Mustahik;
use App\Models\Wilayah\Kelurahan;
use Illuminate\Database\Eloquent\Factories\Factory;

class MustahikFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mustahik::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $list_id_kelurahan;

        $list_id_kelurahan ??= Kelurahan::pluck('id');

        $faker = $this->faker;

        $id_kelurahan = $list_id_kelurahan->random();
        $kelurahan = Kelurahan::find($id_kelurahan);

        $asnaf = Asnaf::getRandomValue();

        if ($faker->boolean(20)) {
            $asnaf |= Asnaf::getRandomValue();
        }

        return [
            'id_kelurahan' => $kelurahan->id,
            'nama' => $faker->name,
            'nik' => $faker->regexify(Mustahik::REGEX['nik']),
            'nrp' => $faker->regexify(Mustahik::REGEX['nrp']),
            'nomor_hp' => $faker->regexify(Mustahik::REGEX['nomor_hp']),
            'asnaf' => $asnaf,
            'alamat' => $faker->streetAddress,
        ];
    }
}
