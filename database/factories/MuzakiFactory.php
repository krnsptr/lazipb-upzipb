<?php

namespace Database\Factories;

use App\Models\Muzaki;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class MuzakiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Muzaki::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        return [
            'nama' => $faker->name,
            'email' => $faker->safeEmail,
            'nip' => $faker->regexify(Muzaki::REGEX['nip']),
            'npwp' => $faker->regexify(Muzaki::REGEX['npwp']),
            'nomor_hp' => $faker->regexify(Muzaki::REGEX['nomor_hp']),
            'nomor_wa' => $faker->regexify(Muzaki::REGEX['nomor_wa']),
            'nomor_rekening' => $faker->regexify(Muzaki::REGEX['nomor_rekening']),
        ];
    }
}
