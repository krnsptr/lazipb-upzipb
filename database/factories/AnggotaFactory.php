<?php

namespace Database\Factories;

use App\Models\Anggota;
use App\Models\AnggotaProfil;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnggotaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Anggota::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;

        $username = $faker->userName . '_' . $faker->bothify('??###');
        $domain = $faker->safeEmailDomain;
        $password = '$2y$10$H/T7.teffG.1AgrJWV.bBue8Cuh/deWNW6CeV.Qnl2Ntw2savZQz.'; // wordpass

        return [
            'nama' => $faker->name,
            'email' => "$username@$domain",
            'password' => $password,
            'email_verified_at' => \now(),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Anggota $anggota) {
            AnggotaProfil::factory()->create([
                'id' => $anggota->id,
            ]);
        });
    }
}
