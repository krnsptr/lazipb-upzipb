<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \setting()->forgetAll();

        \setting(['kontak_wa' => '+62 800 000 000']);

        \setting()->save();
    }
}
