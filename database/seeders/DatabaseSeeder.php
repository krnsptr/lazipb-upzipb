<?php

namespace Database\Seeders;

use Database\Seeders\AdminSeeder;
use Database\Seeders\AnggotaSeeder;
use Database\Seeders\DistribusiSeeder;
use Database\Seeders\DonasiSeeder;
use Database\Seeders\MustahikSeeder;
use Database\Seeders\MuzakiSeeder;
use Database\Seeders\SettingSeeder;
use Database\Seeders\WilayahSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(WilayahSeeder::class);

        $this->call(SettingSeeder::class);

        $this->call(AdminSeeder::class);

        if (\App::environment(['local', 'testing'])) {
            $this->call(AnggotaSeeder::class);
            $this->call(MuzakiSeeder::class);
            $this->call(DonasiSeeder::class);
            $this->call(MustahikSeeder::class);
            $this->call(DistribusiSeeder::class);
        }
    }
}
