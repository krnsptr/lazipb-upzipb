<?php

namespace Database\Seeders;

use App\Models\Anggota;
use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        try {
            for ($i = 1; $i <= 9; $i++) {
                Anggota::factory()->create([
                    'nama' => "Member $i",
                    'email' => $email = "member$i@example.com",
                    'password' => \bcrypt($email),
                    'email_verified_at' => \now(),
                ]);
            }

            Anggota::factory(91)->create();
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
