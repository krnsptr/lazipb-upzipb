<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        try {
            $admin = Admin::make([
                'nama' => 'Administrator',
                'email' => $email = 'admin@example.com',
                'password' => \bcrypt($email),
            ]);

            $admin->save();
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
