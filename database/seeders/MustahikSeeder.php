<?php

namespace Database\Seeders;

use App\Models\AdminLog;
use App\Models\Mustahik;
use Illuminate\Database\Seeder;

class MustahikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        try {
            $list_mustahik = Mustahik::factory(50)->create();

            $input_first = $list_mustahik->first()->nama;
            $input_last = $list_mustahik->last()->nama;
            $count = $list_mustahik->count();

            AdminLog::create([
                'tanggal' => \now(),
                'aksi' => 'mustahik/impor',
                'jumlah' => $count,
                'input' => "$input_first (..) $input_last",
                'output' => json_encode([
                    'OK' => $count,
                    'WARNING' => 0,
                    'ERROR' => 0
                ]),
                'detail' => [
                    'Berhasil diimpor.' => $count,
                ],
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
