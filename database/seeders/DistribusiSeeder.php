<?php

namespace Database\Seeders;

use App\Models\AdminLog;
use App\Models\Mustahik;
use Database\Factories\DistribusiFactory;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class DistribusiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        static $list_mustahik;

        $list_mustahik ??= Mustahik::get(['id', 'asnaf']);
        $list_input = \collect();

        \DB::beginTransaction();

        try {
            for ($j = 1; $j <= 100; $j++) {
                $tanggal = \now()->subMonthNoOverflow($j)->startOfMonth();

                foreach ($list_mustahik as $mustahik) {
                    for ($i = 0; $i < $faker->numberBetween(0, 3); $i++) {
                        $distribusi = (new DistribusiFactory())->withMustahik($mustahik)->make();
                        $distribusi->tanggal = $tanggal;
                        $distribusi->save();

                        $list_input->push($distribusi->tanggal->format('Y-m-d'));
                    }
                }

                for ($i = 0; $i < $faker->numberBetween(0, 2); $i++) {
                    $distribusi = (new DistribusiFactory())->withoutMustahik()->make();
                    $distribusi->tanggal = $tanggal;
                    $distribusi->save();

                    $list_input->push($distribusi->tanggal->format('Y-m-d'));
                }
            }

            $input_first = $list_input->first();
            $input_last = $list_input->last();
            $count = $list_input->count();

            AdminLog::create([
                'tanggal' => \now(),
                'aksi' => 'distribusi/impor',
                'jumlah' => $count,
                'input' => "$input_first (..) $input_last",
                'output' => json_encode([
                    'OK' => $count,
                    'WARNING' => 0,
                    'ERROR' => 0
                ]),
                'detail' => [
                    'Berhasil diimpor.' => $count,
                ],
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
