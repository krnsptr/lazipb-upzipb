<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();

        $sql_names = [
            'wilayah_provinsi',
            'wilayah_kota',
            'wilayah_kecamatan',
            'wilayah_kelurahan',
        ];

        foreach ($sql_names as $sql_name) {
            $this->command->info("Importing: $sql_name.sql");
            $sql = __DIR__ . "/sql/$sql_name.sql";
            \DB::unprepared(file_get_contents($sql));
        }

        \DB::commit();
    }
}
