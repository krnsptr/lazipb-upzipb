<?php

namespace Database\Seeders;

use App\Enums\DonasiStatus;
use App\Models\AdminLog;
use App\Models\Donasi;
use App\Models\Muzaki;
use Illuminate\Database\Seeder;

class DonasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count_muzaki = Muzaki::count();
        $list_input = \collect();

        \DB::beginTransaction();

        try {
            for ($i = 1; $i <= $count_muzaki; $i++) {
                $id_muzaki = $i;

                for ($j = 1; $j <= 100; $j++) {
                    $tanggal = \now()->subMonthNoOverflow($j)->startOfMonth();

                    /** @var Donasi $donasi */
                    $donasi = Donasi::factory()->create([
                        'id_muzaki' => $id_muzaki,
                        'tanggal' => $tanggal,
                    ]);

                    $list_input->push($donasi->tanggal->format('Y-m-d'));
                }
            }

            $input_first = $list_input->first();
            $input_last = $list_input->last();
            $count = $list_input->count();

            AdminLog::create([
                'tanggal' => \now(),
                'aksi' => 'donasi/impor',
                'jumlah' => $count,
                'input' => "$input_first (..) $input_last",
                'output' => json_encode([
                    'OK' => $count,
                    'WARNING' => 0,
                    'ERROR' => 0
                ]),
                'detail' => [
                    'Berhasil diimpor.' => $count,
                ],
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
