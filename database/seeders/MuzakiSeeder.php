<?php

namespace Database\Seeders;

use App\Models\AdminLog;
use App\Models\Anggota;
use App\Models\Muzaki;
use Illuminate\Database\Seeder;

class MuzakiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_anggota = Anggota::all();
        $list_input = \collect();

        \DB::beginTransaction();

        try {
            foreach ($list_anggota as $anggota) {
                $muzaki = Muzaki::factory()->create([
                    'nama' => $anggota->nama,
                    'email' => $anggota->email,
                ]);

                $list_input->push($muzaki->nama);
            }

            $input_first = $list_input->first();
            $input_last = $list_input->last();
            $count = $list_input->count();

            AdminLog::create([
                'tanggal' => \now(),
                'aksi' => 'muzaki/impor',
                'jumlah' => $count,
                'input' => "$input_first (..) $input_last",
                'output' => json_encode([
                    'OK' => $count,
                    'WARNING' => 0,
                    'ERROR' => 0
                ]),
                'detail' => [
                    'Berhasil diimpor.' => $count,
                ],
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            \dd($e->errors());
        }

        \DB::commit();
    }
}
