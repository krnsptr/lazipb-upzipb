<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistribusiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribusi', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_mustahik')->nullable();
            $table->unsignedTinyInteger('asnaf')->nullable()->index();
            $table->unsignedTinyInteger('sdg')->nullable()->index();
            $table->unsignedTinyInteger('bidang')->nullable()->index();
            $table->unsignedTinyInteger('dana')->index();
            $table->dateTime('tanggal')->index();
            $table->string('item');
            $table->unsignedInteger('nominal');
            $table->json('detail');
            $table->timestamps();

            $table->foreign('id_mustahik')->references('id')->on('mustahik');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribusi');
    }
}
