<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMustahikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mustahik', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('id_kelurahan')->nullable();
            $table->unsignedSmallInteger('id_kecamatan')->nullable();
            $table->unsignedSmallInteger('id_kota')->nullable();
            $table->unsignedTinyInteger('id_provinsi')->nullable();
            $table->string('nama');
            $table->string('nik')->unique();
            $table->string('nrp')->nullable();
            $table->string('nomor_hp')->nullable();
            $table->unsignedTinyInteger('asnaf')->index();
            $table->string('alamat')->nullable();
            $table->string('nama_kelurahan')->nullable();
            $table->string('nama_kecamatan')->nullable();
            $table->string('nama_kota')->nullable();
            $table->string('nama_provinsi')->nullable();
            $table->string('foto')->nullable();
            $table->json('detail');
            $table->timestamps();

            $table->foreign('id_kelurahan')->references('id')->on('wilayah_kelurahan');
            $table->foreign('id_kecamatan')->references('id')->on('wilayah_kecamatan');
            $table->foreign('id_kota')->references('id')->on('wilayah_kota');
            $table->foreign('id_provinsi')->references('id')->on('wilayah_provinsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mustahik');
    }
}
