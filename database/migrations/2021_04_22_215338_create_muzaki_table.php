<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuzakiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muzaki', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('email')->nullable()->unique();
            $table->string('nip')->unique();
            $table->string('npwp')->nullable();
            $table->string('nomor_hp')->nullable();
            $table->string('nomor_wa')->nullable();
            $table->string('nomor_rekening')->nullable();
            $table->json('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muzaki');
    }
}
