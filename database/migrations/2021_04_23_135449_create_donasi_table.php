<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_muzaki');
            $table->dateTime('tanggal')->index();
            $table->unsignedTinyInteger('status')->index();
            $table->unsignedInteger('zakat');
            $table->unsignedInteger('infak');
            $table->unsignedInteger('santunan');
            $table->unsignedInteger('wakaf');
            $table->unsignedInteger('jumlah');
            $table->string('link_kuitansi')->nullable();
            $table->text('keterangan')->nullable();
            $table->json('detail');
            $table->timestamps();

            $table->foreign('id_muzaki')->references('id')->on('muzaki')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi');
    }
}
