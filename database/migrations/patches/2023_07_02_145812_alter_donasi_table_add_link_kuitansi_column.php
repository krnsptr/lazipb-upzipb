<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDonasiTableAddLinkKuitansiColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('donasi', 'link_kuitansi')) {
            Schema::table('donasi', function (Blueprint $table) {
                $table->string('link_kuitansi')->nullable()->after('jumlah');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('donasi', 'link_kuitansi')) {
            Schema::table('donasi', function (Blueprint $table) {
                $table->dropColumn('link_kuitansi');
            });
        }
    }
}
